@extends('dalil.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            //$('li#about').addClass('active');
        });
    </script>

	<script type="text/javascript">
	var APP_URL = {!! json_encode(url('/')) !!}
	</script>

    <script type="text/javascript">
        $(document).ready(function() {
			
			$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
				var form_data = $("#formdata").serialize();
				
				
				window.setInterval(function () {
				   var date = new Date();
				   //if ((date.getMinutes() % 1) == 0) {

					   sendRequest(); 
				  // }
			   }, 60000);
   
   
		function sendRequest(){
		 $.ajax({
                    url:"{{ route('updatesliderscategory') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if(data.error.length > 0)
                        {
                            var error_html = '';
                            for(var count = 0; count < data.error.length; count++) {
                                error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                            }
                           // $('#form_output').html(error_html);
                        }
                        else
                        {
							//$('#formdata')[0].reset();
							  $('.carousel-inner,.carousel-indicators,.carousel-control-prev,.carousel-control-next').empty()
							
					
							index = 0;
							slidecount = 0;
							promoted_post = data.result;
							var html ="";
							
							for(var i=0 ; i< promoted_post.length ; i++) {
							
							
							html += '<div class="col-md-4 col mb-1 pr-1 float-right"><div class="catBlock">';
							if(promoted_post[i].image!= null)
                            html += '<img src="'+APP_URL +'/storage/'+promoted_post[i].image+'" class="image img-fluid">';
							else
								html += '<img src="'+APP_URL +'/storage/'+'posts/February2020/k5HJ90FIsVXB2Ef3aQzz.png'+'" class="image img-fluid">';
							
                            html += '<a href="'+APP_URL+'/details/'+promoted_post[i].slug+'" class="overlay">';
							
							html += '<div class="rating">';
							
                            for (ratingindex = 0  ; ratingindex < parseInt(promoted_post[1].rating); ratingindex++)
                            html += '<span class="fa fa-star checked"></span>';
                                                    
									if(parseInt(promoted_post[1].rating) < 5){
                                        for (ratingindex2 = parseInt(promoted_post[1].rating); ratingindex2 < 5; ratingindex2++)
                            html += '<span class="fa fa-star"></span>';              
									}

                            html += '</div>';
												
												
                            html +=  '<div class="name mb-3">'+promoted_post[i].title+'<br/>';
													if(promoted_post[i].translations.length > 3){ 
													html += promoted_post[i].translations[3]['value'];
													}
							html += '</div><div class=" phone mb-1"><i class="fas fa-phone-alt"></i>'+promoted_post[i].phone+'</div>';
													
                            html += '<div class="address"><i class="fas fa-map-marker-alt"></i>'+promoted_post[i].location+'<br/>';
										if(promoted_post[i].translations.length > 1){ 
													html += promoted_post[i].translations[1]['value'];
													}
							
							html += '</div></a></div></div>';             
                                                    
                                                            
							index = index+1;
							if(index==3){
								$('<div class="carousel-item ">'+html+'  </div>').appendTo('.carousel-inner');
								//$('<li data-target="#carouselExampleCaptions" data-slide-to="'+i+'"></li>').appendTo('.carousel-indicators');
								index = 0;
								html="";
								slidecount++;

							}else{
							
								if ((index < 3) && (i == (promoted_post.length-1))){
									$('<div class="carousel-item ">'+html+'  </div>').appendTo('.carousel-inner');
									slidecount++;
								}
								
							}
							
							  }
							  
							  
							  $("<ol class='carousel-indicators'></ol>").appendTo('#carouselExampleCaptions');
							  
							  var myCarousels = $("#carouselExampleCaptions");			
							  var indicators = $("#carouselExampleCaptions .carousel-indicators"); 
							  
								for(var i=0 ; i< slidecount ; i++) {
										
										(i === 0) ? 
										indicators.append("<li data-target='#carouselExampleCaptions' data-slide-to='"+i+"' ></li>") : 
										indicators.append("<li data-target='#carouselExampleCaptions' data-slide-to='"+i+"'></li>");
									}
								
		
							  $('.carousel-item').first().addClass('active');
							  $('#carouselExampleCaptions').carousel();
  
                          //console.log(data.result);

                        }
                    }
                });
		}



		
		
        });

    </script>
	

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="cat-title pb-4 pr-5 pl-5 pt-5">
                    {{$category->name}}
                    <br/>
                    {{ $category->getTranslatedAttribute('name','he') }}
                </div>
            </div>
        </div><!-- /row -->

        @if(count($promoted_posts) > 0)
			<form method="post"  id="formdata">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$category->id}}" name="category_id" />
			</form>
			
        <div class="row">
            <div class="col-md-12 col-12">
                <div id="carouselExampleCaptions" class="carousel slide cat-slider" data-ride="carousel">

                    <!-- carsol inner -->
                    <div class="carousel-inner">
                        @php $active = 'active'; $index = 0; @endphp
                        @foreach($promoted_posts as $promoted_post)
                            @if($index == 0)
                                <div class="carousel-item {{$active}}">
                                    @endif
                                    @php $active = ''; $index ++; @endphp
                                    <div class="col-md-4 col mb-1 pr-1 float-right">
                                        <div class="catBlock">
										@if($promoted_post['image']!="")
                                            <img src="{{url('storage/'.$promoted_post['image'])}}" class="image img-fluid">
										@else
											<img src="{{url('storage/posts/February2020/k5HJ90FIsVXB2Ef3aQzz.png')}}" class="image img-fluid">
											
										@endif
                                            <a href="{{url('details/'.$promoted_post['slug'])}}" class="overlay">
                                                <div class="rating">
                                                    @for ($i =1 ; $i <= $promoted_post['rating']; $i++)
                                                        <span class="fa fa-star checked"></span>
                                                    @endfor

                                                    @if($promoted_post['rating'] <5)
                                                        @for ($i = $promoted_post['rating']; $i < 5; $i++)
                                                            <span class="fa fa-star"></span>
                                                        @endfor
                                                    @endif

                                                </div>
                                                <div class="name mb-3">
                                                    {{$promoted_post['title']}}
                                                    <br/>
													
													@if(count($promoted_post['translations']) > 3)
												
                                                    {{$promoted_post['translations'][3]['value']}}
											
												@endif
                                                </div>
                                                <div class=" phone mb-1">
                                                    <i class="fas fa-phone-alt"></i>
                                                    {{$promoted_post['phone']}}
                                                </div>
                                                <div class="address">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    {{$promoted_post['location']}} <br/>
													
													@if(count($promoted_post['translations']) > 2)
														{{$promoted_post['translations'][1]['value']}}
													@endif
                                                    <br/>
													@if(count($promoted_post['translations']) > 0)
                                                  
												@endif
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @if($index==3)
                                        @php $index = 0; @endphp
                                </div>
                            @endif
                        @endforeach

                    </div>
					   @php $total = count($promoted_posts)/3;  @endphp
                    <ol class="carousel-indicators">
                        @php $active = ''; @endphp
                        @for ($i =0 ; $i < count($promoted_posts)/3; $i++)
                            <li data-target="#carouselExampleCaptions" data-slide-to="{{$i}}" class="{{$active}}"></li>
                            @php $active = ''; @endphp
                            @endfor
                    </ol>
                    <!-- ../carsol inner -->

                </div>
            </div>
        </div>
        @endif



            <div class="col-md-12 col-12">
                <div id="accordion" class="sub-cats">

                    @php $icount = 0; @endphp
                    @foreach($subCats as $subCat)
					@php $icount++   ; @endphp
                    <div class="card mb-2 pt-1 pr-5 pl-5">
                        <div class="card-header" id="heading<?=$icount?>">
                            <h5 class="mb-0 panel-heading"  data-toggle="collapse" data-target="#collapse<?=$icount?>"  aria-controls="collapse<?=$icount?>">
                                @if($subCat->picture != '')
                                <img src="{{url('storage/'.$subCat->picture)}}" class="icon">
                                @endif
                                {{$subCat->name}}
                                <br/>
                                {{ $subCat->getTranslatedAttribute('name','he') }}
                            </h5>
                        </div>

                        <div id="collapse<?=$icount?>" class="collapse" aria-labelledby="heading<?=$icount?>" data-parent="#accordion">
						
                            <div class="card-body border-top">
                                @foreach($subPosts[$subCat->id] as $subpost)
                                <div class="col-md-4 col mb-4 pr-1 float-right" id="innerbox">
                                    <div class="catBlock blue">
                                        <a href="{{url('details/'.$subpost->slug)}}" >
                                            <div class="row mb-3 border-bottom m-2 pb-2">
                                                <div class="col-4">
												@if($subpost->image != '')
                                                    <img src="{{url('storage/'.$subpost->image)}}" class="img-fluid rounded-circle">
												@else
													<img src="{{url('storage/posts/February2020/k5HJ90FIsVXB2Ef3aQzz.png')}}" class="img-fluid rounded-circle">
												@endif
                                                </div>
                                                <div class="col-8">
                                                    <div class="name">
                                                        {{$subpost->title}}
                                                        <br/>
                                                        {{ $subpost->getTranslatedAttribute('title','he') }}
                                                    </div>
                                                    <div class="rating">
                                                        @for ($i =1 ; $i <= $subpost->rating; $i++)
                                                            <span class="fa fa-star checked"></span>
                                                        @endfor

                                                        @if($subpost->rating<5)
                                                            @for ($i = $subpost->rating; $i < 5; $i++)
                                                                <span class="fa fa-star"></span>
                                                            @endfor
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3 address">
                                                <div class="col-4 pl-4"><i class="fas fa-map-marker-alt float-left"></i></div>
                                                <div class="col-8">
                                                    {{$subpost->location}}<br/>
                                                    {{ $subpost->getTranslatedAttribute('location','he') }}
                                                </div>
                                            </div>
                                            <div class="row mb-3 phone">
                                                <div class="col-4 pl-4"><i class="fas fa-phone-alt float-left"></i></div>
                                                <div class="col-8 ">
                                                    {{$subpost->phone}}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                        </div>
                    </div>
                        
                   @endforeach

                </div>
            </div>



    </div><!-- /cotainer -->
    </div><!-- /cotainer -->

@endsection
