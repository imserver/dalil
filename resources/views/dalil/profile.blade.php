@extends('dalil.layouts.Web_app')



@section('content')


    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            //$('li#about').addClass('active');
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#maincategorieslist').on('change', function(event){
                event.preventDefault();
                var form_data = $("#freeads").serialize();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url:"{{ route('getsubcategories') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if(data.error.length > 0)
                        {
                            var error_html = '';
                            for(var count = 0; count < data.error.length; count++) {
                                error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                            }
                            $('#form_output').html(error_html);
                        }
                        else
                        {
                            $('#form_output').html(data.success);
                            $('#subcategorieslist').find('option').remove();
                            $('#subcategorieslist').append(new Option(''),'');
                            subcategoriesresult = data.result;
                            for (i = 0; i < subcategoriesresult.length; i++) {
                                $('#subcategorieslist').append(new Option(subcategoriesresult[i]['name']+"-"+subcategoriesresult[i]['translations'][0].value  , subcategoriesresult[i]['id']));
                            }


                        }
                    }
                })
            });



            $('#maincategorieslist2').on('change', function(event){
                event.preventDefault();
                var form_data = $("#newbusiness").serialize();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url:"{{ route('getsubcategories') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if(data.error.length > 0)
                        {
                            var error_html = '';
                            for(var count = 0; count < data.error.length; count++) {
                                error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                            }
                            $('#form_output').html(error_html);
                        }
                        else
                        {
                            $('#form_output').html(data.success);
                            $('#subcategorieslist2').find('option').remove();
                            $('#subcategorieslist2').append(new Option(''),'');
                            subcategoriesresult = data.result;
                            for (i = 0; i < subcategoriesresult.length; i++) {
                                $('#subcategorieslist2').append(new Option(subcategoriesresult[i]['name']+"-"+subcategoriesresult[i]['translations'][0].value  , subcategoriesresult[i]['id']));
                            }


                        }
                    }
                })
            });


        });

    </script>

    <div class="container about" id="content">
        <div class="row">
            <div class=" intro pt-5 p-4">
                <div class="col-12">
                    <div class="cat-title">

                        الملف الشخصي
                        <br/>
                        פרופיל
                       </div>
                </div>
            </div>
        </div><!-- /row -->

        <div class="row">
            <div class="col">
                <div id="accordion" class="sub-cats">

                    <!-- personal information -->
                    <div class="card mb-2 pt-1 pr-5 pl-5">
                        <div class="card-header" id="heading1">
                            <h5 class="mb-0 panel-heading"  data-toggle="collapse" data-target="#collapse1"  aria-controls="collapse1">
                                <i class="far fa-user"></i>
                                معلومات الحساب

                                פרטי חשבון
                            </h5>
                        </div>

                        <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">

                            <form method="POST" action="{{ route('edit') }}">
                                @csrf
                                <input type="hidden" name="id" value="{{$data['id']}}" />
                            <div class="card-body border-top">

                                <div class="col-md-12 col mb-12 pr-1 float-right">
                                    <div class="col-md-2 float-right">
                                        اسم المستخدم
                                        <br/>
                                        שם המשתמש
                                    </div>

                                    <div class="col-md-1  float-right">
                                        :
                                    </div>

                                    <div class="col-md-4  float-right">
                                        {{$data['email']}}
                                    </div>

                                </div>

                                <div class="col-md-12 col mb-12 pr-1 float-right">
                                <div class="col-md-2  float-right">
                                    الإسم
                                     <br/>
                                    השם
                                </div>

                                    <div class="col-md-1  float-right">
                                        :
                                    </div>

                                    <div class="col-md-4  float-right">

                                        <input id="name" type="text" class="form-control" name="name" value=" {{$data['name']}}" required >

                                    </div>

                                </div>


                                <div class="col-md-12 col mb-12 pr-1 float-right">
                                    <div class="col-md-2 float-right">
                                       الهاتف
                                        <br/>
                                        הטלפון
                                    </div>

                                    <div class="col-md-1  float-right">
                                        :
                                    </div>

                                    <div class="col-md-4  float-right">
                                        <input id="phone" type="text" class="form-control" name="phone" maxlength="14" value=" {{$data['phone']}}" required >
                                    </div>

                                </div>


                                <div class="col-md-12 col mb-12 pr-1 float-right">
                                    <div class="col-md-2 float-right">
                                        الموقع
                                        <br/>
                                        האתר
                                    </div>

                                    <div class="col-md-1  float-right">
                                        :
                                    </div>

                                    <div class="col-md-4  float-right">
                                        <input id="location" type="text" class="form-control" name="location" value=" {{$data['location']}}" required >

                                    </div>

                                </div>

                                <div class="col-md-12 col mb-12 pr-1 float-right">
                                    <div class="col-md-2 float-right">
                                        البريد الإلكتروني
                                        <br/>
                                        דואר אלקטרוני
                                    </div>

                                    <div class="col-md-1  float-right">
                                        :
                                    </div>

                                    <div class="col-md-4  float-right">
                                        <input id="bussiness_email" type="text" class="form-control" name="bussiness_email" value=" {{$data['bussiness_email']}}" required >
                                    </div>

                                </div>



                                <div class="col-md-12 col mb-12 pr-1 float-right">
                                    <div class="col-md-2 float-right">
<p></p>
                                    </div>

                                    <div class="col-md-1  float-right">
<p></p>
                                    </div>

                                    <div class="col-md-4  float-right">
                                        <button type="submit"  ><i class="fas fa-user-edit"></i></button>
                                        <p></p>
                                    </div>

                                </div>


                            </div>
                            </form>
                        </div>
                    </div>

                    <!-- my bussiness -->
                    <div class="card mb-2 pt-1 pr-5 pl-5">
                        <div class="card-header" id="heading2">
                            <h5 class="mb-0 panel-heading"  data-toggle="collapse" data-target="#collapse2" aria-controls="collapse2">
                                <i class="far fa-address-card"></i>
                                مصلحتي
                                העסק שלי
                            </h5>
                        </div>

                        <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                            <div class="card-body border-top">
                                @if($data['account_validation']==1)
                                @if($post!=null)
                                <div class="col-md-4 col mb-4 pr-1 float-right">
                                    <div class="catBlock blue">

                                        @php
                                        $link = "#";
                                        if($data['active']!='Not Active'){
                                        $link = url('details/'.$post->slug);
                                        }


                                        @endphp

                                        <a href="{{$link}}" >
                                            <div class="row mb-3 border-bottom m-2 pb-2">
                                                <div class="col-4">
                                                    <img src="{{url('storage/'.$post->image)}}" class="img-fluid rounded-circle">
                                                </div>
                                                <div class="col-8">
                                                    <div class="name">
                                                        {{$post->title}}
                                                        <br/>
                                                        {{ $post->getTranslatedAttribute('title','he') }}
                                                    </div>


                                                    <div class="rating">
                                                        @for ($i =1 ; $i <= $post->rating; $i++)
                                                            <span class="fa fa-star checked"></span>
                                                        @endfor

                                                        @if($post->rating<5)
                                                                @for ($i = $post->rating; $i < 5; $i++)
                                                                    <span class="fa fa-star"></span>
                                                                @endfor
                                                            @endif

                                                    </div>
                                                        </div>

                                                </div>

                                            <div class="row mb-3 address">
                                                <div class="col-4 pl-4"><i class="fas fa-map-marker-alt float-left"></i></div>
                                                <div class="col-8">
                                                    {{$post->location}}
                                                    {{ $post->getTranslatedAttribute('location','he') }}
                                                    </div>
                                            </div>
                                            <div class="row mb-3 phone">
                                                <div class="col-4 pl-4"><i class="fas fa-phone-alt float-left"></i></div>
                                                <div class="col-8 ">
                                                    {{$post->phone}}

                                                </div>
                                            </div>


                                            <div class="row mb-3 phone">
                                                <div class="col-4 pl-4"></div>
                                                <div class="col-8 ">
                                                    الحالة   המצב:
                                                    <br/>

                                                    {{$data['active']}}

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                @endif

                                <div class="col-md-8  float-right">
                                    <div class="contact-pg" style="margin-bottom: 30px;">

                                        <form action="{{ route('editbusiness') }}"  method="post"  id="freeads" enctype="multipart/form-data">
                                                    <input type="hidden" name="id" value="{{$data['id']}}" />
                                                    <input type="hidden" name="post_id" value="@if($post!=null) {{$post->id}} @endif" />
                                            {{ csrf_field() }}
                                            <div class="form-row contact-form">
                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>
                                                        العنوان الرئيسي للمصلحة
                                                    </label>
                                                    <input type="text"  name="name" class="form-control" value="@if($post!=null) {{$post->title}} @endif" required placeholder="">
                                                </div>

                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>
                                                        הכתובת העיקרית של הרשות
                                                    </label>

                                                    <input type="text"  name="namehe" class="form-control" value="@if($post!=null) {{ $post->getTranslatedAttribute('title','he') }}  @endif" required placeholder="">

                                                </div>


                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>رقم الهاتف

                                                        <br/>
                                                        מספר הטלפון
                                                    </label>
                                                    <input type="text" name="phone" class="form-control phone" maxlength="14" value="@if($post!=null) {{$post->phone}} @endif" placeholder="079 999 99 99">
                                               </div>
                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>البريد الالكتروني

                                                        <br/>
                                                        דואר אלקטרוני
                                                    </label>
                                                    <input type="text" name="email" class="form-control" value="@if($post!=null) {{$post->email}} @endif" required placeholder="">
                                                </div>

                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>
                                                        الموقع

                                                    </label>
                                                    <input type="text" name="location" value="@if($post!=null) {{$post->location}} @endif" class="form-control" required placeholder="">
                                               </div>

                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>

                                                        האתר
                                                    </label>
                                                    <input type="text" name="locationhe" value="@if($post!=null) {{ $post->getTranslatedAttribute('location','he') }}  @endif" class="form-control" required placeholder="">
                                                </div>


                                                <div class="form-group col-md-12 col-12 mt-3">
                                                    <label>
                                                        التصنيف الحالي للمصلحة

                                                        <br/>
                                                        הגדרה נוכחית לעסק
                                                    </label>

                                                    <input type="text" readonly value="{{$data['currentcats']->name}} " class="form-control" />

                                                </div>


                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>
                                                        التصنيف الرئيسي
                                                        <br/>
                                                        קטגוריה
                                                    </label>
                                                    <select id="maincategorieslist" name="maincategory" class="form-control" >
                                                        <option>اختر - בחר</option>
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->name}} - {{ $category->getTranslatedAttribute('name','he') }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div id="form_output"></div>
                                                </div>


                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>
                                                        التصنيف الفرعي
                                                        <br/>
                                                        תת קטגוריה
                                                    </label>
                                                    <select id="subcategorieslist" name="category" class="form-control" >
                                                        <option value="">اختر - בחר</option>
                                                    </select>
                                                </div>



                                                <div class="form-group col-md-12 col-12 mt-3">
                                                    <label>
                                                        صورة رئيسية

                                                        <br/>
                                                        תמונה ראשית
                                                    </label>
                                                    <input type="file" name="thumb" class="form-control" value="@if($post!=null) {{$post->image}} @endif" readonly style="width: 100%;">
                                                </div>


                                                <div class="form-group col-md-12 col-12 mt-3" style="    text-align: center;">
                                                @if($post!=null)
                                                        <img src="{{url('storage/'.$post->image)}}" style="margin-top: 15px;     width: 50%;"/>
                                                    @endif
                                                </div>


                                                <div class="form-group col-md-12 col-12 mt-3">
                                                    <label>
                                                        صور إضافية
                                                        <br/>
                                                        תמונות נוספות
                                                    </label>
                                                    <input id="additionaimages" class="form-control" type="file" name="photos[]" multiple="multiple"  readonly value="" style="width: 100%;">
                                                </div>


                                                @foreach($additionals_images as $img)
                                                    <div class="col-md-3" id="lightboxfancy">
                                                        <a href="{{url('storage/'.$img)}}" data-fancybox data-caption="">
                                                            <img src="{{url('storage/'.$img)}}"  />
                                                        </a>
                                                    </div>
                                                @endforeach


                                                <div class="form-group col-md-12 col-12 mt-3">
                                                    <label>
                                                        توصيف المصلحة
                                                    </label>
                                                    <textarea name="companydesc" class="form-control" placeholder="" id="companydesc" style="margin-bottom: 15px;">@if($post!=null) {{$post->body}} @endif</textarea>
                                                    <label>
                                                        אפיון עניין
                                                    </label>
                                                    <textarea name="companydesche" class="form-control" placeholder="" id="companydesc">@if($post!=null) {{ $post->getTranslatedAttribute('body','he') }} @endif</textarea>
                                                </div>



                                                <button class="col-md-4 offset-md-4 col-12 mt-3 btn btn-primary " type="submit">
                                                        تعديل المصلحة
                                                        <br/>
                                                        התאמת ריבית
                                                </button>
                                            </div>
                                        </form>
                                </div>

                            </div>

                                    @elseif($post!=null)
                                    <form action="{{ route('newrerequest') }}"  method="post"  enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                        <input type="hidden" name="user_id" value="{{$data['id']}}" />
                                        <input type="hidden" name="email" value="{{$data['email']}}" />
                                        <input type="hidden" name="msg" value="الرجاء تجديد الإشتراك" />

                                        <button class="col-md-4 offset-md-4 col-12 mt-3 btn btn-primary " type="submit">

                                            تجديد الإشتراك

                                            <br/>
                                            חידוש מנוי

                                        </button>
                                    </form>

                                    @else
                                    <form action="{{ route('business') }}"  method="post" id="newbusiness" enctype="multipart/form-data">

                                        <div class="form-row contact-form">
                                        {{ csrf_field() }}
                                        <div class="form-group col-md-6 col-12 mt-3">
                                            <label>
                                                العنوان الرئيسي للمصلحة
                                            </label>
                                            <input type="text"  name="name" class="form-control" required placeholder="">
                                        </div>

                                            <div class="form-group col-md-6 col-12 mt-3">
                                                <label>
                                                    הכתובת העיקרית של הרשות
                                                </label>
                                                <input type="text"  name="namehe" class="form-control" required placeholder="">
                                            </div>


                                        <div class="form-group col-md-6 col-12 mt-3">
                                            <label>رقم الهاتف

                                                <br/>
                                                מספר הטלפון
                                            </label>
                                            <input type="text" name="phone" class="form-control phone"  placeholder="079 999 99 99" maxlength="14" >
                                        </div>
                                        <div class="form-group col-md-6 col-12 mt-3">
                                            <label>البريد الالكتروني

                                                <br/>
                                                דואר אלקטרוני
                                            </label>
                                            <input type="text" name="email" class="form-control" required placeholder="">
                                        </div>

                                            <div class="form-group col-md-6 col-12 mt-3">
                                                <label>
                                                    الموقع
                                                </label>
                                                <input type="text" name="location"  class="form-control" required placeholder="">

                                            </div>


                                            <div class="form-group col-md-6 col-12 mt-3">
                                                <label>
                                                    האתר
                                                </label>
                                                <input type="text" name="locationhe" class="form-control" required placeholder="">
                                            </div>


 <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>
                                                        التصنيف الرئيسي
                                                        <br/>
                                                        קטגוריה
                                                    </label>
                                                    <select id="maincategorieslist2" name="maincategory" class="form-control" >
                                                        <option>اختر - בחר</option>
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->name}} - {{ $category->getTranslatedAttribute('name','he') }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div id="form_output"></div>
                                                </div>


                                                <div class="form-group col-md-6 col-12 mt-3">
                                                    <label>
                                                        التصنيف الفرعي
                                                        <br/>
                                                        תת קטגוריה
                                                    </label>
                                                    <select id="subcategorieslist2" name="category" class="form-control" >
                                                        <option value="">اختر - בחר</option>
                                                    </select>
                                                </div>



                                        <div class="form-group col-md-6 col-12 mt-3">
                                            <label>
                                                صورة رئيسية

                                                <br/>
                                                תמונה ראשית
                                            </label>
                                            <input type="file" name="thumb" class="form-control"  style="width: 100%; ">

                                        </div>

                                            <div class="form-group col-md-6 col-12 mt-3">
                                                <label>
                                                    صور إضافية
                                                    <br/>
                                                    תמונות נוספות
                                                </label>
                                                <input id="additionaimages" class="form-control" type="file" name="photos[]" multiple="multiple"  readonly value="" style="width: 100%;">
                                            </div>

                                        <div class="form-group col-md-12 col-12 mt-3">
                                            <label>
                                                توصيف المصلحة

                                            </label>
                                            <textarea name="companydesc" class="form-control" placeholder="" id="companydesc" style="margin-bottom: 15px;"></textarea>
                                        </div>


                                            <div class="form-group col-md-12 col-12 mt-3">
                                                <label>
                                                    אפיון עניין
                                                </label>
                                               <textarea name="companydesche" class="form-control" placeholder="" id="companydesc"></textarea>
                                            </div>

                                            <input type="hidden" name="id" value="{{$data['id']}}" />


                                        <button class="col-md-4 offset-md-4 col-12 mt-3 btn btn-primary " type="submit">
                                            أضف المصلحة

                                            <br/>
                                            הוסף ריבית
                                        </button>
                                        </div>
                                    </form>

                                @endif
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div><!-- /cotainer -->
    </div><!-- /cotainer -->


@endsection
