@extends('dalil.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#contact').addClass('active');
        });
    </script>

    <div class="container contact">
        <div class="row">
            <div class=" intro pt-5 p-4">
                <div class="col-12">
                    <div class="cat-title">
                        اتصل بنا
                        <br/>
                        התקשרו אלינו
                        </div>
                </div>
                <div class="col-12">

                    @if(!empty($page)>0)
                        {!! $page['body'] !!}
                        <br/>
                    {!!  $page->getTranslatedAttribute('body','he')  !!}

                        @endif

                </div>
            </div>
        </div><!-- /row -->

        <div class="row contact-pg1 m-4">
            <div class="col-md-4 col-12 info p-4 pt-5">
                <div class="row title justify-content-center mb-4">معلومات التواصل
                <br/>
                    מידע ליצירת קשר
                </div>
                @if(setting('contact.address')!=null)
                <div class="row mb-4 desc">
                    <div class="col-2"><i class="fas fa-map-marker-alt"></i></div>
                    <div class="col-10">
                        {!! setting('contact.address') !!}
                    </div>
                </div>
                @endif

                @if(setting('contact.email') != null)
                <div class="row mb-4 desc">
                    <div class="col-2"><i class="fas fa-envelope"></i></div>
                    <div class="col-10">
                        {{ setting('contact.email') }}
                    </div>
                </div>
                @endif

                @if(setting('contact.fax') != null)
                <div class="row mb-4 desc num">
                    <div class="col-2"><i class="fas fa-fax"></i></div>
                    <div class="col-10" style="    direction: ltr;">{{ setting('contact.fax') }}</div>
                </div>
                @endif

                @if(setting('contact.phone') != null)
                <div class="row mb-4 desc num">
                    <div class="col-2"><i class="fas fa-phone-alt"></i></div>
                    <div class="col-10 " style="    direction: ltr;">{{ setting('contact.phone') }}</div>
                </div>
                    @endif

            </div>
            <div class="col-md-8 col-12 contact-form p-5">
                <div class="row title justify-content-center mb-4">

                    أرسل لنا رسالة

                    <br/>
                    שלח לנו הודעה
                    </div>
                <form method="post"  action="{{ route('contact') }}"  >
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>

                                الاسم الثلاثي
                            <br/>
                                שם מלא
                            </label>
                            <input type="text" class="form-control" name="name" value="{{old('name')}}" required placeholder="">
                        </div>
                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>
                                رقم الهاتف
                                <br/>
                                מספר הטלפון
                            </label>
                            <input type="text" class="form-control phone" name="phone" value="{{old('phone')}}" required placeholder="" style="direction: ltr;" maxlength="14">
                        </div>
                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>

                                البريد الإلكتروني


                                <br/>
                                דואר אלקטרוני
                            </label>
                            <input type="text" class="form-control" name="email" value="{{old('email')}}" required placeholder="">
                        </div>
                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>محتوى الرسالة
                            <br/>
                                תוכן ההודעה
                            </label>
                            <input type="text" class="form-control"  name="msg"  value="{{old('msg')}}" required>
                        </div>
                        <button class="col-md-4 offset-md-4 col-12 mt-3 btn btn-primary "  required type="submit">
                            أرسل
                            <br/>
                            שלח

                        </button>
                    </div>
                </form>
            </div>
        </div>


    </div><!-- /cotainer -->

@endsection
