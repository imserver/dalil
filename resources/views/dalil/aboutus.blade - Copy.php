@extends('nadsoftweb.layouts.Web_app')



@section('content')
    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#about').addClass('active');
        });
    </script>
    <!-- about style -->
    <link rel="stylesheet" href="{{ url('/web/assets/about.css') }}" />
    <!-- section 1 -->
    <div class="container-fluid" id="section1">
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-4" id="pagetitle">
                    <h2>ABOUT US</h2>
                </div>
                <div class="col-md-1"></div>

                <div class="col-md-7" id="intro">
                    <h2 class="maintitle aos-item" data-aos="fade-up">WHO<span>WE ARE ?</span>  </h2>
                    {!! $about->desc !!}
                </div>



            </div>
        </div>
    </div>
    <!-- ../section 1 -->



    <!-- section 2 -->
    <div class="container" id="section2">
        <div class="">
            <div class="col-lg-l2">
                <div class="col-md-4 aos-item" data-aos="fade-down" id="innerbox">
                    <span id="icon"><img src="/web/assets/images/about/startup-2.png" /></span>
                    <span id="title">Startups</span>
                    {!! $about->service1_desc !!}
                </div>

                <div class="col-md-4 aos-item" data-aos="zoom-in" id="innerbox">
                    <span id="icon"><img src="/web/assets\images\about\entrepreneur-2.png" /></span>
                    <span id="title">Individual Entrepreneurs</span>
                    {!! $about->service2_desc !!}
                </div>

                <div class="col-md-4 aos-item" data-aos="fade-up" id="innerbox">
                    <span id="icon"><img src="/web/assets\images\about\building.png" /></span>
                    <span id="title">Organisations/Businesses</span>

                    {!! $about->service3_desc !!}
                </div>

            </div>
        </div>
    </div>
    <!-- ../section 2 -->


    <!-- section 3 -->
    <div class="container-fluid" id="section3">

        <h2 class="maintitle aos-item" data-aos="fade-up"> OUR Partners <span>& Clients</span> </h2>
        <!-- snow -->
        <div class="d-none d-sm-block d-sm-none  d-md-block">
        <div class="snowflakes" aria-hidden="true">
            <div class="snowflake">
                <img src="/web/assets/images/stars-red.png" />
            </div>
            <div class="snowflake" style="display:none;" >
                ❅
            </div>
            <div class="snowflake"  >
                <img src="/web/assets/images/stars-red.png" />
            </div>
            <div class="snowflake">
                <img src="/web/assets/images/stars-blue.png" />
            </div>
            <div class="snowflake">
                <img src="/web/assets/images/stars-red.png"  />
            </div>
            <div class="snowflake">
                <img src="/web/assets/images/stars-blue.png"
            </div>
            <div class="snowflake" style="display:none;">
                <img src="/web/assets/images/stars-red.png" />
            </div>
            <div class="snowflake">
                <img src="/web/assets/images/stars-red.png" />
            </div>
            <div class="snowflake" style="display:none;" >
                <img src="/web/assets/images/stars-blue.png" />
            </div>
            <div class="snowflake" style="display:none;">
                <img src="/web/assets/images/stars-red.png" />
            </div>
        </div> <!-- snow -->
    </div>
        <span class="skystar"  aria-hidden="true"></span>

        <span class="skystar"  aria-hidden="true"></span>





        <span class="skystar red"  aria-hidden="true"></span>


        <div class="row">
            <div class="col-lg-l2 aos-item" data-aos="zoom-in-up">
            {!! $about->about_clients !!}
           </div>

            <div class="containercustom">
                <div class="col-md-l2">

                    <div class="d-none d-sm-block d-sm-none  d-md-block">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel"> <!--  -->
                        <!-- Indicators -->
                        @if(count($clients) > 10)
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <!--
                            <li data-target="#myCarousel" data-slide-to="2"></li> -->
                        </ol>
                        @endif

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php $index = 0;  $slide = 1;?>
                            @foreach($clients as $client)
                            @if(($slide==1) && ($index==0))
                                        <div class="item active">
                                @endif
                                            @if(($slide==2) && ($index==0))
                                        </div>
                                                <div class="item">
                                                    @endif
                                            <div>
                                                <img src="{{url('storage/'.$client->thumbbg)}}" alt="Nadsoft">
                                                <span><img src="{{url('storage/'.$client->thumb)}}" /></span>
                                            </div>
                                                <?php $index++; ?>
                                            @if($index==10)
                                                <?php $slide ++;
                                                        $index = 0;
                                                ?>
                                    @endif
                            @endforeach
                        </div>
                                        @if(count($clients) > 10)
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <img src="/web/assets/images/prev.png" style="    margin-top: 45px;"/>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <img src="/web/assets/images/next.png" style="    margin-top: 45px;"/>
                        </a>
                                @endif
                    </div>
                </div><!-- descktop slider -->

            </div>

                    <!-- mobile slider -->
                    <div class="d-block d-sm-none d-sm-none  d-none d-sm-block d-md-none">
                        <div id="myCarouselmobile" class="carousel slide" data-ride="carousel">
                        <?php $index = 0;  ?>
                            <ol class="carousel-indicators">
                        @foreach($clients as $client)
                            @if($index==0)
                                        <li data-target="#myCarouselmobile" data-slide-to="{{$index}}" class="active"></li>

                                @else
                                        <li data-target="#myCarouselmobile" data-slide-to="{{$index}}"></li>
                                    @endif
                                <?php $index++; ?>
                            @endforeach

                        </ol>
                            <?php $index = 0;  ?>

                        <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php $class='active'; ?>
                                @foreach($clients as $client)
                                        <div class="item {{$class}}">
                                            <div style="width: 100% !important; height: 70% !important;">
                                                <img src="{{url('storage/'.$client->thumbbg)}}" alt="Nadsoft">
                                                <span><img src="{{url('storage/'.$client->thumb)}}" /></span>
                                            </div>
                                        </div>
                                        <?php $class=''; ?>
                                    @endforeach

                                        @if(count($clients) > 1)
                                        <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#myCarouselmobile" data-slide="prev">
                                                <img src="/web/assets/images/prev.png" style="    margin-top: 15px;"/>
                                            </a>
                                            <a class="right carousel-control" href="#myCarouselmobile" data-slide="next">
                                                <img src="/web/assets/images/next.png" style="    margin-top: 15px;"/>
                                            </a>
                                        @endif

                    </div>
                    </div>
                    </div>
                    <!-- ../mobile slider -->
            </div>
        </div>


    </div>


    </div>
    </div>









    <!-- ../section 3 -->


    <!-- section 4 -->
    <div class="container-fluid" id="section4">
        <div class="row" >
            <h2 class="maintitle aos-item" data-aos="fade-up"> OUR  <span>TEAM</span> </h2>
            <span class="skystar"></span>
            <div class="desc col-md-12 aos-item" data-aos="zoom-out-up">
                {!! $about->about_team !!}

<!--
                <div class="d-none d-sm-block d-sm-none  d-md-block">
                <div class="team col-lg-12 ">

                    <div class="emp aos-item" data-aos="zoom-in-up">
                        <img src="{{url('storage/'.$ourteams[0]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[0]->name}}</span>
                            {{$ourteams[0]->jobtitle}}
                        </p>
                    </div>

                    <div class="emp aos-item" data-aos="zoom-in-down">
                        <img src="{{url('storage/'.$ourteams[1]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[1]->name}}</span>
                            {{$ourteams[1]->jobtitle}}
                        </p>
                    </div>


                    <div class="emp aos-item" data-aos="zoom-in-left">
                        <img src="{{url('storage/'.$ourteams[2]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[2]->name}}</span>
                            {{$ourteams[2]->jobtitle}}
                        </p>
                    </div>


                    <div class="emp aos-item" data-aos="zoom-in-right">
                        <img src="{{url('storage/'.$ourteams[3]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[3]->name}}</span>
                            {{$ourteams[3]->jobtitle}}
                        </p>
                    </div>

                    <div class="emp aos-item" data-aos="zoom-in-up">
                        <img src="{{url('storage/'.$ourteams[4]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[4]->name}}</span>
                            {{$ourteams[4]->jobtitle}}
                        </p>
                    </div>


                    <div class="emp aos-item" data-aos="zoom-in-down">
                        <img src="{{url('storage/'.$ourteams[5]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[5]->name}}</span>
                            {{$ourteams[5]->jobtitle}}
                        </p>
                    </div>


                    <div class="emp aos-item" data-aos="zoom-in-left">
                        <img src="{{url('storage/'.$ourteams[6]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[6]->name}}</span>
                            {{$ourteams[6]->jobtitle}}
                        </p>
                    </div>

                    <div class="emp aos-item" data-aos="zoom-in-right">
                        <img src="{{url('storage/'.$ourteams[7]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[7]->name}}</span>
                            {{$ourteams[7]->jobtitle}}
                        </p>
                    </div>

                    <div class="emp aos-item" data-aos="zoom-in-up">
                        <img src="{{url('storage/'.$ourteams[8]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[8]->name}}</span>
                            {{$ourteams[8]->jobtitle}}
                        </p>
                    </div>

                    <div class="emp aos-item" data-aos="zoom-in-down">
                        <img src="{{url('storage/'.$ourteams[9]->thumb)}}" />
                        <p id="empintro">
                            <span>{{$ourteams[9]->name}}</span>
                            {{$ourteams[9]->jobtitle}}
                        </p>
                    </div>

                </div>
                </div> -->

                <!-- for desktop -->


                <!-- mobile slider -->
                    <!--
                <div class="d-block d-sm-none d-sm-none  d-none d-sm-block d-md-none">
                    <div id="myCarouselmobileemp" class="carousel slide" data-ride="carousel">

                    <?php $index = 0;  ?>


                        <div class="carousel-inner">
                            <?php $class='active'; ?>
                            @foreach($ourteams as $emp)
                                <div class="item {{$class}}">

                                        <img src="{{url('storage/'.$emp->thumb)}}" alt="{{$emp->name}}" class = "img-responsive" style="max-width: 70%; display: block; float: none;     margin: auto;">
                                    <h2 style="color:#4ea9e0">{{$emp->name}}</h2>
                                    <p>{{$emp->jobtitle}}</p>


                                </div>
                                <?php $class=''; ?>
                            @endforeach

                            @if(count($clients) > 1)
                            <!-- Left and right controls -->        <a class="left carousel-control" href="#myCarouselmobileemp" data-slide="prev">
                                    <img src="/web/assets/images/prev.png" style="    margin-top: 15px;"/>
                                </a>
                                <a class="right carousel-control" href="#myCarouselmobileemp" data-slide="next">
                                    <img src="/web/assets/images/next.png" style="    margin-top: 15px;"/>
                                </a>
                            @endif

                        </div>
                    </div>
                </div> -->
                <!-- ../mobile slider -->




            </div>
        </div>
    </div>



    <!-- ../section 4 -->


@endsection
