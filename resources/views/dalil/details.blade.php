@extends('dalil.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            //$('li#about').addClass('active');
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#noncheckedstar').click(function () {
                currentrate = $('#postrating').val();
                newrate = parseInt(currentrate) + 1;
                $('#postrating').val(newrate);
                //$(this).addClass('checked');
                $('#ratestar').submit();
            });

            $('#ratestar').on('submit', function(event){
                event.preventDefault();
                var form_data = $(this).serialize();
                $.ajax({
                    url:"{{ route('putrate') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if(data.error.length > 0)
                        {
                            var error_html = '';
                            for(var count = 0; count < data.error.length; count++)
                            {
                                error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                            }
                            $('#form_output').html(error_html);
                        }
                        else
                        {
                            $('#form_output').html(data.success);
                            $('#ratestar')[0].reset();

                            rate = parseInt(data.rating);
                            newrating = '';
                            for (i = 0; i < rate; i++) {
                                newrating = newrating +'<span class="fa fa-star checked"></span>';
                            }

                            if(rate < 5){
                                for (i = rate; i < 5; i++) {
                                    newrating = newrating +'<span class="fa fa-star" ></span>';
                                }
                            }
                            $('#ratestarbasic').html(newrating);

                        }
                    }
                })
            });

        });

    </script>

    <div class="container">
        <div class="row mt-4 mb-3 justify-content-center">
            @if($post!=null)

            <div class="col-md-12" id="mainimagepost">
                <img src="{{url('storage/'.$post->image)}}" class="img-fluid img">
            </div>
            <div class="col-12 ">
                <div class="details p-5">
                    <div class="row mb-2 name justify-content-center " id="maintitle">
                        {{$post->title}}
                        <br/>
                        {{ $post->getTranslatedAttribute('title','he') }}

                    </div>


                    <div class="row mb-2  justify-content-center " >
                       تقييمنا الحالي
                        <br/>
                        ההערכה הנוכחית שלנו
                    </div>


                    <div class="row mb-2 rating justify-content-center" >
                        <div class="rating" id="ratestarbasic">
                            @for ($i =1 ; $i <= $post->rating; $i++)
                                <span class="fa fa-star checked"></span>
                            @endfor

                            @if($post->rating<5)
                                @for ($i = $post->rating; $i < 5; $i++)
                                    <span class="fa fa-star" ></span>
                                @endfor
                            @endif

                        </div>
                    </div>



                    <div class="row mb-2  justify-content-center " >
                        ارجوك قم بتقييمنا
                        <br/>
                        בבקשה, דרג אותנו
                    </div>

                    <form method="post"  id="ratestar">
                        {{csrf_field()}}
                        <span id="form_output"></span>
                        <input type="hidden" value="{{$post->id}}" name="post_id" />
                        <input type="hidden" value="{{$post->rating}}" id="postrating" name="rate" />
                        <div class="row mb-2 rating justify-content-center">
                            <div class="rating">
                                    @for ($i = 0; $i < 5; $i++)
                                        <span class="fa fa-star" id="noncheckedstar"></span>
                                    @endfor
                            </div>
                        </div>
                    </form>

                    <div class="row mb-4 address justify-content-center" id="location">
                        <i class="fas fa-map-marker-alt ml-2"></i>
                        {{$post->location}}
                        <br/>
                        {!! $post->getTranslatedAttribute('location','he') !!}
                    </div>
                    <hr/>
                    <div class="row mb-4 desc pt-4 pr-5 pl-5">
                        {!! $post->body !!}
                        <br/>
                        {!! $post->getTranslatedAttribute('body','he') !!}
                    </div>

                    <div class="row mb-4 desc pt-4 pr-5 pl-5">
                        @if(count($additionals_images) > 0)
                    @foreach($additionals_images as $img)
                        <div class="col-md-3" id="lightboxfancy">
                            <a href="{{url('storage/'.$img)}}" data-fancybox data-caption="">
                                <img src="{{url('storage/'.$img)}}"  />
                            </a>
                        </div>
                        @endforeach
                            @endif
                    </div>

                    <div class="row mb-3 mt-5  justify-content-center">
                        <div class="col-md-3 col phone" id="phone">
                            {{$post->phone}}
                            <i class="fas fa-phone-alt ml-2"></i>
                        </div>
                    </div>
                </div>
            </div>

        @endif
        </div>
    </div>





@endsection

