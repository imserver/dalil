@extends('dalil.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#add').addClass('active');
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#maincategorieslist').on('change', function(event){
                event.preventDefault();
                var form_data = $("#freeads").serialize();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url:"{{ route('getsubcategories') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if(data.error.length > 0)
                        {
                            var error_html = '';
                            for(var count = 0; count < data.error.length; count++) {
                                error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                            }
                            $('#form_output').html(error_html);
                        }
                        else
                        {
                            $('#form_output').html(data.success);
                            $('#subcategorieslist').find('option').remove();
                            $('#subcategorieslist').append(new Option(),'');
                            subcategoriesresult = data.result; 
                            for (i = 0; i < subcategoriesresult.length; i++) {
                                $('#subcategorieslist').append(new Option(subcategoriesresult[i]['name']+"-"+subcategoriesresult[i]['translations'][0].value , subcategoriesresult[i]['id']));
                            }


                        }
                    }
                })
            });

        });

    </script>


    <div class="container contact">
        <div class="row intro add justify-content-center pt-5 p-5">
            <div class="col-12 mb-3">
                <div class="cat-title">أضف مصلحة</div>
                <div class="cat-title">הוסף עסק </div>
            </div>
            <div class="col-11">
                <p>
                    يمكنك ادخال معلومات شركتك من خلال النموذج في الأسقل وارسال تلك المعولومات لنا وسيتم الاتصال بك حال مراجعة طلبك
                </p>

                <p>
                    אתה יכול להזין את פרטי החברה שלך באמצעות הטופס שלהלן ולשלוח אלינו את המידע ואנחנו ניצור איתך קשר אחרי בדיקת בקשתך  </p>
            </div>
        </div><!-- /row -->

        <div class="row ml-5 mr-5 mb-5 add-pg">

            <div class="col-md-10  col-12 contact-pg">
                <form action="{{ route('freeadd') }}" id="freeads" method="post" name="freeads"  enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-row contact-form">
                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>
                                العنوان الرئيسي للمصلحة

                            </label>
                            <input type="text"  name="name" class="form-control" required placeholder="">
                        </div>

                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>

                                הכתובת העיקרית של הרשות
                            </label>
                            <input type="text"  name="namehe" class="form-control" required placeholder="">
                        </div>


                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>رقم الهاتف

                                <br/>
                                מספר נייד
                            </label>
                            <input type="text" name="phone" class="form-control phone" maxlength="14" required placeholder="079 999 99 99">
                        </div>
                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>البريد الالكتروني
                                <br/>
                                דואר אלקטרוני
                            </label>
                            <input type="text" name="email" class="form-control" required placeholder="">
                        </div>

                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>
                                الموقع
                            </label>
                            <input type="text" name="location" class="form-control" required placeholder="">
                        </div>

                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>
                                האתר
                            </label>
                            <input type="text" name="locationhe" class="form-control" required placeholder="">
                        </div>



                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>
                                التصنيف الرئيسي
                            <br/>
                                קטגוריה
                            </label>
                            <select id="maincategorieslist" name="maincategory" class="form-control" required>
                                <option>اختر - בחר</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}} - {{ $category->getTranslatedAttribute('name','he') }}</option>
                                    @endforeach
                            </select>
                            <div id="form_output"></div>
                        </div>


                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>
                                التصنيف الفرعي
                                <br/>
                                תת קטגוריה
                            </label>
                            <select id="subcategorieslist" name="category" class="form-control" required>
                                <option value="">اختر - בחר</option>
                            </select>
                        </div>



                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>
                               صورة رئيسية
                                <br/>
                                תמונה ראשית
                            </label>
                            <input type="file" name="thumb" class="form-control" required readonly style="width: 100%;" required>
                        </div>


                        <div class="form-group col-md-6 col-12 mt-3">
                            <label>
                                صور إضافية
                                <br/>
                                תמונות נוספות
                            </label>
                            <input id="additionaimages" class="form-control" type="file" name="photos[]" multiple="multiple"  readonly value="" style="width: 100%;">
                        </div>


                        <div class="form-group col-md-12 col-12 mt-3">
                            <label>
                                توصيف المصلحة

                            </label>
                            <textarea name="companydesc" class="form-control" placeholder="" id="companydesc" required></textarea>
                        </div>




                        <div class="form-group col-md-12 col-12 mt-3">
                            <label>
                                אפיון עניין
                            </label>
                            <textarea name="companydesche" class="form-control" placeholder="" id="companydesche" required></textarea>
                        </div>


                        <button class="col-md-4 offset-md-4 col-12 mt-3 btn btn-primary " type="submit">

                            أضف المصلحة

                            <br/>
                            הוסף עסק
                        </button>
                    </div>
                </form>
            </div>
        </div>


    </div><!-- /cotainer -->

@endsection
