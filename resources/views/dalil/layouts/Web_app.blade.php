<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    

    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{ url('/web/dalil/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('/web/dalil/css/bootstrap-rtl.css') }}" rel="stylesheet">

    <link href="{{ url('/web/dalil/fontawesome/css/all.css') }}" rel="stylesheet">
    <link href="{{ url('/web/dalil/css/fontface.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ url('/web/dalil/style.css') }}?v=<?=time();?>" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

     <!-- fancybox -->
    <link href="{{ url('/web/dalil/js/fancybox/dist/jquery.fancybox.min.css') }}" rel="stylesheet">
</head>




<body>

@if ((Session::has('success')) || (count($errors) > 0))
	
    @if(count($errors) > 0)
        <!-- Modal -->
            <div class="modal  " id="errorsmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-align: left; padding-left: 25px;">
                                <span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>
                            </button>
                        <div class="modal-body" style=" padding: 5%;">
						  
                            @foreach ($errors->all() as $error)
                                <p class="text-right alert alert-danger">{!!   $error !!}</p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
    @endif



    @if(Session::has('success'))
        <!-- Modal -->
            <div class="modal " id="errorsmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-align: left; padding-left: 25px;">
                                <span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>
                            </button>

                        <div class="modal-body" style=" padding: 5%;">
						
                            <div class="alert alert-success">
                                {!!   Session::get('success') !!}
                                @php Session::forget('success');  @endphp
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif
		
@endif
<div class="container" id="header">

    


    

    <div class="row">
        <div class="col-md-9 col-xs-12">
            <a href="{{url('/')}}" style="float: right;"><img src="/web/dalil/images/logo-dummy.png" class="img-fluid"></a>

<!--
            <a class="navbar-brand" href="{{url('/')}}" id="logo"  >
                <img src="/web/dalil/images/logo.png"  class ="img-responsive"/>
            </a> -->


            <nav class="navbar navbar-expand-sm navbar-light bg-light" style="float: right;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item" id="home">
                            <a class="nav-link" href="{{url('/')}}">الرئيسية</a><br>
                            <a class="nav-link" href="{{url('/')}}">דף ראשי</a>
                        </li>
                        @if(!Session::has('user'))
                        <li class="nav-item" id="add">
                            <a class="nav-link" href="{{url('/freeaddscreen')}}">أضف مصلحة</a><br>
                            <a class="nav-link" href="{{url('/freeaddscreen')}}">הוסף עסק </a>
                        </li>
                        @endif
                        <li class="nav-item" id="about">
                            <a class="nav-link" href="{{url('/about')}}">من نحن</a><br>
                            <a class="nav-link" href="{{url('/about')}}">מי אנחנו</a>
                        </li>
                        <li class="nav-item" id="contact">
                            <a class="nav-link" href="{{url('/page/اتصل-بنا')}}">اتصل بنا</a><br>
                            <a class="nav-link" href="{{url('/page/اتصل-بنا')}}">צור קשר</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-md-3 col-xs-12">

            @if(!Session::has('user'))
            <a href="{{url('/login')}}" class="btn btn-primary btn-login btn-sm float-left">تسجيل الدخول
            <br/>
                כניסה
            </a>
            <!--
            <div class="btn-group float-left">
                <a href="{{url('/login')}}" class="btn btn-primary btn-login btn-sm float-left">כניסה </a>
            </div> -->

                @else
                <a href="{{url('/weblogin')}}" class="btn btn-primary btn-login btn-sm float-left">
                    ملفي الشخصي
                    <br/>
                    הפרופיל שלי
                </a>

                <a href="{{url('/logout')}}" class="btn btn-primary btn-login btn-sm float-left">
                    تسجيل الخروج
                    <br/>
                    התנתק
                </a>
                @endif
        </div>
    </div><!-- /row -->


</div><!-- /cotainer -->
    <!-- Main Content -->
    @yield('content')

<footer>
    <div class="container">
        <div class="row mt-4 mb-4 sitemap">

            <div class="col-md col-6 mb-1">
                <div class="head">
                    روابط مهمة
<br/>
                    קישורים חשובים
                </div>
                <a href="" class="link">
                    الرئيسية
                    <br/>
                    בית
                </a>
                <a  href="" class="link">
                    أضف مصلحة
                    <br/>
                    הוסף ריבית
                </a>


                <a  href="#" class="link">
                    المقالات
                    <br/>
                    מאמרים
                </a>


                <a  href="#" class="link">
                    قائمة الاسعار
                    <br/>
                    מחירון
                </a>


                <a  href="#" class="link">
                    الاراء
                    <br/>
                    דעות
                </a>

            </div>

            @if(count($footer_links) > 0)
            <div class="col-md col-6 mb-1">
                <div class="head">
                    {!! $footer_links[0]['section1_title'] !!}<br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section1_title','he') }}
                </div>

                @if($footer_links[0]['section1_text1'])
                <a  href="{{$footer_links[0]['section1_link1'] }}" class="link">
                    {{ $footer_links[0]['section1_text1'] }}<br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section1_text1','he') }}
                </a>
                @endif

                @if($footer_links[0]['section1_text2'])
                <a  href="{{$footer_links[0]['section1_link2'] }}" class="link">
                    {!! $footer_links[0]['section1_text2'] !!}<br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section1_text2','he') }}
                </a>
                @endif

                @if($footer_links[0]['section1_text3'])
                <a  href="{{$footer_links[0]['section1_link3'] }}" class="link">
                    {!! $footer_links[0]['section1_text3'] !!}<br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section1_text3','he') }}
                </a>
                @endif

                @if($footer_links[0]['section1_text4'])
                <a  href="{{$footer_links[0]['section1_link4'] }}" class="link">
                    {!! $footer_links[0]['section1_text4'] !!}<br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section1_text4','he') }}
                </a>
                @endif

                @if($footer_links[0]['	section1_text5'])
                <a  href="{{$footer_links[0]['section1_link5'] }}" class="link">
                    {!! $footer_links[0]['section1_text5'] !!}<br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section1_text5','he') }}
                </a>
                @endif

            </div>

            <div class="col-md col-6 mb-1">
                <div class="head">
                    {!! $footer_links[0]['section2_title'] !!}<br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section2_title','he') }}
                </div>

                @if($footer_links[0]['section2_text1'])
                    <a  href="{{$footer_links[0]['section2_link1'] }}" class="link">
                        {!! $footer_links[0]['section2_text1'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section2_text1','he') }}
                    </a>
                @endif

                @if($footer_links[0]['section2_text2'])
                    <a  href="{{$footer_links[0]['section2_link2'] }}" class="link">
                        {!! $footer_links[0]['section2_text2'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section2_text2','he') }}
                    </a>
                @endif

                @if($footer_links[0]['section2_text3'])
                    <a  href="{{$footer_links[0]['section2_link3'] }}" class="link">
                        {!! $footer_links[0]['section2_text3'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section2_text3','he') }}
                    </a>
                @endif

                @if($footer_links[0]['section2_text4'])
                    <a  href="{{$footer_links[0]['section2_link4'] }}" class="link">
                        {!! $footer_links[0]['section2_text4'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section2_text4','he') }}
                    </a>
                @endif

                @if($footer_links[0]['section2_text5'])
                    <a  href="{{$footer_links[0]['section2_link5'] }}" class="link">
                        {!! $footer_links[0]['section2_text5'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section2_text5','he') }}
                    </a>
                @endif

            </div>

            <div class="col-md col-6 mb-1">
                <div class="head">
                    {!! $footer_links[0]['section3_title'] !!}<br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section3_title','he') }}
                </div>
                    @if($footer_links[0]['section3_text1'])
                        <a  href="{{$footer_links[0]['section3_link1'] }}" class="link">
                            {!! $footer_links[0]['section3_text1'] !!}<br/>
                            {{ $footer_links[0]->getTranslatedAttribute('section3_text1','he') }}
                        </a>
                    @endif

                    @if($footer_links[0]['section3_text2'])
                        <a  href="{{$footer_links[0]['section3_link2'] }}" class="link">
                            {!! $footer_links[0]['section3_text2'] !!}<br/>
                            {{ $footer_links[0]->getTranslatedAttribute('section3_text2','he') }}
                        </a>
                    @endif

                    @if($footer_links[0]['section3_text3'])
                        <a  href="{{$footer_links[0]['section3_link3'] }}" class="link">
                            {!! $footer_links[0]['section3_text3'] !!}<br/>
                            {{ $footer_links[0]->getTranslatedAttribute('section3_text3','he') }}
                        </a>
                    @endif

                    @if($footer_links[0]['section3_text4'])
                        <a  href="{{$footer_links[0]['section3_link4'] }}" class="link">
                            {!! $footer_links[0]['section3_text4'] !!}<br/>
                            {{ $footer_links[0]->getTranslatedAttribute('section3_text4','he') }}
                        </a>
                    @endif

                    @if($footer_links[0]['section3_text5'])
                        <a  href="{{$footer_links[0]['section3_link5'] }}" class="link">
                            {!! $footer_links[0]['section3_text5'] !!}<br/>
                            {{ $footer_links[0]->getTranslatedAttribute('section3_text5','he') }}
                        </a>
                    @endif


            </div>

            <div class="col-md col-6 mb-1">
                <div class="head">
                    {!! $footer_links[0]['section4_title'] !!}
                    <br/>
                    {{ $footer_links[0]->getTranslatedAttribute('section4_title','he') }}
                </div>

                @if($footer_links[0]['section4_text1'])
                    <a  href="{{$footer_links[0]['section4_link1'] }}" class="link">
                        {!! $footer_links[0]['section4_text1'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section4_text1','he') }}
                    </a>
                @endif

                @if($footer_links[0]['section4_text2'])
                    <a  href="{{$footer_links[0]['section4_link2'] }}" class="link">
                        {!! $footer_links[0]['section4_text2'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section4_text2','he') }}
                    </a>
                @endif

                @if($footer_links[0]['section4_text3'])
                    <a  href="{{$footer_links[0]['section4_link3'] }}" class="link">
                        {!! $footer_links[0]['section4_text3'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section4_text3','he') }}
                    </a>
                @endif

                @if($footer_links[0]['section4_text4'])
                    <a  href="{{$footer_links[0]['section4_link4'] }}" class="link">
                        {!! $footer_links[0]['section4_text4'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section4_text4','he') }}
                    </a>
                @endif

                @if($footer_links[0]['section4_text5'])
                    <a  href="{{$footer_links[0]['section4_link5'] }}" class="link">
                        {!! $footer_links[0]['section4_text5'] !!}<br/>
                        {{ $footer_links[0]->getTranslatedAttribute('section4_text5','he') }}
                    </a>
                @endif



            </div>
                @endif
        </div>

        <div class="row">
            <div class="col-md-3 col-xs-12 pull-right mrg-btm-10">
                جميع الحقوق محفوظة
                <strong>الدليل</strong>
                &copy; 2019


                <br/>
                כל הזכויות שמורות © 2019
            </div>
            <div class="col-md-6 col-xs-12 mx-auto mrg-btm-10 ">
                <div class="row justify-content-center">
                    تواصل معنا من خلال قنواتنا التالية :
                    <br/>
                    צרו קשר דרך הערוצים הבאים:
                </div>

                <div class="row justify-content-center social">
                    <a href="http://instagram.com" target="_blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a href="https://www.facebook.com" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="https://twitter.com/?lang=en" target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
            </div>

            <div class="col-md-3 col-xs-12 pull-left mrg-btm-10">
                <a href="" class="pull-left">تصميم وتطوير موقع


                    <span>
                        <img src="/web/dalil/images/nadlogo.jpg">
                    </span>

                </a>





            </div>
        </div>
    </div><!-- /cotainer -->
</footer>



<!--
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

-->

<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="{{url('/web/dalil/js/bootstrap.min.js')}}"></script>
<script src="{{url('/web/dalil/js/fancybox/dist/jquery.fancybox.min.js')}}"></script>


<script>
    $(document).ready(function(){
        $(function () {

            $('#errorsmodal').modal('show');
            $('[data-toggle="tooltip"]').tooltip()
        });
		
		setTimeout(function() {$('#errorsmodal').modal('hide');}, 5000);
    });
</script>

</body>
</html>
