@extends('dalil.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#home').addClass('active');
        });
    </script>


    <div class="container">
        <div class="row">
            <div class="col-md-12" id="section1home" >
                <div class="row maintop mrg-top-20">

                    @php
                        $active = 'active';
                    @endphp

                    <div id="carouselhome" class="carousel slide carousel-fade cat-slider" data-ride="carousel" style="width: 100%; max-width: 100%;">
                        <div class="carousel-inner ">
                            @foreach($slider as $slide)
                                <div class="carousel-item {{$active}} col-md-12">
                    <div class="col-md-6  mrg-btm-15 txt">

                        <div class="ar-title">
                            {{$slide->title_ar}}
                        </div>
                        <div class="he-title">
                            {{$slide->title_he}}
                        </div>

                    </div>
                    <div class="col-md-6  mrg-btm-15 pic">
                                        <img src="{{url('storage/'.$slide->image)}}" />
                    </div>

                            @php
                                $active = '';
                            @endphp
                                </div>
                            @endforeach

                    </div>

                        <ol class="carousel-indicators">
                            @php
                                $index = 0;
                            @endphp
                            @foreach($slider as $slide)
                                @if($index == 0)
                                 <li data-target="#carouselhome" data-slide-to="{{$index}}" class="active"></li>
                                @else
                                <li data-target="#carouselhome" data-slide-to="{{$index}}"></li>
                                @endif
                                    @php $index ++; @endphp
                            @endforeach
                        </ol>

                        <a class="carousel-control-prev" href="#carouselhome" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">
                                <i class="fas fa-angle-right"></i>
                                </span>
                        </a>
                        <a class="carousel-control-next" href="#carouselhome" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only"><i class="fas fa-angle-left"></i></span>
                        </a>

                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="row "  id="section2home" >
                    <div class="col-md-6  mrg-btm-15 txt" id="searchdiv">
                        <form action="{{ route('search') }}" id="searchform" method="get" name="searchform" >
                            {{ csrf_field() }}
                            <div class="input-group mt-4 search ">
                                <input type="text" class="form-control"   name="query" placeholder="ابحث عن شركات"  id="searchinput" aria-label="" aria-describedby="" required>
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary search-btn" type="submit" id="button-addon1">
                                        <i class="fas fa-search"></i>
                                        ابحث<br>
                                        חיפוש
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 col-12 mrg-btm-15 ">
                    </div>
                </div>
            </div>

        </div><!-- /row -->

        <div class="row" id="homebanners">
            @php
            $index = 0;
            @endphp

            @foreach($banners as $banner)
                @if($index==0)
            <div class="col-md-8" style="max-height: 200px; overflow: hidden">
                <img src="{{url('storage/'.$banner['image'])}}"  style="height: 100%" >
            </div>
                    @php
                        $index = 1;
                    @endphp
                    @else
                    <div class="col-md-4" style="max-height: 200px; overflow: hidden">
                        <img src="{{url('storage/'.$banner['image'])}}" style="width: 100%" >
                    </div>
                @endif


                @endforeach

        </div><!-- /row -->

        <div class="row " id="maicatshome">
            <div class="col-md-12 col-12 title mt-4 mb-4">
                تصنيفات الدليل<br>
                <span class="he">תוכן המדריך</span>

            </div>
        </div>
        <div class="row " id="catshome">
            @foreach($categories as $category)
            <div class="col-md-3 col-6 mb-1 p-1" >

                    <a href="{{url('/category/'.$category->slug)}}" class="catBlock" >
                     
                        <div class="col-md-12 pic text-right" style=" z-index: 3;">
						<span class="imgchome" style="background-image:url('{{url('storage/'.$category->picture)}}')"></span>
                        <div class="name" style="    margin-top: 15px;">
                            {{$category->name}}
                        </div>
                        <div class="name he">
                           {{$category->getTranslatedAttribute('name','he')}}

                        </div>
                        </div>
               
                    </a>


            </div>
           @endforeach
        </div>

        <div class="row ">
            <div class="col-md-12 col-12 title mt-4 mb-4">
                أرقام هواتف مهمة<br>
                מספרים חשובים
            </div>
        </div>

        <div class="row justify-content-center mb-3">
            <div class="col-10 text-center" id="phonesecdesc">

                @if($pagephones != null)
                    {!! $pagephones->body !!}

                {!! $pagephones->getTranslatedAttribute('body','he') !!}
    @endif
</div>
</div>

<div class="row justify-content-center" id="directorieshome">
<div class="col-12 ">
<div class="row">

    <div class="col-mdcustom-3 col-12 mb-1 p-2">
	<div class="head pink col-md-12">
                <span class="ar">البلديات</span>
                <span class="he">עיריות</span>
            </div>
        <div class="phoneBlock">

            


            @foreach($directories_col1 as $directories1)
            <div class="mt-2">
                <div class="col-md-12 name">
                    {{$directories1->fullname}}
                </div>
                <div class="col-md-12 name ">
                    {{ $directories1->getTranslatedAttribute('fullname','he') }}
                </div>

                <div class="col-md-12 phone" style="direction: ltr;">
                    {{$directories1->phone}}
                </div>
                @if($directories1->location !="")
                <div class="col-md-12 location" >
                    <a href="{{$directories1->location}}" target="_blank">
                        <img src="/web/dalil/images/icon-visit.png">
                        الى الموقع / לאתר

                    </a>
                </div>
                @endif

            </div>

            @endforeach


        </div>
    </div>

    <div class="col-mdcustom-3 col-12 mb-1 p-2">
	<div class="head green col-md-12">
                <span class="ar">عيادات ومراكز طبية</span>
                <span class="he">מרפאות ומרכזים רפואיים</span>
            </div>
        <div class="phoneBlock">
            


            @foreach($directories_col2 as $directories2)
                <div class=" mt-2">
                    <div class="col-md-12 name">
                        {{$directories2->full_tilte}}
                    </div>
                    <div class="col-md-12 name ">
                        {{ $directories2->getTranslatedAttribute('full_tilte','he') }}
                    </div>



                    <div class="col-md-12 phone" style="direction: ltr;    ">
                        {{$directories2->phone}}
                  </div>

                    @if($directories2->location !="")
                    <div class="col-md-12 location" >
                        <a href="{{$directories2->location}}" target="_blank">
                            <img src="/web/dalil/images/icon-visit.png">
                            الى الموقع / לאתר


                        </a>
                    </div>
                        @endif

                </div>

            @endforeach

        </div>
    </div>


    <div class="col-mdcustom-3 col-12 mb-1 p-2">
	 <div class="head skyblue col-md-12">
                <span class="ar">مؤسسات تعليمية</span>
                <span class="he">מוסדות חינוך</span>
            </div>

        <div class="phoneBlock">
           
            @foreach($directories_col3 as $directories3)
                <div class=" mt-2">
                    <div class="col-md-12 name">
                        {{$directories3->fullname}}
                    </div>
                    <div class="col-md-12 name ">
                        {{ $directories3->getTranslatedAttribute('fullname','he') }}
                    </div>



                    <div class="col-md-12 phone" style="direction: ltr;     ">
                        {{$directories3->phone}}
                   </div>
                    @if($directories3->location !="")
                    <div class="col-md-12 location" >
                        <a href="{{$directories3->location}}" target="_blank">
                            <img src="/web/dalil/images/icon-visit.png">
                            الى الموقع / לאתר

                        </a>
                    </div>

                        @endif
                </div>

            @endforeach

        </div>
    </div>

    <div class="col-mdcustom-3 col-12 mb-1 p-2">
	<div class="head purpile col-md-12">
                <span class="ar">شرطة و اطفائية</span>
                <span class="he">
                    משטרה וכבאות

                </span>
            </div>

        <div class="phoneBlock">
            

            @foreach($directories_col4 as $directories4)
                <div class=" mt-2">
                    <div class="col-md-12 name">
                        {{$directories4->fullname}}
                    </div>
                    <div class="col-md-12 name ">
                        {{ $directories4->getTranslatedAttribute('fullname','he') }}
                    </div>



                    <div class="col-md-12 phone" style="direction: ltr;     text-align: center;">
                        {{$directories4->phone}}
                   </div>

                    @if($directories4->location !="")
                    <div class="col-md-12 location" >
                        <a href="{{$directories4->location}}" target="_blank">
                            <img src="/web/dalil/images/icon-visit.png">
                            الى الموقع / לאתר
                        </a>
                    </div>
                        @endif
                </div>

            @endforeach

        </div>
    </div>

    <div class="col-mdcustom-3 col-12 mb-1 p-2">
	 <div class="head yeloow col-md-12">
                <span class="ar">مكاتب حكومية</span>
                <span class="he">משרדי ממשלה</span>
            </div>
        <div class="phoneBlock">
           


            @foreach($directories_col5 as $directories5)
                <div class=" mt-2">
                    <div class="col-md-12 name">
                        {{$directories5->fullname}}
                    </div>
                    <div class="col-md-12 name ">
                        {{ $directories5->getTranslatedAttribute('fullname','he') }}
                    </div>



                    <div class="col-md-12 phone" style="direction: ltr;     ">
                        {{$directories5->phone}}
                    </div>

                    @if($directories5->location !="")
                    <div class="col-md-12 location" >

                        <a href="{{$directories5->location}}" target="_blank">
                            <img src="/web/dalil/images/icon-visit.png">
                            الى الموقع / לאתר

                        </a>
                    </div>

                        @endif
                </div>

            @endforeach
        </div>
    </div>





</div>
</div>

</div>


</div><!-- /cotainer -->

@endsection
