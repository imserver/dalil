@extends('dalil.layouts.Web_app')



@section('content')


    <div class="container">
    <div class="row" id="loginarea">
        <div class="row  col-md-12 mb-3 login-pg justify-content-center" id="loginareainner">

            <div class="col-md-7 col-12">
            <div class="col-md-7 col-12 r-side  " id="intrologin" >
                <div class="row title">مرحباً بك !
                <br/>
                    שלום לך !
                </div>

                <div class="row desc mt-3 pl-5">
                    هنا يمكنك الدخول الى الشركة التي قمنا بانشاءها لك و تعديل محتوياتها كما تريد

                    <br/>
                    כאן תוכלו להיכנס לחברה שיצרנו עבורכם ולשנות את תוכנה כרצונכם
                </div>
            </div>
            </div>
            <div class="col-md-5 col-12 login-form p-5">
                <div class="row title justify-content-center mb-4">تسجيل الدخول
                <br/>
                    התחבר
                </div>
                <form method="POST" action="{{ route('weblogin') }}">
                    @csrf
                    <div class="form-group">
                        <label>اسم المستخدم
                        <br/>
                            שם המשתמש
                        </label>
                        <input type="text"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="اسم المستخدم الذي تم تزويدك به">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>كلمة المرور
                        <br/>
                            סיסמא
                        </label>


                        <input id="password" placeholder="******" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="row justify-content-center pb-3 mt-5">
                        <button class="col-7 btn btn-primary" type="submit">دخول
                        <br/>
                            כניסה
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    </div>

@endsection
