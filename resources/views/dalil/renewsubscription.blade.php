@extends('dalil.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
         //   $('li#about').addClass('active');
        });
    </script>


    <div class="row contact-pg1 m-4">

        <div class="col-md-12 col-12 contact-form p-5">
            <div class="row title justify-content-center mb-4">
                تجديد الإشتراك
                <br/>
                חידוש מנוי
            </div>

            <form action="{{ route('rerequest') }}" id="renewrequest" method="post" name="renewrequest" >
                {{ csrf_field() }}
                <div class="form-row">


                    <div class="form-group col-md-6 col-12 mt-3">
                        <label>

                            البريد الإلكتروني


                            <br/>
                            דואר אלקטרוני
                        </label>
                        <input type="text" class="form-control" required placeholder="">
                    </div>
                    <div class="form-group col-md-6 col-12 mt-3">
                        <label>محتوى الرسالة
                            <br/>
                            תוכן ההודעה
                        </label>
                        <input type="text" class="form-control" placeholder="">
                    </div>
                    <button class="col-md-4 offset-md-4 col-12 mt-3 btn btn-primary " type="submit">
                        أرسل
                        <br/>
                        שלח

                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
