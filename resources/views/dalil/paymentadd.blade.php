@extends('dalil.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#about').addClass('active');
        });
    </script>


    <div class="container about" id="content">

        <div class="row">
            <div class=" intro pt-5 p-4">
                <div class="col-12">
                    <div class="cat-title">

                        من نحن
                        <br/>
                        מי אנחנו

                       </div>
                </div>
                @if(count($about) > 0)
                <div class="col-12">
                    <!-- first section -->
                    {!! $about[0]['section1'] !!}
                    <br/>
                    {!! $about[0]['translations'][3]['value'] !!}
                </div>
                    @endif
            </div>
        </div><!-- /row -->

        @if(count($about) > 0)

        <div class="row">
            <div class=" started mt-5 pb-5">
                <div class="col-12 mb-3">
                    <div class="cat-title">
                        {!! $about[0]['sect1_title'] !!}
                        <br/>
                        {!! $about[0]['translations'][1]['value'] !!}

                    </div>
                </div>
                <div class="col-md-6 col-12 float-right">
                    <!-- Seconed Section -->
                    {!! $about[0]['section2'] !!}
                    <br/>
                    {!! $about[0]['translations'][4]['value'] !!}
                </div>
                <div class="col-md-6 col-12 float-left">
                    <img src="/web/dalil/images/building-city-town-icon-png--3.png" class="img-fluid">
                </div>
            </div>
        </div><!-- /row -->

        <div class="row">
            <div class="col-12 mb-3">
                <div class="cat-title">

                    {!! $about[0]['sect3_title'] !!}
                    <br/>
                    {!! $about[0]['translations'][2]['value'] !!}

                </div>
            </div>
            <div class="col-12">
                <!-- third section -->
                {!! $about[0]['fulldescription'] !!}
                <br/>
                {!! $about[0]['translations'][0]['value'] !!}
            </div>
        </div><!-- /row -->

        @endif
    </div><!-- /cotainer -->


@endsection
