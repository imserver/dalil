@extends('dalil.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            //$('li#about').addClass('active');
        });
    </script>

    <div class="container search-bg">
        <div class="row justify-content-center mb-5">
            <div class="col-12">
                <div class="title pr-5 pl-5 pt-5">
                    نتائج البحث

                <br/>
                    תוצאות חיפוש
                </div>
            </div>
            <div class="col-md-7 col-10 ">
                <form action="{{ route('search') }}" id="searchform" method="get" name="searchform" >
                    {{ csrf_field() }}
                    <div class="input-group mt-4 search ">

                        <input type="text" class="form-control"  name="query"  placeholder="ابحث عن شركات" aria-label="" aria-describedby="" required id="searchinput">
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary search-btn" type="submit" id="button-addon1">
                                <i class="fas fa-search"></i>
                                ابحث<br>
                                חיפוש
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div><!-- /row -->


        <div class="row">
            <div class="col">
               @foreach($posts as $post)
                <div class="col-md-3 col mb-4 pr-1 float-right">
                    <div class="catBlock blue">
                        <a href="{{url('details/'.$post->slug)}}" >
                            <div class="row mb-3 border-bottom m-2 pb-2">
                                <div class="col-4">
                                    <img src="{{url('storage/'.$post->image)}}" class="img-fluid rounded-circle">
                                </div>
                                <div class="col-8">
                                    <div class="name">
                                        {{$post->title}}
                                       </div>
                                    <div class="rating">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3 address">
                                <div class="col-2 pl-1" style="text-align:center"><i class="fas fa-map-marker-alt float-left"></i></div>
                                <div class="col-10" style=" line-height:42px;    width: 100%; overflow: hidden;">

                                    {{$post->location}}

                                </div>
                            </div>
                            <div class="row mb-3 phone">
                                <div class="col-2 pl-1" style="text-align:center"><i class="fas fa-phone-alt float-left"></i></div>
                                <div class="col-10" style=" line-height:22px;    width: 100%; overflow: hidden;">

                                    {{$post->phone}}
                                  </div>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach

            </div>
        </div>


    </div><!-- /cotainer -->

@endsection
