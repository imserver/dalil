@extends('index')

@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

<div class="container">
    @include('theme.banners.big')
</div>

@endsection

@section('content')

  <div class="container">
      <div class="row">
          <div class="col-md-12 col-xs-12 mrg-top-10 no-pd-left">

              <div class="row">
                  <div class="col-xs-12 ">
                    <div class="block shadow media mrg-btm-20">
                      @component('components.breadcrumbs')
                            <a href="{{ route('home') }}" title="الرئيسية" class="home">الرئيسية</a>
                            <i class="fa fa-angle-double-left"></i>
                            <a href="{{ route('video.showCategory',$category->slug) }}" title="{{ $category->title }}">{{ $category->title }}</a>
                            <i class="fa fa-angle-double-left"></i>
                            <span class="current">{{ $video->title }}</span>
                        @endcomponent

                      <div class="row">
                          <div class="col-xs-12">
                              <h1 class="article-h1">{{ $video->title }}</h1>
                          </div>
                          <div class="col-xs-12 mrg-btm-10 art-date">
                              {{ $video->created_at->format('d.m.Y') }} /
                               الساعة {{ $video->created_at->format('H:i') }} بتوقيت القدس
                          </div>
                      </div>
                      <!-- Go to www.addthis.com/dashboard to customize your tools -->
                          <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f8844ec57f383b2"></script>

                          <!-- Go to www.addthis.com/dashboard to customize your tools -->
                          <div class="addthis_inline_share_toolbox mrg-btm-15 no-print"></div>
                          <script type="text/javascript">
                          var addthis_config = addthis_config||{};
                          addthis_config.lang = 'ar' //show in Spanish regardless of browser settings;
                          </script>

                      <article>
                            <div class="main-video">
                                <img class="img-responsive" src="{{ getVidImage($video->image,$video->crop_image,1280,720) }}">
                                <div class="play"></div>
                                {!! $videoUrl !!}
                            </div>
                            {!! $video->details !!}

                      </article>

                  </div>
                  </div>
              </div>
          </div>

      </div><!-- /row -->
  </div>
  @endsection

  @section('javascripts')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>

    $(".main-video").click(function () {
                $(".video-content").show();
                $(".video-content").attr("src", $(".video-content").attr("src") + "&autoplay=1");

                $(".video-content-file").show();
                $(".video-content-file").attr("src", $(".video-content-file").attr("src"));
            });
    </script>
@endsection
