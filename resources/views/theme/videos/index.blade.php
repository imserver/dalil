@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

    <div class="container">
        @include('theme.banners.big')
    </div>

@endsection

@section('content')
  <div class="container">
      <div class="row mrg-top-20">
          <div class="col-md-12 col-xs-12 no-pd-left">
              <div class="row">
                  <div class="col-xs-12">
                      @foreach ($categories as $cat)


                    <div class="block shadow mrg-btm-20 media">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb pull-right col-xs-12">
                                <a href="{{ route('video.showCategory',$cat->slug) }}" class="cat-subtitle pull-right">{{ $cat->title }}</a>
                            </div>
                        </div>

                        @foreach ($subCats[$cat->id] as $subcat)
                            <div class="col-md-3 col-xs-12 mrg-top-10 cat-item">
                                <a href="{{ route('video.showCategory',$subcat->slug) }}">
                                    <img src="{{ getVidImage($subcat->picture,null,480,360) }}" class="img-responsive">
                                    <div class="txt mrg-top-10">{{ $subcat->title }}</div>
                                </a>
                            </div>
                      @endforeach

                      @foreach ($videos[$cat->id] as $video)
                        <div class="col-md-3 col-xs-12 mrg-top-10 cat-item">
                            <a href="{{ route('video.showVideo',$video->slug) }}">
                                <img src="{{ getVidImage($video->image,$video->crop_image,480,360) }}" class="img-responsive">
                                <div class="play"></div>
                                <div class="txt mrg-top-10">{{ $video->title }}</div>
                            </a>
                        </div>
                      @endforeach
                  </div>

                  @endforeach

                  </div>
              </div>


          </div>
      </div>
  </div>
  @endsection
