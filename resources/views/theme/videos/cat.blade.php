@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

    <div class="container">
        @include('theme.banners.big')
    </div>

@endsection

@section('content')
  <div class="container">
      <div class="row mrg-top-20">
          <div class="col-md-12 col-xs-12 no-pd-left">
              <div class="row">
                  <div class="col-xs-12 mrg-btm-200">
                    <div class="block shadow media">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb pull-right col-xs-12">
                                <a class="cat-subtitle pull-right">{{ $categoryName }}</a>
                            </div>
                        </div>


                      @foreach ($videos as $video)
                            <div class="col-md-3 col-xs-12 mrg-top-10 cat-item">
                                <a href="{{ route('video.showVideo',$video->slug) }}">
                                    <img src="{{ getVidImage($video->image,$video->crop_image,480,360) }}" class="img-responsive">
                                    <div class="play"></div>
                                    <div class="txt mrg-top-10">{{ $video->title }}</div>
                                </a>
                            </div>
                      @endforeach
                  </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-12 text-center pull-right">
                      <nav aria-label="Page navigation">
                        <ul class="pagination">
                                {{ $videos->render() }}
                        </ul>
                      </nav>
                  </div>
              </div>
          </div>
      </div>
  </div>
  @endsection
