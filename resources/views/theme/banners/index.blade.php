@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('content')

  <div class="container">
      <div class="row">
          <div class="col-md-12 col-xs-12 mrg-top-10 mrg-btm-20">

              <div class="row">
                  <div class="col-xs-12 block shadow">
                    @if(!empty($banner))
                      <div class="row">
                          <div class="col-xs-12">
                              <h1 class="article-h1">{{ $banner->title }}</h1>
                          </div>
                      </div>
                        @if($banner->image)
                        <img src="{{ asset('storage/'.$banner->page) }}" class="img-responsive center-block" >
                        @endif
                      @endif
                  </div>
              </div>
          </div>

      </div><!-- /row -->
  </div>
  @endsection
