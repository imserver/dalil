@extends('index')

@section('big-banner')

<div class="container">
    <div class="pull-right mrg-top-20">
    @include('theme.banners.big')
    </div>
</div>

@endsection

@section('header')

    @include('theme.partials.header-index')

@endsection


@section('content')

<div class="main-slider">
    <div class="container">
        @include('theme.partials.main-news')
    </div>
</div>

<div class="container">

    <div class="row">
        <div class="col-xs-12 mrg-btm-10 mrg-top-10">
            <div class="bnrslider ">
                @include('theme.partials.bnrslider')
            </div>
        </div>
    </div>
    <div class="row">
          <div class="col-md-7 col-xs-12 mrg-btm-10">

              <div class="row">
                <div class="col-xs-12">
                  @include('theme.partials.media')
                </div>
              </div>

              @foreach ($ThreeInRowPlugin as $cat)

              @php
                    if (sizeof($midBanner)>$midBnrCount+1) $midBnrCount++;
                    else $midBnrCount=0;
                @endphp

                @include('theme.catPlugins.' .$cat->main_design )

                @if ($loop->first)
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-12 mrg-btm-15 block shadow">

                                @include('theme.partials.social-main')

                            </div>

                        </div>
                    </div>
                @endif
              @endforeach

              @foreach ($oneThreePlugin as $cat)

                @include('theme.catPlugins.' .$cat->main_design )

            @endforeach

            </div>
            <div class="col-md-5 col-xs-12 mrg-btm-10">

              <div class="row">
                <div class="col-md-4 col-xs-12">

                  @foreach ($ArchievePlugin as $cat)
                    @include('theme.catPlugins.' .$cat->main_design )
                  @endforeach

                  @include('theme.partials.polls')

                  @include('theme.banners.tall')

                </div>
                <div class="col-md-8 col-xs-12">
                  <div class="mrg-btm-10">
                    @php
                        if (sizeof($cubeBanner)>$cubeBnrCount+1) $cubeBnrCount++;
                        else $cubeBnrCount=0;
                    @endphp
                    @include('theme.banners.cubes')

                  </div>

                  @include('theme.partials.horos')

                  @for($i=0;$i<5;$i++)
                  @php
                    if (sizeof($cubeBanner)>$cubeBnrCount+1) $cubeBnrCount++;
                    else $cubeBnrCount=0;
                    @endphp

                        @include('theme.banners.cubes')

                    @endfor

                  </div>


                </div>
              </div>


            </div>


        @php
            if (sizeof($bigBanner)>$bigBnrCount+1) $bigBnrCount++;
            else $bigBnrCount=0;
        @endphp
        @include('theme.banners.big')


        @foreach ($fiveArtPlugin as $cat)
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 cat-index cat-5 mrg-btm-20 mrg-top-20 block shadow">
                    <div class="row">

                        @include('theme.catPlugins.' .$cat->main_design )
                    </div>
                </div>

            </div>
        </div>

        @endforeach


        @php
            if (sizeof($bigBanner)>$bigBnrCount+1) $bigBnrCount++;
            else $bigBnrCount=0;
        @endphp
        @include('theme.banners.big')

        <div class="row">
            <div class="col-xs-8">

                @foreach ($SateeraPlugin as $cat)

                @include('theme.catPlugins.' .$cat->main_design )

                @endforeach

                </div>

            <div class="col-md-4 col-xs-12 mrg-btm-10 mrg-top-20">
                @php
                    if (sizeof($cubeBanner)>$cubeBnrCount+1) $cubeBnrCount++;
                    else $cubeBnrCount=0;
                @endphp
                @include('theme.banners.cubes')

            </div>

        </div>




    </div>
        @foreach ($ExtraPlugin as $cat)
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 block shadow">
                    <div class="row">

                    @include('theme.catPlugins.' .$cat->main_design )
                </div>
            </div>
            </div>
        </div>

        @endforeach

@endsection

