
<div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 mrg-btm-10">
                <div class="cat-title">
                    <a href="{{ route('post.showCategory',$cat->slug) }}">{{ $cat->name }}</a>
                    <a href="{{ route('post.addArt') }}" class="btn btn-danger send" style="font-size: 18px">أرسل خبر</a>
                </div>
            </div>
        </div>
        <div class="row ">

        @foreach ($fiveArtPost[$cat->id] as $post)
        <div class="col-md-25 col-xs-12 ">
                <a href="{{ route('post.showPost',$post->slug) }}" class="cat-news">
                    <img src="{{ getImage($post->image,$post->crop_image,550,337) }}" class="img-responsive">
                    <div class="title">{{ str_limit($post->title,100) }}</div>
                    <div class="author">
                        الكاتب: <span>{{ $post->author }}</span>
                    </div>
                 </a>
            </div>
        @endforeach

        </div>
    </div>
