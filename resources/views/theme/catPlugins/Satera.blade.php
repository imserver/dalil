<div class="col-xs-12 cat-index one-3 mrg-btm-15 mrg-top-20 block satera">
    <div class="row">
        <div class="col-xs-12">
                <div class="col-md-3 col-xs-12 mrg-btm-10 mrg-top-20 no-padding">
                        <a href="{{ route('post.showCategory',$cat->slug) }}">
                        <img src="{{ asset('theme/images/cat-satera.png') }}" class="img-responsive mrg-btm-10">
                    </a>
                </div>
                @foreach ($sateeraPost[$cat->id] as $post)
                <div class="col-md-3 col-xs-12 mrg-btm-10">
                    <a href="{{ route('post.showPost',$post->slug) }}">
                        <img src="{{ getImage($post->image,$post->crop_image,180,120) }}" class="img-responsive mrg-btm-10">
                        <div class="title">{{ str_limit($post->title,100) }}</div>
                    </a>
                </div>
                @endforeach

        </div>

    </div>

</div>
