<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 cat-index mrg-btm-15 block shadow">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 mrg-btm-10">
                            <div class="cat-title">
                                <a href="{{ route('post.showCategory',$cat->slug) }}">{{ $cat->name }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="row ">

                    @foreach ($ThreeInRowPost[$cat->id] as $post)
                        <div class="col-xs-4 ">
                            <a href="{{ route('post.showPost',$post->slug) }}" class="cat-news">
                                <img src="{{ getImage($post->image,$post->crop_image,550,337) }}" class="img-responsive">
                            <div class="title">{{ str_limit($post->title,100) }}</div>
                            </a>
                        </div>
                    @endforeach


                    </div>
                </div>

                @include('theme.banners.small-banner')

            </div>
        </div>

  </div>
</div>
