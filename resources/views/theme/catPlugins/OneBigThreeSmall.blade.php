<div class="col-xs-12 cat-index one-3 mrg-btm-15 mrg-top-20 block shadow">
        <div class="row">
<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-12 mrg-btm-10">
            <div class="cat-title">
                <a href="{{ route('post.showCategory',$cat->slug) }}">{{ $cat->name }}</a>
            </div>
        </div>
    </div>
    <div class="row ">
    @foreach ($threeOneArtPost[$cat->id] as $post)
        @if($loop->first)
        <div class="col-xs-5 ">
            <a href="{{ route('post.showPost',$post->slug) }}" class="cat-news">
                <img src="{{ getImage($post->image,$post->crop_image,550,337) }}" class="img-responsive">
                <div class="title">{{ str_limit($post->title,100) }}</div>
             </a>
        </div>
        @else

        <div class="col-xs-7 item mrg-btm-10">
            <a href="{{ route('post.showPost',$post->slug) }}" class="cat-news">
                <div class="title">{{ str_limit($post->title,100) }}</div>
            </a>
        </div>
        @endif

        @endforeach

    </div>
</div>
        </div>
</div>
