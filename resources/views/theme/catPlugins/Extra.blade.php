<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="{{ asset('theme/js/owl.carousel.min.js') }}"></script>


<div class="extra">
    <div class="main-title"><a href="{{ route('post.showCategory',$cat->slug) }}">{{ $cat->name }}</a></div>
  <div class="extra-carousel">
        @foreach ($ExtraPost[$cat->id] as $post)
    <div class="item">
        <a href="{{ route('post.showPost',$post->slug) }}" >
            <img src="{{ getImage($post->image,$post->crop_image,550,337) }}" class="img-responsive">
            <div class="title mrg-top-10">{{ str_limit($post->title,100) }}</div>
        </a>
    </div>
    @endforeach
  </div>
</div>

<script type="text/javascript">
    (function($) {
   $(document).ready(function() {
         var owl = $('.extra-carousel');
         owl.owlCarousel({
           rtl: true,
           margin: 15,
           dots: false,
           loop: true,
           responsive: {
             0: {
               items: 1
             },
             600: {
               items: 3
             },
             1000: {
               items: 7
             }
           }
         })
       });
}) (jQuery);
</script>
