@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection


@section('big-banner-inner')

<div class="container">
    @include('theme.banners.big')
</div>

@endsection

@section('content')

  <div class="container">
      <div class="row">
          <div class="col-md-8 col-xs-12 mrg-top-20 no-pd-left">

              <div class="row">
                  <div class="col-xs-12 ">
                      <div class="block shadow">

                      <div class="row">
                          <div class="col-xs-12">
                              <h1 class="article-h1">{{ $page->title }}</h1>
                          </div>
                      </div>
                        @if($page->image)
                        <img src="{{ getImage($page->image,$page->crop_image) }}" class="img-responsive art-main" >
                        @endif

                      <article>
                          <p>{!! $page->body !!}</p>
                      </article>

                  </div>
                  </div>
              </div>
          </div><!-- /col-xs-9 -->

          <div class="col-md-4 col-xs-12 mrg-btm-10 mrg-top-20">
              <div class="row">
                    @include('theme.partials.aside')
              </div>
          </div>

      </div><!-- /row -->
  </div>
  @endsection
