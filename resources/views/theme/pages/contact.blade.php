@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

    <div class="container">
        @include('theme.banners.big')
    </div>

@endsection

@section('content')
  <div class="container">
      <div class="row mrg-top-20 mrg-btm-20">
            <div class="col-xs-12 ">
                <div class="block shadow">
                    <div class="col-xs-12 ">
                        <div class="breadcrumb pull-right col-xs-12">
                            <a class="cat-subtitle pull-right">اتصل بنا</a>
                        </div>
                    </div>

                    <div class="col-md-7 col-xs-12">
                        <div class="row">
                            <form action="{{ route('saveContact') }}" id="contform" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div id="contform" class="add-comment">
                                        @if ($errors->any())
                                        <div class="col-md-12 col-xs-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @endif

                                        @if (session('status'))
                                        <div class="col-md-12 col-xs-12">
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-md-12 col-xs-12">
                                            <div class="row">

                                                <div class="col-xs-12  mrg-top-20">
                                                    <input type="text" name="name" class="form-control mrg-btm-15" placeholder="الاسم" required>
                                                </div>
                                                <div class="col-xs-12 ">
                                                    <input type="text" name="phone" class="form-control mrg-btm-15" placeholder="رقم الهاتف">
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="email" class="form-control mrg-btm-15 email" placeholder="البريد الالكتروني" required>
                                                </div>

                                                <div class="col-xs-12 mrg-btm-20 mrg-top-20">
                                                    <textarea name="msg" class="form-control" placeholder="الرسالة" style="height:100px;"></textarea>
                                                </div>
                                                <div class="col-md-2 col-xs-12 pull-left">
                                                    <button class="btn btn-lg btn-cmnt pull-left" type="submit">
                                                            ارسل </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-xs-12 mrg-top-10 mrg-btm-15">
                                    <span class="glyphicon glyphicon-map-marker"></span>&nbsp;{{ setting('contact.address') }}
                                </div>
                                <div class="col-xs-12 mrg-btm-15">
                                    <span class="glyphicon glyphicon-earphone"></span>&nbsp;{{ setting('contact.phone') }}</div>
                                <div class="col-xs-12 mrg-btm-15">
                                        <span class="fa fa-fax"></span>&nbsp;{{ setting('contact.fax') }}</div>
                                <div class="col-xs-12 mrg-btm-15"><span class="fa fa-envelope-o contact"></span><a href="mailto:{{ setting('contact.email') }}">&nbsp;{{ setting('contact.email') }}</a></div>
                                <div class="col-xs-12 mrg-btm-15">
                                    {!! setting('contact.map') !!}
                                </div>
                            </div>
                        </div>

                </div>
            </div>
      </div>
  </div>
  @endsection
