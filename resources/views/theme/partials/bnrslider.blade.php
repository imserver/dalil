<div class="row">
    <div class="col-xs-12 no-padding" id="slider">

            <div class="col-md-12" id="carousel-bounding-box">
                <div id="myCarousel1" class="carousel slide carousel-fade">
                    <!-- main slider carousel items -->
                    <div class="carousel-inner">
                        @foreach ($BannerSlider as $bnr)
                        <div class="@if($loop->first) active @endif item" data-slide-number="{{ $loop->iteration-1 }}">
                            <a href="{{ $bnr->link }}" target="_blank" title="{{ $bnr->title }}">
                                <img src="{{ getImage($bnr->image,$bnr->crop_image,1160,200) }}" class="img-responsive" title="{{ $bnr->title }}" alt="{{ $bnr->title }}">
                            </a>
                        </div>
                        @endforeach

                    </div>
                    <a class="carousel-control left" href="#myCarousel1" data-slide="prev"><img src="{{ asset ('theme/images/arrow2.png') }}"></a>

                    <a class="carousel-control right" href="#myCarousel1" data-slide="next"><img src="{{ asset('theme/images/arrow1.png') }}"></a>
                </div>
            </div>

    </div>
</div>
<!--/main slider carousel-->

<!-- thumb navigation carousel -->
<div class="col-xs-12 mrg-top-15 no-padding" id="slider-thumbs">

        <!-- thumb navigation carousel items -->
  <ul class="list-inline">
        @foreach ($BannerSlider as $bnr)
			<li>
				<a id="carousel-selector-{{ $loop->iteration-1 }}" class="selected" style="cursor: pointer;">
		        	<img src="{{ getImage($bnr->logo,$bnr->crop_logo,142,60) }}" class="img-responsive" title="{{ $bnr->title }}" alt="{{ $bnr->title }}">
		      	</a>
              </li>
            @endforeach
    </ul>

</div>

@section('javascripts')

    <script>
        $(document).ready(function()
    {
        $('#myCarousel1').carousel({
            interval: 4000
        });

        // handles the carousel thumbnails
        $('[id^=carousel-selector-]').click( function(){
          var id_selector = $(this).attr("id");
          var myid = id_selector.length - 18;
          //var id = id_selector.substr(id_selector.length -1);
          var id = id_selector.substr(id_selector.length -myid);
          id = parseInt(id);
          $('#myCarousel1').carousel(id);
          $('[id^=carousel-selector-]').removeClass('selected');
          $(this).addClass('selected');
        });

        // when the carousel slides, auto update
        $('#myCarousel1').on('slid', function (e) {
          var id = $('.item.active').data('slide-number');
          id = parseInt(id);
          $('[id^=carousel-selector-]').removeClass('selected');
          $('[id=carousel-selector-'+id+']').addClass('selected');
        });
    });
    </script>


    @endsection

