@foreach($items as $item)
<a target="{{ $item->target }}" href="{{ url($item->url) }}">{{ $item->title }}</a>
@endforeach
