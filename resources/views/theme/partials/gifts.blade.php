<div class="ehda mrg-btm-10">
    <div class="col-xs-12">
        <div class="title mrg-btm-15">
            <img src="{{ asset('theme/images/ehda-title.png') }}" class="img-responsive">
        </div>
        <div class="row">
            <div class="col-xs-12 ">
                <div class="bdy">
                <div id="ehdaCarousel" class="carousel slide" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner mrg-btm-20">

                        @foreach ($gifts as $gift)
                        <div class="item @if($loop->first) active @endif">
                            <div class="col-xs-12 pull-right"><h2>{{ $gift->title }}</h2></div>
                            <div class="col-xs-12 pull-right">{{ $gift->data }}</div>
                        </div>
                        @endforeach
                    </div>

                    <div class="arrows">
                        <!-- Left and right controls -->
                    <a class="left carousel-control" href="#ehdaCarousel" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#ehdaCarousel" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                        <span class="sr-only">Next</span>
                    </a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
            <a href="#" data-toggle="modal" data-target="#ehdaModal">
                <div class="btn send col-md-4 col-xs-6 mrg-btm-15">
                    أرسل تهنئة
                </div>
            </a>
                <div class="modal fade" id="ehdaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h2 class="modal-title ehda-modal" id="myModalLabel">
                            أرسل تهنئة
                            </h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-xs-12">
                                    <div class="ehda-modal pull-right mrg-top-10">
                                        <div class="row">
                                                <span id="form_output"></span>
                                            <form method="post"  id="addGift">
                                                {{csrf_field()}}

                                                <div class="col-md-12 col-xs-12 no-padding">

                                                    <div class="col-xs-12 mrg-right-10">
                                                        <label>الاسم</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12 ">
                                                            <input type="text" name="name" class="form-control input-xs " id="" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-xs-12 no-padding">

                                                    <div class="col-xs-12 mrg-right-10">
                                                        <label>العنوان</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12 ">
                                                            <input type="text" name="title" class="form-control input-xs" id="" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-xs-12 no-padding">

                                                    <div class="col-xs-12 mrg-right-10">
                                                        <label>الاهداء</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12 ">
                                                            <textarea name="data" class="form-control input-xs" id="" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-xs-12 no-padding">

                                                    <div class="form-group">

                                                        <div class="col-md-12 col-xs-12 center-block mrg-top-15">
                                                            <button type="submit" class="btn add center-block">
                                                                    إرسال
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    </div>
    @section('javascripts')
    <script type="text/javascript">
        $(document).ready(function() {

            $('#addGift').on('submit', function(event){
            event.preventDefault();
            var form_data = $(this).serialize();
            $.ajax({
                url:"{{ route('ajaxGift') }}",
                method:"POST",
                data:form_data,
                dataType:"json",
                success:function(data)
                {
                    if(data.error.length > 0)
                    {
                        var error_html = '';
                        for(var count = 0; count < data.error.length; count++)
                        {
                            error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                        }
                        $('#form_output').html(error_html);
                    }
                    else
                    {
                        $('#addGift')[0].reset();
                        $('#addGift').slideUp('slow');
                        $('#form_output').hide().html(data.success).slideDown('slow');
                        setTimeout(function(){
                            $('#ehdaModal').modal('hide')
                        }, 2000);
                    }
                }
            })
        });

    });

    </script>
    @endsection
