<div class="nav-container">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="col-xs-12">
                <ul class="nav navbar-nav">

                    @foreach($items as $item)
                    @if($item->children->count())
                        <li class="dropdown"><a target="{{ $item->target }}" href="{{ url($item->url) }}" data-toggle="dropdown" class="dropdown-toggle">{{ $item->title }}</a>
                                <ul role="menu" class="dropdown-menu">
                                    @foreach($item->children as $subItem)
                                        <li>
                                            <a target="{{ $subItem->target }}" href="{{ url($subItem->url) }}">{{ $subItem->title }}</a></li>
                                        @if(!$loop->last) <li class="divider"></li> @endif
                                    @endforeach
                                </ul>
                        </li>
                        @else
                        <li><a target="{{ $item->target }}" href="{{ url($item->url) }}">{{ $item->title }}</a></li>
                        @endif
                    @endforeach

                </ul>
            </div>
            <div class="col-xs-2"></div>

        </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

