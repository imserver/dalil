@if(!empty($postSlider))
<div class="row">
    <div class="col-xs-12">
        <div class=" main_news mrg-btm-5">

            <div id="carousel-main_news" class="carousel slide mrg-btm-5 carousel-fade" data-ride="carousel">
            	<div class="col-xs-12">
	            	<!-- Wrapper for slides -->
	                <div class="carousel-inner" role="listbox">

						@foreach ($postSlider as $post)
						<div class="item @if($loop->first) active @endif">
							<a href="{{ route('post.showPost',$post->slug) }}">
								<div class="col-xs-6 no-padding img"><img src="{{ getImage($post->image,$post->crop_image,780,520) }}" class="img-responsive"></div>
								<div class="col-xs-6 no-padding">
									<h1 class="title mrg-btm-15">{{ str_limit($post->title,100) }}</h1>
									<div class=" details">{{ str_limit($post->excerpt,100) }}</div>

								</div>
							</a>
						</div>
						@endforeach
	                </div>

	                <!-- Indicators -->
	                <div class="indicators">
	                    <div class="indic">
	                        <ol  class="carousel-indicators">
	                        <li data-target="#carousel-main_news" data-slide-to="0" class="active">1
	                        </li>
	                        <li data-target="#carousel-main_news" data-slide-to="1" class="">
	                            2
	                        </li>
	                        <li data-target="#carousel-main_news" data-slide-to="2" class="">
	                            3
	                        </li>
	                        <li data-target="#carousel-main_news" data-slide-to="3" class="">
	                            4
	                        </li>
	                        <li data-target="#carousel-main_news" data-slide-to="4" class="last">
	                            5
	                        </li>
	                    </ol>
	                    </div>
	                </div>
               </div>

            </div>
        </div>
    </div>
</div>
@endif
