<div class="header red mrg-btm-20">
    <div class="container">
  <div class="row">
      <div class="col-xs-12">

          <div class="col-md-2 col-xs-12 mrg-top-10">
              <center>
                  <a href="{{ route('home') }}"><img src="{{ asset('theme/images/logo2.png') }}" class="logo"></a>
              </center>
          </div>
          <div class="col-md-10 col-xs-12 mrg-top-10">
                {{ menu('site-nav','theme.partials.nav') }}
          </div>
      </div>

  </div><!-- row -->
</div>
</div>
