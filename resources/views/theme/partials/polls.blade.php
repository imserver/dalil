@if($poll)
<div class="row">
    <div class="col-xs-12 no-padding mrg-top-15">
        <div class="poll">
            <div class="title">
            استطلاع بلدنا
            </div>
            <div class="row mrg-btm-15">
                <div class="col-xs-12">{{ $poll->question }}</div>
            </div>
            @if ($poll->hasVoted())

            {{-- مجموع الاصوات {{ $poll->totalVotes() }} --}}

                @foreach($PollAnswers as $option)
                {{ $option->answer }} {{ $poll->votesPercent($option->votes) }}%
                <div class="progress">
                    <div class="progress-bar progress-bar-info six-sec-ease-in-out" aria-valuetransitiongoal="{{ $poll->votesPercent($option->votes) }}">{{ $poll->votesPercent($option->votes) }}%</div>
                </div>
                @endforeach

            @else
            <span id="form_output"></span>
            <form method="post" id="poll">
                {{csrf_field()}}

                <input type="hidden" name="poll_id" id="poll_id" class="form-control" value="{{ $poll->id }}">

                @foreach($PollAnswers as $option)
                <div class="col-xs-12 no-padding">
                    <label>
                        <input type="radio" name="option" id="optionsRadios{{ $option->id }}" value="{{ $option->id }}"> <span class="label-text">{{ $option->answer }}</span>
                    </label>
                </div>
                @endforeach
                <div class="col-xs-12 no-padding">
                    <button type="submit" class="btn btn-sm btn-default sawet-btn  mrg-btm-10">صوت</button>
                </div>

            </form>

            @endif
        </div>

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
    $(window).ready(function(e){
            $.each($("div.progress-bar"),function(){
            $(this).css("width", $(this).attr("aria-valuetransitiongoal")+"%");
            });
    });
    $(document).ready(function() {

        $('#poll').on('submit', function(event){
        event.preventDefault();
        var form_data = $(this).serialize();

        $.ajax({
            url:"{{ route('ajaxVotePoll') }}",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data)
            {
                if(data.error.length > 0)
                {
                    var error_html = '';
                    for(var count = 0; count < data.error.length; count++)
                    {
                        error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                    }
                    $('#form_output').html(error_html);
                }
                else
                {
                    $('#poll')[0].reset();
                    $('#poll').slideUp('slow');
                    $('#form_output').hide().html(data.success).slideDown('slow');;
                }
            }
        })

    });

});

</script>

@endif
