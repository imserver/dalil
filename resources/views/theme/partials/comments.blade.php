<section class="add-comment">
    <h2 class="mrg-btm-20">أضف تعقيب</h2>

    <form method="post"  id="addcom">
        <div class="row">

            {{csrf_field()}}
            <span id="form_output"></span>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <input type="text"  name="name" class="form-control input-lg" id="name" placeholder="الاسم" required>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <textarea class="form-control input-lg req" name="body" id="body" rows="4" placeholder=" التعقيب"></textarea>
                </div>
            </div>
            <input type="hidden" id="post_id" name="post_id" value="{{ $post->id }}">
            <div class="col-md-2 col-xs-12 pull-left">
                <button type="submit" class="btn btn-lg btn-cmnt pull-left " >انشر <i class="fa fa-angle-left"></i></button>
            </div>
        </div>
    </form>
</section>

@if(count($comments)>0)
<section class="comments">
    <h3 class="mb-20 bleu">ألتعقيبات</h3>
    <ol>
        @foreach ($comments as $comment)
        <li class="comment">
            <div class="content">
                <div class="txt">{{ $comment->body }}</div>
                <div class="name">{{ $comment->name }}</div>
                <div class="space"></div>
                <div class="date">{{ $comment->created_at->format('d.m.Y H:i') }}</div>
            </div>
        </li>
        @endforeach
    </ol>

</section>
@endif

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function() {

        $('#addcom').on('submit', function(event){
        event.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url:"{{ route('ajaxComment') }}",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data)
            {
                if(data.error.length > 0)
                {
                    var error_html = '';
                    for(var count = 0; count < data.error.length; count++)
                    {
                        error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                    }
                    $('#form_output').html(error_html);
                }
                else
                {
                    $('#form_output').html(data.success);
                    $('#addcom')[0].reset();
                }
            }
        })
    });

});

</script>
@endsection
