<footer>
    <div class="container">
        <div class="row">
          <div class="col-xs-1">
            <img src="{{ asset('theme/images/footer-logo.png') }}" class="img-responsive">
          </div>
            <div class="col-xs-10">
              <div class="row">
                <div class="col-xs-12 mrg-btm-5">

                  {{ menu('site-footer','theme.partials.nav-footer') }}

                  <div class="social pull-left mrg-top-5 mrg-btm-5">
                            <a href="{{ setting('social.facebook') }}" target="_blank">
                              <i class="fa fa-facebook"></i>
                            </a>
                            <a href="{{ setting('social.youtube') }}" target="_blank">
                              <i class="fa fa-youtube"></i>
                            </a>
                            <a href="{{ setting('social.twitter') }}" target="_blank">
                              <i class="fa fa-twitter"></i>
                            </a>


                          </div>
                </div>
                <div class="col-xs-12">
                  <div class="copy">
                جميع الحقوق محفوظة لموقع بلدنا &copy; 2019
                  </div>
                </div>
              </div>

          </div>
      </div>
  </div>
  </footer>
