<div class="topbar">
    <div class="container">
      <div class="row mrg-btm-10">
          <div class="col-md-12 col-xs-12">

                <div class="social pull-left mrg-top-10">
                  <span class="pull-right">
                      <form action="{{ route('search') }}" method="GET" class="search" name="Searchfrm" id="Searchfrm">
                        <a href="javascript:void(0);" class=" search-btn col-xs-2 fa fa-search" onclick="javascript:$('#Searchfrm').submit()" style="margin-right:0px;"></a>
                        <input type="text" name="query" id="query" value="{{ request()->input('query') }}" class="search-input pull-left col-xs-10" placeholder="ابحث في بلدنا" required>
                    </form>
                  </span>
                    <a href="{{ route('contact') }}" class="contact">
                      <i class="fa fa-phone"></i>
                      اتصل بنا
                    </a>
                    <a href="" target="_blank">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a href="" target="_blank">
                      <i class="fa fa-youtube"></i>
                    </a>
                    <a href="" target="_blank">
                      <i class="fa fa-twitter"></i>
                    </a>


                  </div>
            </div>

        </div><!-- row -->


    </div>
</div>
