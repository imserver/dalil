<div class="horos mrg-btm-10">
    <div class="row">
        <div class="col-xs-12">
            <div class="title mrg-btm-15">توقعات الأبراج</div>
            @for ($i = 0; $i < 12; $i++)
            <a href="#" data-toggle="modal" data-target="#horoModal_{{ $i }}">
                <div class="item col-md-4 col-xs-6 mrg-btm-15">
                    <img src="{{ asset('theme/images/horo_') }}{{ $i }}.png" class="img-responsive mrg-btm-10">
                    {{ $horos[$i]['title'] }}
                </div>
            </a>
            <div class="modal fade" id="horoModal_{{ $i }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2>برج {{ $horos[$i]['title'] }}</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-xs-12">
                                        <img src="{{ asset('theme/images/horo_') }}{{ $i }}.png" class="img-responsive mrg-btm-10 center-block">
                                    {!! $horos[$i]['data'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endfor
        </div>
    </div>
</div>
