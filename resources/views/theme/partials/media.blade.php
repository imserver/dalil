

<div class=" block shadow mrg-btm-20">

        <div class="media">
            @foreach ($media as $video)
                @if($loop->first)
                <div class=" item">
                    <div class="row">
                        <div class="col-xs-12 mrg-btm-20">
                            <div class="main-video">
                                <div class="top-title"><a href="{{ route('video.index') }}">فيديو بلدنا</a></div>
                                <img class="img-responsive" src="{{ getVidImage($video->image,$video->crop_image,1280,720) }}">
                                <div class="play"></div>
                                {!! $videoUrl !!}
                                <div class="item-title">{{ str_limit($video->title,100) }}</div>
                            </div>
                            </div>
                    </div>
                </div>
                @else
                <div class="col-md-4 col-xs-12 no-padding">
                    <div class=" item">
                        <div class="row">
                            <div class="col-xs-12 mrg-btm-20">
                                <a href="{{ route('video.showVideo',$video->slug) }}"><img class="img-responsive" src="{{ getVidImage($video->image,$video->crop_image,320,180) }}"></a>
                                <div class="col-xs-12">
                                        <div class="item-title small">{{ str_limit($video->title,30) }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

            @endforeach

        </div>

        <div class="col-xs-12 no-padding">
            <div class="bnr_border">
                    @include('theme.banners.small-banner')
                    @php
                    if (sizeof($midBanner)>$midBnrCount+1) $midBnrCount++;
                    else $midBnrCount=0;
                @endphp
            </div>
        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>

    $(".main-video").click(function () {
        $('.main-video .item-title').hide();
                $(".video-content").show();
                $(".video-content").attr("src", $(".video-content").attr("src") + "&autoplay=1");

                $(".video-content-file").show();
                $(".video-content-file").attr("src", $(".video-content-file").attr("src"));
            });
    </script>


