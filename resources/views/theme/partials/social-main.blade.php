<div class="row social-main">

        <div class=" col-md-3 col-xs-12 col-md-offset-1 title">
            تابعونا عبر مواقع التواصل الاجتماعي
        </div>
        <div class="col-md-6 col-xs-12 pull-left">
            <a href="{{ setting('social.instagram') }}" target="_blank">
                <img src="{{ asset('theme/images/social1.png') }}">
            </a>
            <a href="{{ setting('social.twitter') }}" target="_blank">
                <img src="{{ asset('theme/images/social2.png') }}">
            </a>
            <a href="{{ setting('social.facebook') }}" target="_blank">
                <img src="{{ asset('theme/images/social3.png') }}">
            </a>
            <a href="{{ setting('social.youtube') }}" target="_blank">
                <img src="{{ asset('theme/images/social4.png') }}">
            </a>
            <a href="{{ setting('social.whatsapp') }}" target="_blank">
                <img src="{{ asset('theme/images/social5.png') }}">
            </a>
        </div>
      </div>
