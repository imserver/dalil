@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

    <div class="container">
        @include('theme.banners.big')
    </div>

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-xs-12 mrg-btm-20  mrg-top-10">
            <div class="row">
                <div class="col-xs-12 block shadow mrg-btm-200">
                	<div class="col-xs-12 ">
                            <div class="breadcrumb pull-right col-xs-12">
                                <span class="cat-subtitle pull-right">أرسل خبر</span>
                            </div>
                        </div>



					<div class="add-comment pull-right mrg-top-10">
						<div class="row">

                            <form action="{{ route('post.saveArt') }}" id="addart" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
								<div id="addart">

                                    @if ($errors->any())
                                    <div class="col-md-12 col-xs-12">
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    @endif

                                    @if (session('status'))
                                    <div class="col-md-12 col-xs-12">
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    </div>
                                    @endif

								<div class="col-md-12 col-xs-12 no-padding">

						        	<div class="form-group">
						        		<div class="col-xs-12">
							        		<div class="col-md-6 col-xs-12  ">
							        			<input type="text" name="title" class="form-control input-xs req mrg-top-15" id="" placeholder="عنوان الخبر" required>
							        		</div>
							        		<div class="col-md-6 col-xs-12 ">

										        <input type="file" name="image" id="files" class="inputfile"  />
												<label for="files" id="file_btn"><span>إختيار صورة</span></label>
												<div id="image-holder"></div>
										    </div>

							        		<div class="col-md-6 col-xs-12 ">
							        			<input type="text" name="name" class="form-control input-xs req mrg-top-15" id="" placeholder="الاسم" required>
							        		</div>
							        		<div class="col-md-6 col-xs-12  ">
							        			<input type="text" name="phone" class="form-control input-xs req mrg-top-15" id="" placeholder="الهاتف" required>
							        		</div>
							        		<div class="col-md-12 col-xs-12 ">
							        			<textarea name="details" class="col-xs-12 mrg-top-15" placeholder="تفاصيل الخبر"></textarea>
							        		</div>
										</div>
										<div class="col-xs-12">
											<div class="col-md-12 col-xs-12 center-block mrg-top-15 pull-left">
							        			<button class="btn add center-block" type="submit">
									        	اضف </button>
										        </div>
								    		</div>
										</div>
						            </div>
								</div>
							</div>

							</form>
						</div>
					</div>

        </div>
            </div>

        <div class="col-md-4 col-xs-12 mrg-btm-10 mrg-top-10">
                <div class="row">
                      @include('theme.partials.aside')
                </div>
            </div>

        </div><!-- /row -->
    </div>
    @endsection

    @section('javascripts')
<script type="text/javascript">
	$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length,
        count_thumbs = 0,
        limit = 3;
        $(".thumb-image").each(function(){
	        count_thumbs++;
	    });
	    if(count_thumbs >0){
	    	limit = 3 - count_thumbs;
	    }

      for (var i = 0; i < limit; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<div class=\"col-xs-4 thumb-image\">" +
            "<img class=\"img-responsive\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">X</span>" +
            "</div>").insertAfter("#file_btn");
          $(".remove").click(function(){
            $(this).parent(".thumb-image").remove();
          });

        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});

</script>
@endsection
