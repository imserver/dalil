@extends('index')

@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

<div class="container">
    @include('theme.banners.big')
</div>

@endsection

@section('content')

  <div class="container">
      <div class="row">
          <div class="col-md-7 col-xs-12 mrg-top-10 no-pd-left">

              <div class="row">
                  <div class="col-xs-12 ">
                    <div class="block shadow">
                      @component('components.breadcrumbs')
                            <a href="{{ route('home') }}" title="الرئيسية" class="home">الرئيسية</a>
                            @if(!empty($category))
                            <i class="fa fa-angle-double-left"></i>
                            <a href="{{ route('post.showCategory',$category->slug) }}" title="{{ $category->name }}">{{ $category->name }}</a>
                            @endif
                            <i class="fa fa-angle-double-left"></i>
                            <span class="current">{{ $post->title }}</span>
                        @endcomponent

                      <div class="row">
                          <div class="col-xs-12">
                              <h1 class="article-h1">{{ $post->title }}</h1>
                          </div>
                          <div class="col-xs-12 mrg-btm-10 art-date">
                              {{ $post->created_at->format('d.m.Y') }} /
                               الساعة {{ $post->created_at->format('H:i') }} بتوقيت القدس
                          </div>
                      </div>
                      <!-- Go to www.addthis.com/dashboard to customize your tools -->
                          <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f8844ec57f383b2"></script>

                          <!-- Go to www.addthis.com/dashboard to customize your tools -->
                          <div class="addthis_inline_share_toolbox mrg-btm-15 no-print"></div>
                          <script type="text/javascript">
                          var addthis_config = addthis_config||{};
                          addthis_config.lang = 'ar' //show in Spanish regardless of browser settings;
                          </script>
                        @if($post->show_pic)
                        <img src="{{ getImage($post->image,$post->crop_image,780,520) }}" class="img-responsive art-main" >
                        @endif

                      <article>
                          <p>{!! $post->body !!}</p>

                          @if ($post->images)
                            @foreach (json_decode($post->images, true) as $image)
                            <img src="{{ getImage($image) }}" class="img-responsive center-block">
                            @endforeach
                        @endif


                      </article>
                  </div>
                  </div>
                  <div class="col-xs-12 mrg-top-20 mrg-btm-10">
                      <div class="row">

                          <div class="col-xs-12">
                                <div class="block shadow">
                                @include('theme.partials.comments')
                                </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-md-5 col-xs-12 mrg-btm-10 mrg-top-10">
              <div class="row">
                    @include('theme.partials.aside_post')
              </div>
          </div>

      </div><!-- /row -->
  </div>
  @endsection
