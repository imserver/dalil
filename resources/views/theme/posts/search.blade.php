@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

    <div class="container">
        @include('theme.banners.big')
    </div>

@endsection

@section('content')
  <div class="container">
      <div class="row mrg-top-20">
          <div class="col-md-8 col-xs-12 no-pd-left">
              <div class="row">
                  <div class="col-xs-12">
                    @if (session()->has('success_message'))
                        <div class="alert alert-success">
                            {{ session()->get('success_message') }}
                        </div>
                    @endif

                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                  </div>
              </div>
              <div class="row">
                  <div class="col-xs-12 mrg-btm-200">
                    <div class="block shadow">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb pull-right col-xs-12">
                                <span class="cat-subtitle pull-right">
                                        نتائج البحث عن '{{ request()->input('query') }}' هي ( {{ $posts->total() }} )
                                </span>
                            </div>
                        </div>

                        @if ($posts->total() > 0)
                                @foreach ($posts as $post)
                                    <div class="col-md-3 col-xs-12 mrg-top-10 cat-item">
                                        <a href="{{ route('post.showPost',$post->slug) }}">
                                            <img src="{{ getImage($post->image,$post->crop_image,780,520) }}" class="img-responsive">
                                            <div class="txt mrg-top-10">{{ $post->title }}</div>
                                        </a>
                                    </div>
                                @endforeach
                        {{ $posts->appends(request()->input())->links() }}
                        @endif

                    </div>

                  </div>
              </div>

              <div class="row">
                <div class="col-lg-12 text-center pull-right">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                                {{ $posts->render() }}
                        </ul>
                    </nav>
                </div>
            </div>

          </div>
          <div class="col-md-4 col-xs-12 mrg-btm-10">
              <div class="row">
                  @include('theme.partials.aside')
              </div>
          </div>
      </div>
  </div>
  @endsection
