@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

    <div class="container">
        @include('theme.banners.big')
    </div>

@endsection

@section('content')
  <div class="container">
      <div class="row mrg-top-20">
          <div class="col-md-8 col-xs-12 no-pd-left">
              <div class="row">
                  <div class="col-xs-12 mrg-btm-200">
                    <div class="block shadow">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb pull-right col-xs-12">
                                <span class="cat-subtitle pull-right">{{ $categoryName }}</span>
                            </div>
                        </div>


                      @foreach ($posts as $post)
                        @if(!isset($_GET['page']) && $loop->first)
                            <div class="col-md-12 col-xs-12 cat-item mrg-btm-10">
                                <div class="row">
                                    <a href="{{ route('post.showPost',$post->slug) }}">
                                        <div class="col-xs-5">
                                            <img src="{{ getImage($post->image,$post->crop_image,780,520) }}" class="img-responsive">
                                        </div>
                                        <div class="col-xs-7">
                                            <div class="title">{{ $post->title }}</div>
                                            <p>{{ $post->excerpt }}</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @else
                            <div class="col-md-3 col-xs-12 mrg-top-10 cat-item">
                                <a href="{{ route('post.showPost',$post->slug) }}">
                                    <img src="{{ getImage($post->image,$post->crop_image,780,520) }}" class="img-responsive">
                                    <div class="txt mrg-top-10">{{ $post->title }}</div>
                                </a>
                            </div>
                            @endif
                      @endforeach
                  </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-12 text-center pull-right">
                      <nav aria-label="Page navigation">
                        <ul class="pagination">
                                {{ $posts->render() }}
                        </ul>
                      </nav>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-xs-12 mrg-btm-10">
              <div class="row">
                  @include('theme.partials.aside')
              </div>
          </div>
      </div>
  </div>
  @endsection
