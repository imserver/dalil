@extends('index')


@section('header')

    @include('theme.partials.header-inner')

@endsection

@section('big-banner-inner')

    <div class="container">
        @include('theme.banners.big')
    </div>

@endsection

@section('content')
  <div class="container">
      <div class="row mrg-top-20">
          <div class="col-md-8 col-xs-12 no-pd-left">
              <div class="row">
                  <div class="col-xs-12 mrg-btm-200">
                    <div class="block shadow">

                    @foreach ($subCats as $subcat)
                        <div class="col-xs-12">
                            <div class="breadcrumb pull-right col-xs-12">
                                <a href="{{ route('post.showCategory',$subcat->slug) }}" class="cat-subtitle pull-right">{{ $subcat->name }}</a>
                                <a href="{{ route('post.showCategory',$subcat->slug) }}" class="pull-left mrg-top-5">
                                        المزيد <i class="fa fa-angle-double-left"></i></a>
                            </div>
                        </div>
                            @foreach ($subPosts[$subcat->id] as $subpost)
                                @if($loop->first)
                                <div class="col-md-12 col-xs-12 cat-item mrg-btm-10">
                                    <div class="row">
                                        <a href="{{ route('post.showPost',$subpost->slug) }}">
                                            <div class="col-xs-5">
                                                <img src="{{ getImage($subpost->image,$subpost->crop_image,780,520) }}" class="img-responsive">
                                            </div>
                                            <div class="col-xs-7">
                                                <div class="title">{{ $subpost->title }}</div>
                                                <p>{{ $subpost->excerpt }}</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                @else
                                <div class="col-md-3 col-xs-12 mrg-top-10 cat-item mrg-btm-10">
                                    <a href="{{ route('post.showPost',$subpost->slug) }}">
                                        <img src="{{ getImage($subpost->image,$subpost->crop_image,780,520) }}" class="img-responsive">
                                        <div class="txt mrg-top-10">{{ $subpost->title }}</div>
                                    </a>
                                </div>
                                @endif
                            @endforeach
                        @endforeach



                  </div>
                  </div>
              </div>

          </div>
          <div class="col-md-4 col-xs-12 mrg-btm-10">
              <div class="row">
                  @include('theme.partials.aside')
              </div>
          </div>
      </div>
  </div>
  @endsection
