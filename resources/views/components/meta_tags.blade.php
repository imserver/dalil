<title>{{ $meta['title'] }}</title>
<link rel="icon" type="image/x-icon" href="{{ asset('/storage/'.setting('site.favicon')) }}"/>
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('/storage/'.setting('site.favicon')) }}"/>
<meta name="title" content="{{ $meta['title'] }}" />
<meta name="description" content="{{ $meta['description'] }}" />
<meta name="keywords" content="{{ $meta['keywords'] }}">

<meta property="og:title" content="{{ $meta['title'] }}"/>
<meta property="og:site_name" content="{{ $meta['site_name'] }}"/>
<meta property="og:url" content="{{ $meta['url'] }}">
<meta property="og:description" content="{{ $meta['description'] }}">
<meta property="og:image" content="{{ $meta['image'] }}"/>
<link rel="image_src" href="{{ $meta['image'] }}" />


<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
