
<option value="{{ $category->id }}" {{ $categoriesForPost->contains($category) ? 'selected="selected"' : '' }} >{{ $dash ." ". $category->name }}</option>

@if ($category->subcategory()->count() > 0 )
    @foreach($category->subcategory as $category)
        @php($dash = $dash . "-")
        @include('cms.category-list', $category) 
    @endforeach

@endif
