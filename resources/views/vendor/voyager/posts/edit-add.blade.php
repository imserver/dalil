@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
    <link href="{{ asset('css/post.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(isset($dataTypeContent->id)){{ route('voyager.posts.update', $dataTypeContent->id) }}@else{{ route('voyager.posts.store') }}@endif" method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <!-- ### TITLE ### -->
                    <div class="panel">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="voyager-character"></i>  Title
                                <span class="panel-desc" style="display: none"> {{ __('voyager::post.title_sub') }}</span>
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'title',
                                '_field_trans' => get_field_translations($dataTypeContent, 'title')
                            ])
                            <input type="text" class="form-control" id="title" name="title" placeholder="{{ __('voyager::generic.title') }}" value="{{ $dataTypeContent->title ?? '' }}">
                        </div>
                    </div>

                    <!-- ### CONTENT ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Content</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>

                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'body',
                                '_field_trans' => get_field_translations($dataTypeContent, 'body')
                            ])
                            @php
                                $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                $row = $dataTypeRows->where('field', 'body')->first();
                            @endphp
                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                        </div>
                    </div><!-- .panel -->

                    <!-- ### EXCERPT ### -->

                    <div class="panel" style="display: none">
                        <div class="panel-heading">
                            <h3 class="panel-title">{!! __('voyager::post.excerpt') !!}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'excerpt',
                                '_field_trans' => get_field_translations($dataTypeContent, 'excerpt')
                            ])
                            <textarea class="form-control" name="excerpt">{{ $dataTypeContent->excerpt ?? '' }}</textarea>
                        </div>
                    </div>


                    <div class="panel" >
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ __('voyager::post.additional_fields') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @php
                                $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                $exclude = ['title', 'body', 'excerpt', 'slug', 'status', 'category_id', 'author_id', 'featured', 'image', 'meta_description', 'meta_keywords', 'seo_title','crop_image'];
                            @endphp

                            @foreach($dataTypeRows as $row)
                                @if(!in_array($row->field, $exclude))
                                    @php
                                        $display_options = $row->details->display ?? NULL;
                                    @endphp
                                    @if (isset($row->details->formfields_custom))
                                        @include('voyager::formfields.custom.' . $row->details->formfields_custom)
                                    @else
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif @if(isset($display_options->width)){{ 'col-md-' . $display_options->width }}@endif" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            {{ $row->slugify }}
                                            <label for="name">{{ $row->display_name }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if($row->type == 'relationship')
                                                @include('voyager::formfields.relationship', ['options' => $row->details])
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> {{ __('voyager::post.details') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="slug">{{ __('voyager::post.slug') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'slug',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                ])
                                <input type="text" class="form-control" id="slug" name="slug"
                                    placeholder="slug"
                                    {!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}
                                    value="{{ $dataTypeContent->slug ?? '' }}">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" name="status">
                                    <option value="PUBLISHED"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PUBLISHED') selected="selected"@endif>{{ __('voyager::post.status_published') }}</option>
                                    <option value="READY"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'READY') selected="selected"@endif>{{ __('voyager::post.status_ready') }}</option>
                                    <option value="NEED PICTURES"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'NEED PICTURES') selected="selected"@endif>{{ __('voyager::post.status_pictures') }}</option>
                                    <option value="DRAFT"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'DRAFT') selected="selected"@endif>{{ __('voyager::post.status_draft') }}</option>
                                    <option value="PENDING"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PENDING') selected="selected"@endif>{{ __('voyager::post.status_pending') }}</option>
                                </select>
                            </div>

                            {{-- <div class="form-group">
                                <label for="category_id">{{ __('voyager::post.category') }}</label>
                                <select class="form-control" name="category_id">
                                    @foreach(TCG\Voyager\Models\Category::all() as $category)
                                        <option value="{{ $category->id }}"@if(isset($dataTypeContent->category_id) && $dataTypeContent->category_id == $category->id) selected="selected"@endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div> --}}

                            <div class="form-group">
                                <label for="categories">Category</label>
                                <select class="form-control select2" name="categories[]" multiple>
                                    @foreach ($parentCategories as $category)
                                        @php($dash = "-")
                                        @if($category->parent_id == 0 )

                                            @include('cms.category-list', $category)

                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="featured">{{ __('voyager::generic.featured') }}</label>
                                <input class="toggleswitch" type="checkbox" name="featured"@if(isset($dataTypeContent->featured) && $dataTypeContent->featured) checked="checked"@endif>


                            </div>
                        </div>
                    </div>

                    <!-- ### IMAGE ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> Image</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if(!isset($dataTypeContent->crop_image) && !isset($dataTypeContent->image))
                            <div class="previewImage" id="previewImage" data-field-name="image"  style="display:none;">
                                <a href="#" class="voyager-x remove-single-image" style="position:absolute;" id="delete_previewImage"></a>
                                <img id="imgpreview" data-id="{{ $dataTypeContent->id }}" src="" data-file-name="" >
                            <button v-if="allowCrop" :disabled="selected_files.length != 1 || !fileIs(selected_file, 'image')" type="button" class="btn btn-default crop-btn" data-toggle="modal" data-target="#crop_modal_{{ $dataType->slug }}">
                                    <i class="voyager-crop"></i>
                                </button>
                            </div>
                            @elseif(isset($dataTypeContent->crop_image))
                            <div id="previewImage" class="previewImage" data-field-name="crop_image">
                                <a href="#" class="voyager-x remove-crop-image" id="delete_previewImage" style="position:absolute;"></a>
                                <img id="imgpreview" data-id="{{ $dataTypeContent->id }}" src="{{ $dataTypeContent->crop_image }}" data-file-name="{{ $dataTypeContent->crop_image }}" >
                            </div>
                            @elseif(isset($dataTypeContent->image))

                                <div id="previewImage" class="previewImage" data-field-name="image">
                                    <a href="#" class="voyager-x remove-single-image" style="position:absolute;"></a>
                                    <img id="imgpreview" data-id="{{ $dataTypeContent->id }}" src="@if( !filter_var($dataTypeContent->image, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->image ) }}@else{{ $dataTypeContent->image }}@endif"
                                        data-file-name="{{ $dataTypeContent->image }}"  >
                                </div>
                            @endif
                            <input type="file" name="image" id="upload" accept="image/*" onchange="readURL(this)" dataurl="">
                            <input type="hidden" name="crop_image" value="" id="crop_image">
                        </div>


                    </div>

                    <!-- ### SEO CONTENT ### -->

                    <div class="panel panel-bordered panel-info" style="display: none">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> {{ __('voyager::post.seo_content') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="meta_description">{{ __('voyager::post.meta_description') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_description',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_description')
                                ])
                                <textarea class="form-control" name="meta_description">{{ $dataTypeContent->meta_description ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="meta_keywords">{{ __('voyager::post.meta_keywords') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_keywords',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords')
                                ])
                                <textarea class="form-control" name="meta_keywords">{{ $dataTypeContent->meta_keywords ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="seo_title">{{ __('voyager::post.seo_title') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'seo_title',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'seo_title')
                                ])
                                <input type="text" class="form-control" name="seo_title" placeholder="SEO Title" value="{{ $dataTypeContent->seo_title ?? '' }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right">
                @if(isset($dataTypeContent->id)){{ __('voyager::post.update') }}@else <i class="icon wb-plus-circle"></i> {{ __('voyager::post.new') }} @endif
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>
                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Crop Image Modal -->
    <div class="modal fade modal-warning" id="crop_modal_{{ $dataType->slug }}">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::media.crop_image') }}</h4>
                    </div>

                    <div class="modal-body">
                        <div class="panel panel-info">
                            <div class="panel-body">

                                <div class="row">
                                <div class="col-md-12 text-center">
                                    <div id="upload-demo"></div>
                                </div>
                                <div class="col-md-8 col-md-offset-3 col-xs-12">

                                <button class="btn btn-primary upload-image" id="cropImageBtn">{{ __('voyager::generic.crop') }}</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" id="cancelCropBtn">{{ __('voyager::generic.cancel') }}</button>
                                <button type="button" class="btn btn-warning" id="confirmCropBtn" style="display:none;">Confirm</button>
                                </div>

                                <div class="col-md-12">
                                <div id="preview-crop-image">
                                    <img src="" class="gambar img-responsive img-thumbnail" id="item-img-output" />
                                </div>
                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
        <!-- End Crop Image Modal -->


@stop


@section('javascript')

<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
<script>

        var $uploadCrop,
        tempFilename,
        rawImg,
        imageId,
        imgWidth,
        imgHeight;

        function readURL(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imgpreview').attr('src', e.target.result);
                    $('#imgpreview').attr('data-file-name', e.target.result);
                   // $('#cropping-image_{{ $dataType->slug }}').attr('src', e.target.result);
                    $('#previewImage').show();
                  //  $('#currentImg').hide();
                  $('.crop-btn').show();


                    $('#crop_modal_{{ $dataType->slug }}').modal('show');

                    rawImg = e.target.result;

                    var img = new Image();
                    img.src = e.target.result;

                    img.onload = function () {
                        imgWidth = this.width;
                        imgHeight = this.height;
                        }
                    }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            // viewport: 'original',
            viewport: { width: 200, height: 200 },
            boundary: { width: 300, height: 300 },
            showZoomer: false,
            enableResize: true,
            enableExif: true,
            enforceBoundary: false,
        });
        $('#crop_modal_{{ $dataType->slug }}').on('shown.bs.modal', function(){
            // alert(imgWidth);
            // $("#preview-crop-image").html(rawImg);
            $('#new-image-width_{{ $dataType->slug }}').html(imgWidth);
            $('#new-image-height_{{ $dataType->slug }}').html(imgHeight);
            $uploadCrop.croppie('bind', {
                url: rawImg,
            }).then(function(){
                console.log('jQuery bind complete');
            });
        });



        $('#upload').on('change', function () {
            imageId = $(this).data('id');
            tempFilename = $(this).val();
            $('#cancelCropBtn').data('id', imageId);
            $('#item-img-output').attr('src', '');
            $('#delete_previewImage').removeClass( "remove-single-image" ).addClass( "remove-crop-image" );
            $('.crop-btn').show();
            readURL(this);
        });

        $('#cropImageBtn').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'original'
            }).then(function (resp) {
                //var objectURL = URL.createObjectURL(resp);
                var objectURL = resp;
                $('#item-img-output').attr('src', objectURL);
                $('#confirmCropBtn').data('url', resp);
                $('#new-image-width_{{ $dataType->slug }}').html(imgWidth);
                $('#confirmCropBtn').show();
                //$('#cropping-image_{{ $dataType->slug }}').modal('hide');
            });
        });

        $('#confirmCropBtn').on('click', function () {
            //alert($('#confirmCropBtn').data('url'));
            var cropedurl = $('#confirmCropBtn').data('url');
            $('#imgpreview').attr('src', cropedurl);
            $('#imgpreview').attr('data-file-name', cropedurl);
            $('#previewImage').attr('data-field-name', 'crop_image');
            $('#crop_image').val(cropedurl);
            $('#upload').val('');
            $( "#delete_previewImage" ).removeClass( "remove-single-image" ).addClass( "remove-crop-image" );

            $('#crop_modal_{{ $dataType->slug }}').modal('hide');
        });


    </script>

<script>

        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            //$('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }


        $('#delete_previewImage').on('click', function (e) {
            $('#confirm_delete_modal')
                .modal({ backdrop: 'static', keyboard: false })
                .one('click', '#confirm_delete', function (e) {
                    $('#crop_image').val('');
                    $('#currentImg').fadeOut();
                    $('#crop-preview').fadeOut();
                    $('#previewImage').fadeOut();
                    $('#imgpreview').attr('src', '');
                    $('#imgpreview').attr('data-file-name', '');
                    $('#upload').val('');
                });
        });


        $('document').ready(function () {

            // $('.toggleswitch').bootstrapToggle();
            $('.toggleswitch').bootstrapToggle({
                on: '{{ __('voyager::generic.on') }}',
                off: '{{ __('voyager::generic.off') }}'
            });

            $('#slug').slugify();

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

           $('.panel-body').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.panel-body').on('click', '.remove-single-file', deleteHandler('a', false));

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));$('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));


           $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(500, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });

           $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

@stop
