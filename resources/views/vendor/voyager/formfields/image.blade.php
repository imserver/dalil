@php
$collect =  collect($dataType->addRows);
$search = $collect->where('type','crop');
//echo sizeof($search);
//print_r($search);
$crop_field = 'crop_'.$row->field;
$with_crop = $search->contains('field',$crop_field);
@endphp
    @if(!isset($dataTypeContent->{$crop_field}) && !isset($dataTypeContent->{$row->field}))
    <div class="previewImage" id="previewImage_{{ $row->field }}" data-field-name="{{ $row->field }}"  style="display:none;">
        <a href="#" class="voyager-x remove-single-image" style="position:absolute;" id="delete_previewImage_{{ $row->field }}"></a>
        <img class="imgpreview" id="imgpreview_{{ $row->field }}" data-id="{{ $dataTypeContent->id }}" src="" data-file-name="" >
        @if($with_crop)
    <button v-if="allowCrop" :disabled="selected_files.length != 1 || !fileIs(selected_file, 'image')" type="button" class="btn btn-default crop-btn " data-toggle="modal" data-target="#crop_modal_{{ $row->field }}">
            <i class="voyager-crop"></i>
        </button>
        @endif
    </div>
    @elseif(isset($dataTypeContent->{$crop_field}))
    <div id="previewImage" class="previewImage" data-field-name="{{ $crop_field }}">
        <a href="#" class="voyager-x remove-crop-image" id="delete_previewImage_{{ $row->field }}" style="position:absolute;"></a>
        <img class="imgpreview" id="imgpreview_{{ $row->field }}" data-id="{{ $dataTypeContent->id }}" src="{{ $dataTypeContent->{$crop_field} }}" data-file-name="{{ $dataTypeContent->{$crop_field} }}" >
        @if($with_crop)
        <button v-if="allowCrop" :disabled="selected_files.length != 1 || !fileIs(selected_file, 'image')" type="button" class="btn btn-default crop-btn " data-toggle="modal" data-target="#crop_modal_{{ $crop_field }}">
            <i class="voyager-crop"></i>
        </button>
        @endif
    </div>
    @elseif(isset($dataTypeContent->{$row->field}))

<div id="previewImage_{{ $row->field }}" class="previewImage" data-field-name="{{ $dataTypeContent->{$row->field} }}">
            <a href="#" class="voyager-x remove-single-image" style="position:absolute;"></a>
            <img class="imgpreview" id="imgpreview_{{ $row->field }}" data-id="{{ $dataTypeContent->id }}" src="@if( !filter_var($dataTypeContent->image, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}@else{{ $dataTypeContent->{$row->field} }}@endif"
                data-file-name="{{ $dataTypeContent->{$row->field} }}"  >
                @if($with_crop)
            <button v-if="allowCrop" :disabled="selected_files.length != 1 || !fileIs(selected_file, 'image')" type="button" class="btn btn-default crop-btn " data-toggle="modal" data-target="#crop_modal_{{ $row->field }}">
                    <i class="voyager-crop"></i>
                </button>
                @endif
        </div>
    @endif

<input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file" id="upload_{{ $row->field }}" name="{{ $row->field }}" accept="image/*"  dataurl="" onchange="readURL(this)">

<!-- Crop Image Modal -->
<div class="modal fade modal-warning" id="crop_modal_{{ $row->field }}">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::media.crop_image') }}</h4>
            </div>

            <div class="modal-body">
                <div class="panel panel-info">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div id="upload-demo_{{ $row->field }}"></div>
                            </div>
                            <div class="col-md-8 col-md-offset-3 col-xs-12">

                                <div class="btn btn-primary upload-image" id="cropImageBtn_{{ $row->field }}">{{ __('voyager::generic.crop') }}</div>
                                <div class="btn btn-default" data-dismiss="modal" id="cancelCropBtn_{{ $row->field }}">{{ __('voyager::generic.cancel') }}</div>
                                <div class="btn btn-warning" id="confirmCropBtn_{{ $row->field }}" style="display:none;">Confirm</div>
                            </div>

                            <div class="col-md-12">
                                <div id="preview-crop-image_{{ $row->field }}">
                                    <img src="" class="gambar img-responsive img-thumbnail" id="item-img-output_{{ $row->field }}" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!-- End Crop Image Modal -->

@if(sizeof($search)>0)
<script type="text/javascript">

    var $uploadCrop_{{ $row->field }},
    tempFilename,
    rawImg_{{ $row->field }},
    imageId_{{ $row->field }},
    imgWidth_{{ $row->field }},
    imgHeight_{{ $row->field }};

    function readURL(input) {
        var url = input.value;
        var input_name = input.name;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgpreview_'+input_name).attr('src', e.target.result);
                $('#imgpreview_'+input_name).attr('data-file-name', e.target.result);
                // $('#cropping-image_{{ $dataType->slug }}').attr('src', e.target.result);
                $('#previewImage_'+input_name).show();
                $('.crop-btn').show();
                //  $('#currentImg').hide();


             $('#crop_modal_'+input_name).modal('show');

                rawImg_{{ $row->field }} = e.target.result;

                var img = new Image();
                img.src = e.target.result;

                img.onload = function () {

                    imgWidth_{{ $row->field }} = img.width;
                    imgHeight_{{ $row->field }} = img.height;

                    $('#upload-demo_'+input_name).attr('data-width', img.width);
                    $('#upload-demo_'+input_name).attr('data-height', img.height);
                    $('#upload-demo_'+input_name).attr('data-url', e.target.result);
                    }

                }

            reader.readAsDataURL(input.files[0]);
        }
        $('#crop_modal_'+input_name).modal('show');
    }

    $uploadCrop_{{ $row->field }} = $('#upload-demo_{{ $row->field }}').croppie({
        // viewport: 'original',
        viewport: { width: 200, height: 200 },
        boundary: { width: 300, height: 300 },
        showZoomer: false,
        enableResize: true,
        enableExif: true,
        enforceBoundary: false,
    });
    $(document).ready(function(){

        $('#crop_modal_{{ $row->field }}').on('shown.bs.modal', function(){

           //alert(imgWidth_{{ $row->field }});
            // $("#preview-crop-image").html(rawImg);
            $('#new-image-width_{{ $row->field }}').html(imgWidth_{{ $row->field }});
            $('#new-image-height_{{ $row->field }}').html(imgHeight_{{ $row->field }});

            imgWidth_{{ $row->field }} = $('#upload-demo_{{ $row->field }}').attr('data-width');
                    imgHeight_{{ $row->field }} = $('#upload-demo_{{ $row->field }}').attr('data-height');
                    rawImg_{{ $row->field }} = $('#upload-demo_{{ $row->field }}').attr('data-url');
                  //  alert(imgWidth_{{ $row->field }});

            $uploadCrop_{{ $row->field }}.croppie('bind', {
                url: rawImg_{{ $row->field }},
            }).then(function(){
                console.log('jQuery bind complete.');
            });
        });
    });



    $('#upload_{{ $row->field }}').on('change', function () {
        imageId_{{ $row->field }} = $(this).data('id');

        tempFilename = $(this).val();
        $('#cancelCropBtn_{{ $row->field }}').data('id', imageId_{{ $row->field }});
        $('#item-img-output_{{ $row->field }}').attr('src', '');
        $('#delete_previewImage_{{ $row->field }}').removeClass( "remove-single-image" ).addClass( "remove-crop-image" );
        $('.crop-btn').show();
        readURL(this);
    });

    $('#cropImageBtn_{{ $row->field }}').on('click', function (ev) {
        $uploadCrop_{{ $row->field }}.croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (resp) {
            //var objectURL = URL.createObjectURL(resp);
            var objectURL = resp;
            $('#item-img-output_{{ $row->field }}').attr('src', objectURL);
            $('#confirmCropBtn_{{ $row->field }}').data('url', resp);
            $('#new-image-width_{{ $row->field }}').html(imgWidth_{{ $row->field }});
            $('#confirmCropBtn_{{ $row->field }}').show();
            //$('#cropping-image_{{ $dataType->slug }}').modal('hide');
        });
    });

    $('#confirmCropBtn_{{ $row->field }}').on('click', function () {
        //alert($('#confirmCropBtn').data('url'));
        var cropedurl = $('#confirmCropBtn_{{ $row->field }}').data('url');
        $('#imgpreview_{{ $row->field }}').attr('src', cropedurl);
        $('#imgpreview_{{ $row->field }}').attr('data-file-name', cropedurl);
        $('#previewImage_{{ $row->field }}').attr('data-field-name', 'crop_{{ $row->field }}');
        $('#crop_{{ $row->field }}').val(cropedurl);
        $('#upload_{{ $row->field }}').val('');
        $( "#delete_previewImage_{{ $row->field }}" ).removeClass( "remove-single-image" ).addClass( "remove-crop-image" );

        $('#crop_modal_{{ $row->field }}').modal('hide');
    });


    </script>

@else

<script type="text/javascript">

    var $uploadCrop_{{ $row->field }},
    tempFilename,
    rawImg_{{ $row->field }},
    imageId_{{ $row->field }},
    imgWidth_{{ $row->field }},
    imgHeight_{{ $row->field }};

    function readURL(input) {
        var url = input.value;
        var input_name = input.name;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgpreview_'+input_name).attr('src', e.target.result);
                $('#imgpreview_'+input_name).attr('data-file-name', e.target.result);
                // $('#cropping-image_{{ $dataType->slug }}').attr('src', e.target.result);
                $('#previewImage_'+input_name).show();
               // $('.crop-btn').show();
                //  $('#currentImg').hide();


            // $('#crop_modal_'+input_name).modal('show');

                rawImg_{{ $row->field }} = e.target.result;

                var img = new Image();
                img.src = e.target.result;

                img.onload = function () {

                    imgWidth_{{ $row->field }} = img.width;
                    imgHeight_{{ $row->field }} = img.height;

                    $('#upload-demo_'+input_name).attr('data-width', img.width);
                    $('#upload-demo_'+input_name).attr('data-height', img.height);
                    $('#upload-demo_'+input_name).attr('data-url', e.target.result);
                    }

                }

            reader.readAsDataURL(input.files[0]);
        }
        //$('#crop_modal_'+input_name).modal('show');
    }

    $('#upload_{{ $row->field }}').on('change', function () {
        imageId_{{ $row->field }} = $(this).data('id');

        tempFilename = $(this).val();
        $('#cancelCropBtn_{{ $row->field }}').data('id', imageId_{{ $row->field }});
        $('#item-img-output_{{ $row->field }}').attr('src', '');
        $('#delete_previewImage_{{ $row->field }}').removeClass( "remove-single-image" ).addClass( "remove-crop-image" );
        readURL(this);
    });

</script>
@endif
