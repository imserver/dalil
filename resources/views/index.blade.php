<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1170px">

    <!-- Bootstrap -->
    <link href="{{ asset('theme/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/bootstrap.rtl.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/fontface.css') }}" rel="stylesheet">

    <link href="{{ asset('theme/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('theme/css/feature-carousel.css') }}" charset="utf-8" />
    <link rel="Stylesheet" type="text/css" href="{{ asset('theme/css/carousel.css') }}">

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ asset('theme/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/owl.theme.default.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('stylesheets')

    @yield('head-javascripts')

    @include('components.meta_tags')

    </head>
  <body>

    @include('theme.partials.topbar')

    @yield('big-banner')

    @yield('header')

    @yield('big-banner-inner')

    @yield('content')

    @include('theme.partials.footer')


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('theme/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('theme/js/main.js') }}"></script>

    @yield('javascripts')

  </body>
</html>
