@extends('nadsoftweb.layouts.Web_app')



@section('content')
    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#portfolio').addClass('active');
        });
    </script>
    <!-- about style -->
    <link rel="stylesheet" href="{{ url('/web/assets/portfolio.css') }}" />

    <!-- section 1 -->
    <div class="container-fluid" id="section1">
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-4" id="pagetitle">
                    <h2>PORTFOLIO</h2>
                </div>
                <div class="col-md-1"></div>

                <div class="col-md-7" id="intro">
                    <h2 class="maintitle aos-item" data-aos="fade-left" data-aos-duration="1000">OUR <span> Projects</span>  </h2>
                    <div class="aos-item" data-aos="fade-right" data-aos-duration="1000" data-aos-offset="250" data-aos-easing="linear">
                   {!! $intro->desc !!}

                    </div>
              <!--     <p class="aos-item" data-aos="fade-up" data-aos-duration="3000" data-aos-offset="250" data-aos-easing="linear">
                        Planning a project is the most important so we believe that a perfect planning leading us to a perfect project. Planning a project is the most important
                    </p>
-->
                </div>



            </div>
        </div>
    </div>
    <!-- ../section 1 -->


    <!-- section 2 -->
    <div class="container-fluid" id="section3">
        <div class="row" id='filiter'>
            <div class="col-md-l2" style="text-align:center">
                <ul class="filiterul">

                    <li class="active"><a href='' id="all"><img src="/web/assets/images/all.png"><span>All</span></a></li>
                    <li><a href='#' id="web"><img src="/web/assets/images/portfolio/item10.png"><span>Web</span></a></li>
                    <li><a href='#' id="mobile"><img src="/web/assets/images/portfolio/item11.png"><span>Mobile</span></a></li>
                    <li><a href='#' id="design"><img src="/web/assets/images/portfolio/item12.png" ><span>Design</span></a></li>
                </ul>
            </div>

            <div class="containercustom">
                <div class="col-md-l2">

                            @foreach($apps as $app)
                                        <?php $cats = '';?>
                                        @if ($app->web == 1)
                                            <?php $cats = $cats . ' web '; ?>
                                        @endif

                                        @if ($app->mobile == 1)
                                            <?php $cats = $cats.' mobile '; ?>
                                        @endif

                                        @if ($app->design == 1)
                                            <?php $cats = $cats.' design '; ?>
                                        @endif

                                        <div class="item">
                                        <div class="cats {{$cats}}">
                                            <a href="{{url('app/'.$app->id)}}">
                                                <img src="{{url('storage/'.$app->mainthumb)}}" alt="{{$app->title}}" class = "img-responsive">
                                                <span>{{$app->title}}</span>
                                            </a>

                                        </div>
                                        </div>

                                        @endforeach


                    </div>



                </div>
            </div>
        </div>




    <!-- ../section 2 -->



@endsection
