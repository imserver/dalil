@extends('nadsoftweb.layouts.Web_app')



@section('content')
    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#services').addClass('active');
        });
    </script>
    <!-- about style -->
    <link rel="stylesheet" href="{{ url('/web/assets/services.css') }}" />

    <!-- section 1 -->
    <div class="container-fluid" id="section1">
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-4" id="pagetitle">
                    <h2>SERVICES</h2>
                </div>
                <div class="col-md-1"></div>

                <div class="col-md-7 aos-item" id="intro" data-aos="fade-up">
                    <h2 class="maintitle " >OUR <span>SERVICES</span>  </h2>
                    <p class="aos-item" data-aos="zoom-in-up" >
                    {!!  $services->intduction !!}
                    </p>

                </div>



            </div>
        </div>
    </div>
    <!-- ../section 1 -->


    <!-- section 2 -->
    <div class="container-fluid" id="section2">
        <div class="row">
            <div class="col-lg-l2">
                <div class="d-none d-sm-block d-sm-none  d-md-block">
                <div class="col-md-4 aos-item" data-aos="zoom-in"  data-aos-offset="100" data-aos-easing="linear">
                    <img src="{{url('storage/'.$services->sec1_thumb)}}" class = "img-responsive"  id="mainimg" />
                </div>
                </div>
                <div class="col-md-1"></div>

                <div class="col-md-7  aos-item" data-aos="zoom-in-up" >
                    <h2 class="maintitle">{{$services->sec1_title}} </h2>
                    <p>
                        {!!  $services->sec1_desc !!}
                   </p>
                    <p class="subtitle">Technologies</p>
                    <div class="addedimgs">
                        <ul>
                            <li><img src="/web/assets/images/services/node-js-736399_960_720.png" /></li>
                            <li><img src="/web/assets/images/services/Mask Group 56.png" /></li>
                            <li><img src="/web/assets/images/services/Mask Group 57.png" /></li>
                            <li><img src="/web/assets/images/services/Mask Group 59.png" /></li>
                        </ul>




                    </div>
                </div>



            </div>
        </div>
    </div>
    <!-- ../section 2 -->


    <!-- section 3 -->

    <div class="container-fluid" id="section3">
        <div class="row">
            <div class="col-lg-l2">


                <div class="col-md-7 aos-item" data-aos="zoom-in-right" >
                    <h2 class="maintitle">{{$services->sec2_title}} </h2>
                    <p>
                        {!! $services->sec2_desc !!}
                    </p>
                    <p class="subtitle">Technologies</p>
                    <div class="addedimgs">
                        <ul>
                            <li><img src="/web/assets/images/services/Rectangle 232.png" /></li>
                            <li><img src="/web/assets/images/services/Mask Group 17.png" /></li>
                            <li><img src="/web/assets/images/services/Rectangle 230.png" /></li>
                            <li><img src="/web/assets/images/services/Rectangle 233.png" /></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-1"></div>


                <div class="d-none d-sm-block d-sm-none  d-md-block">
                <div class="col-md-4 aos-item" data-aos="zoom-in-right"  data-aos-offset="100" data-aos-easing="linear">
                    <img src="{{url('storage/'.$services->sec2_thumb)}}" id="mainimg" class = "img-responsive" />
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ../section 3 -->

    <!-- section 4 -->

    <div class="container-fluid" id="section4">
        <div class="row">
            <div class="col-lg-l2">
                <div class="d-none d-sm-block d-sm-none  d-md-block">
                <div class="col-md-4 aos-item" data-aos="zoom-in" data-aos-offset="100" data-aos-easing="linear"  >
                    <img src="{{url('storage/'.$services->sec3_thumb)}}" id="mainimg" class = "img-responsive" />
                </div>
                </div>
                <div class="col-md-1"></div>

                <div class="col-md-7 aos-item" data-aos="fade-up" >
                    <h2 class="maintitle">{{$services->sec3_title}}</h2>
                    <p>
                  {!! $services->sec3_desc !!}
                   </p>
                    <p class="subtitle ">Technologies</p>
                    <div class="addedimgs">
                        <ul>
                            <li><img src="/web/assets/images/services/Mask Group 18.png" /></li>
                            <li><img src="/web/assets/images/services/Rectangle 228.png" /></li>
                            <li><img src="/web/assets/images/services/Mask Group 19.png" /></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ../section 4 -->



    <!-- section 5 -->

    <div class="container-fluid" id="section5">
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-7 aos-item" data-aos="fade-right"    data-aos-easing="ease-in-sine">
                    <h2 class="maintitle">{{$services->sec4_title}}</h2>
                    <p>
                    {!! $services->sec4_desc !!}
                   </p>
                    <p class="subtitle">Technologies</p>
                    <div class="addedimgs">
                        <ul>
                            <li><img src="/web/assets/images/services/works_with_adobe_xd.png" /></li>
                            <li><img src="/web/assets/images/services/Mask Group 20.png" /></li>
                            <li><img src="/web/assets/images/services/Unknown.png" /></li>
                            <li><img src="/web/assets/images/services/zeplin-logo-25BAD7EC64-seeklogo.com.png" /></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-1"></div>


                <div class="d-none d-sm-block d-sm-none  d-md-block">
                <div class="col-md-4 aos-item" data-aos="fade-left"  data-aos-offset="100" data-aos-easing="linear" id="mainimg">
                    <img src="{{url('storage/'.$services->sec4_thumb)}}"  class = "img-responsive" />
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ../section 5 -->
@endsection
