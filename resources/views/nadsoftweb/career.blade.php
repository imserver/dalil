@extends('nadsoftweb.layouts.Web_app')



@section('content')
    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#careers').addClass('active');
        });
    </script>
    <!-- about style -->
    <link rel="stylesheet" href="{{ url('/web/assets/career.css') }}" />

    <!-- section 1 -->
    <div class="container-fluid" id="section1">
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-4" id="pagetitle">
                    <h2>careers</h2>
                </div>
                <div class="col-md-1"></div>

                <div class="col-md-7 aos-item" id="intro" data-aos="fade-up">
                    <h2 class="maintitle " >OUR <span>careers</span>  </h2>
                    <p>Planning a project is the most important part of the project’s life-story, so we believe that a perfect planning leading us to a perfect project.</p>
                    <p>

                        Planning a project is the most important so we believe that a perfect planning leading us to a perfect project. Planning a project is the most important</p>

                </div>



            </div>
        </div>
    </div>
    <!-- ../section 1 -->

    <!-- section 2 -->
    <div class="container-fluid" id="section3">
        <div class="row">
            <div class="col-md-12" id="intro">
                <h2 class="maintitle " >open <span>vacancy</span>  </h2>
                <p>Choose the location you want to browse its vacancies</p>
            </div>
        </div>
        <div class="row" id='filiter'>

            <div class="col-md-l2" style="text-align:center">


                <ul class="filiterul">
                    <li class="active"><a href='' id="amman"><span>Amman Office</span></a></li>
                    <li><a href='#' id="nazareth"><span>Nazareth Office</span></a></li>

                </ul>
            </div>

            <div class="containercustom">


                    @foreach($list as $vacancy)
                        <?php $cats = ''; $style= '';?>
                        @if ($vacancy->ammanoffice == 1)
                            <?php $cats = $cats . ' amman '; ?>
                            @else
                                <?php $style= ' display:none;'; ?>
                        @endif

                        @if ($vacancy->nazarehoffice == 1)
                            <?php $cats = $cats.' nazareth '; ?>
                        @endif



                            <a  href="#btnmodaal" class="cats {{$cats}} col-md-3"  data-title=" {{$vacancy->title}}" data-descr="{{$vacancy->description}}" style="{{$style}}">
                                {{$vacancy->title}}
                                <span>Job Dsscription</span>
                            </a>

                    @endforeach






            </div>
        </div>
    </div>




    <!-- ../section 2 -->






    <!-- Modal -->
    <!--
    <div class="modal " id="vacanymodaldetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>
                    </button>

                </div>

                <div class="modal-body" style=" padding: 5%;">

                </div>


            </div>
        </div>
    </div> -->

    <div id="vacanymodaldetailsdiv" ></div>

@endsection
