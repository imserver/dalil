@extends('nadsoftweb.layouts.Web_app')



@section('content')
    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#portfolio').addClass('active');
        });
    </script>
    <!-- about style -->
    <link rel="stylesheet" href="{{ url('/web/assets/portfolio_details.css') }}" />

    <!-- section 1 -->
    <div class="container-fluid" id="section1">
        <span class='leave1'></span>
        <span class='leave2'></span>
        <span class='leave3'></span>
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-4"></div>
                <div class="col-md-4" id="pagetitle">
                    <h2>PORTFOLIO</h2>
                </div>

                <div class="col-md-4"></div>
                <div class="col-md-12" id="intro">
                    <h2 class="maintitle aos-item" data-aos="fade-up"> <span> {{$appdetails->title}} </span>&nbsp;APP  </h2>

                    @if($appdetails->web == 1)
                        {{'Web Development'}}
                    @elseif($appdetails->mobile == 1)
                        {{'Mobile Development'}}
                    @elseif($appdetails->design == 1)
                        {{'Design UI/UX'}}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- ../section 1 -->



    <!-- section 2 -->
    <div class="container-fluid" id="section2">
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-4" id="smallappimg">
                    <span>{{$appdetails->title}}</span>
                    <img src="{{url('storage/'.$appdetails->sec1_thumb)}}" style="max-width:100%;"/>
                </div>
                <div class="col-md-1"></div>

                <div class="col-md-7" >
                    <h2 class="maintitle aos-item" data-aos="fade-up"> <span> Brief about </span> {{$appdetails->title}} App  </h2>
                    <p>
                        {{$appdetails->shortdesc}}
                    </p>


                </div>

            </div>
        </div>
    </div>
    <!-- ../section 2 -->


    <!-- section 3 -->
    <div class="container-fluid" id="section3">
        <div class="row">
            <div class="col-lg-l2">



                <div class="col-md-7" >
                    {!!  $appdetails->sec2_desc !!}

                </div>

                <div class="col-md-1"></div>
                <div class="col-md-4" id="mainimg" >
                    <span>SHOPPING</span>
                    @if($appdetails->sec2_thumb)
                    <img src="{{url('storage/'.$appdetails->sec2_thumb)}}" />
                        @endif
                </div>
            </div>
        </div>
    </div>
    <!-- ../section 3 -->



    <!-- section 4 -->

    <div class="container-fluid" id="section4">
        <div class="row">
            <div class="col-lg-l2">

                @if($appdetails->app_logo)
                <div class="col-md-12" style="text-align:center;">
                    <img src="{{url('storage/'.$appdetails->app_logo)}}" style="max-width: 200px;     margin-bottom: 4%;"/>
                </div>

                @endif
            {!! $appdetails->fulltext  !!}
                <!--
                <div class="col-md-12" style="text-align:center;">
                    <img src="assets\images\portfolio\detailspic1.png" />
                </div>


                <div class="col-md-12">
                    <p style="text-align:center;">
                        Planning a project is the most important part of the project’s life-story, so we believe that a perfect planning leading us to a perfect project.Planning a project is the most important part of the project’s life-story, so we believe that a perfect planning leading us to a perfect project.
                    </p>
                    <p>
                        Planning a project is the most important so we believe that a perfect planning leading us to a perfect project. Planning a project is the most important Planning a project is the most important part of the project’s life-story, so we believe that a perfect planning leading us to a perfect project.
                    </p>
                </div>  -->


            </div>
        </div>
    </div>
    <!-- ../section 4 -->

    <div class="col-md-12" style="text-align:center;">
        <img src="/web/assets/images/thankyou.png" style="    width: 100%;     margin-bottom: 7%;     margin-top: 7%;" class="d-none d-sm-none d-md-block"/>
    </div>





@endsection
