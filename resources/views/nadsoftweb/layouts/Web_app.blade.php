<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>
    <!--
    <link rel="stylesheet" href="{{ asset('/css/w3.css') }}"/> -->

    <!-- Bootstrap core CSS -->
    <link href="{{url('/web/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">


    <!-- animate -->
    <link rel="stylesheet" href="{{url('/web/assets/animate/aos-master/dist/aos.css')}}" />





    <link rel="stylesheet" href="{{ url('/web/assets/footer.css') }}" />
    <link rel="stylesheet" href="{{ url('/web/assets/header.css') }}" />


    <script src="{{url('/web/assets/animate/aos-master/dist/aos.js')}}"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="{{url('/web/vendor/jquery/jquery.slim.min.js')}}"></script>
    <script src="{{url('/web/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!--
    <script src="{{url('/web/assets/js/scrollifymaster/jquery.scrollify.js')}}"></script>
    -->

</head>


<body>


<div class="maincontainer" >


    @if(count($errors) > 0)
        <!-- Modal -->
                <div class="modal " id="errorsmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            @if (($errors->has('name')) || ($errors->has('phone'))  || ($errors->has('code'))  || ($errors->has('email'))  || ($errors->has('msg'))  )
                                <h2 class="maintitle"> Contact Us<span>  Form</span>   </h2>
                                @endif


                                @if (($errors->has('projectname')) || ($errors->has('projectaim'))  || ($errors->has('projectidea'))  || ($errors->has('fullname'))  || ($errors->has('projectemail')) || ($errors->has('projectphone')) || ($errors->has('notes'))   )
                                    <h2 class="maintitle"> Start Project<span>  Form</span>   </h2>
                                @endif

                                @if (($errors->has('name')) || ($errors->has('phone'))  || ($errors->has('code'))  || ($errors->has('email'))  || ($errors->has('msg'))  )
                            <button type="button" class="closecontacterrors" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>
                            </button>
                                    @endif

                                @if (($errors->has('projectname')) || ($errors->has('projectaim'))  || ($errors->has('projectidea'))  || ($errors->has('fullname'))  || ($errors->has('projectemail')) || ($errors->has('projectphone')) || ($errors->has('notes'))   )
                                    <button type="button" class="closestartproject" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>
                                    </button>
                                @endif


                                @if (($errors->has('empname')) || ($errors->has('empphone'))  || ($errors->has('empemail'))  || ($errors->has('empcv'))   )
                                <h2 class="maintitle"> Job<span>  Form</span>   </h2>

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>
                                    </button>
                                @endif
                        </div>

                    <div class="modal-body" style=" padding: 5%;">
                        @foreach ($errors->all() as $error)
                            <p class="text-left alert alert-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                    </div>
                </div>
            </div>
        @endif



        @if(Session::has('success'))
            <!-- Modal -->
                <div class="modal " id="errorsmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>
                                    </button>

                            </div>

                            <div class="modal-body" style=" padding: 5%;">
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

    @endif
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-l2">
                    <div class="col-md-4 col-xs-12" >
                        <a class="navbar-brand" href="{{url('/')}}" id="logo"  >
                            <img src="/web/assets/images/logo.png" alt="Nadsoft" title="Nadsoft"  class ="img-responsive"/>
                        </a>
                    </div>

                    <div class="col-md-8 ">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active" id="home">
                                    <a class="nav-link" href="{{url('/')}}" style="min-width: auto;">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </li>
                                <li class="nav-item" id="about">
                                    <a class="nav-link" href="{{url('/about')}}" style="min-width: 85px;">About</a>
                                </li>
                                <li class="nav-item" id="services">
                                    <a class="nav-link" href="{{url('/services')}}">Services</a>
                                </li>
                                <li class="nav-item" id="portfolio">
                                    <a class="nav-link" href="{{url('/portfolio')}}">Portfolio</a>
                                </li>

                                <li class="nav-item" id="careers">
                                    <a class="nav-link" href="{{url('/jobs')}}">Careers</a>
                                </li>


                                <li class="nav-item" id="contact">
                                    <a class="nav-link" href="#section7">Contact</a>
                                </li>

                                <!--
                                <li class="nav-item" >
                                    <a class="nav-link" href="#section7" id="btnblue" data-toggle="modal" data-target="#exampleModal">Start Project</a>
                                </li> -->

                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </nav>






    <!-- Modal -->
    <div class="modal " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="maintitle"> START<span>  PROJECT</span>   </h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p style="color: #707070;  text-align: center;   font-size: 25px;">Provide us with your project information, and start our different experience.</p>
                    <form action="{{ route('sendsendproject') }}" id="startprojectform" method="post" >
                        {{ csrf_field() }}
                        <div class="form-group col-md-12">
                        <p>Project Information:</p>
                        </div>
                        <div class="form-group col-md-6">
                        <input type="text" name="projectname" value="" placeholder="Project Name" class="form-control">
                        </div>

                        <div class="form-group col-md-6">
                        <input type="text" name="projectaim" value="" placeholder="Field Project refers to " class="form-control">
                        </div>

                        <div class="form-group col-md-12">
                        <input type="text" name="projectidea" value="" placeholder="Project idea" class="form-control">
                        </div>

                        <div class="form-group col-md-12">
                        <p>Your Information:</p>
                        </div>
                        <div class="form-group col-md-6">
                        <input type="text" name="fullname" value="" placeholder="Your Name" class="form-control">
                        </div>

                        <div class="form-group col-md-6">
                        <input type="text" name="projectemail" value="" placeholder="Your Email" class="form-control" >
                        </div>

                        <div class="form-group col-md-6">
                        <input type="text" name="projectphone" value="" placeholder="Your Phone Number" class="form-control">
                        </div>

                        <div class="form-group col-md-6">
                        <input type="text" name="notes" value="" placeholder="Notes" class="form-control" >
                        </div>

                        <div class="form-group col-md-12" style="text-align: center;">
                        <input type="submit" id="btnblue" value="Send Request"><span style="    position: absolute;     right: 285px; margin-right: 18px;">
                        </div>
                    </form>
                </div>

                <!--
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> -->
            </div>
        </div>
    </div>


    <!-- Main Content -->
    @yield('InnerHeder')
    @yield('content')



<!-- section 7 -->

    <div class="container-fluid " id="section7">
        <div class="row" >
            <h2 class="maintitle aos-item" data-aos="zoom-out-up">Want to <span>know more ?</span></h2>
                <div class="col-md-5 aos-item" data-aos="fade-down">
                    <div class="form">
                        <h3>Send us a message</h3>
                        <p>We will contact you to give you details you need</p>
                        <form action="{{ route('saveContact') }}" id="contform" method="post" name="contactform" >
                            {{ csrf_field() }}
                            <label for="fullname"></label>
                            <input type="text" name='name' value='' placeholder='Enter Your Full Name' class='inputform' />
                            <div id="fullnameerror"></div>
                            <input type="text " class='inputform2' name="code" placeholder="ex: 962" maxlength="3"/>



                            <input type="text" name='phone' value='' placeholder='00 0 000 000' class='inputform3'  maxlength="10"/>
                            <div id="div1"></div>
                            <div id="div2"></div>

                            <input type="text" name='email' value='' placeholder='Enter Your Email Address' class='inputform' />
                            <div id="emailerror"></div>
                            <textarea class='' name='msg' placeholder='Write Your Message here …'></textarea>
                            <div id="msgerror"></div>

                            <a type='submit' id="contactbtnsubmit" class="readmore"><span>Send My Info</a>
                        </form>
                    </div>
                </div>

                <div class="col-md-7 aos-item" data-aos="fade-up" style="position:relative">

                        <div class="col-md-6" id="map1">
                            <div class='inner'>
                                <span id="title">Nazareth</span>
                                <span id="desc">Nazareth, Industrial park, 2nd floor</span>

                                <!--
                                <div style="position: relative; display: inline-block;">
                                    <img src="/web/assets/images/call-answer.png" style="position: absolute; top: 28px; left: 18px; width: 20px; "/>
                                    <input type='button' value='Call us'  class='claaus' id='btnblue' data-toggle="tooltip" data-placement="top" title="000 - 00000"/>
                                </div>



                                <div style="position: relative; display: inline-block;">
                                    <img src="/web/assets/images/google.png" style="position: absolute; top: 27px; left: 18px; width: 20px;" />
                                <input type='button' value='Visit us'  class='visitus1' id='btnblue'/>
                                </div> -->
                            </div>
                        </div>


                        <div class="col-md-6" id="map2">
                            <div class='inner'>
                                <span id="title">Amman</span>
                                <span id="desc">Prince Zain Street, Um Alsummag, Bawabet Khelda Bldg# 88 3rd Floor, Office #202 Amman - Jordan</span>

                                <!--
                                <div style="position: relative; display: inline-block;">
                                    <img src="/web/assets/images/call-answer.png" style="position: absolute; top: 28px; left: 18px; width: 20px;"/>
                                <input type='button' value='Call us'  class='claaus' id='btnblue' data-toggle="tooltip" data-placement="top" title="000 - 00000"/>
                                </div>

                                <div style="position: relative; display: inline-block;">
                                    <img src="/web/assets/images/google.png" style="position: absolute; top: 27px; left: 18px; width: 20px;" />
                                <input type='button'value='Visit us'  class='visitus' id='btnblue'/>
                                </div>
                                -->
                            </div>
                        </div>




                    <p id='paragraph1'>It’s in our DNA to make you satisfied, excited, rewarded and super happy!</p>
                    <p id='paragraph2'>NADSOFT managed to acquire and retain world-class talents in all departments</p>
                </div>



        </div>
    </div>
    <!-- ../section 7 -->
</div> <!-- end full contailer -- main -->

<div class="container-fluid d-none d-sm-none d-md-block  " id="section8">
    <div class="floatingbutton">

        <a  data-toggle="modal" data-target="#exampleModal">Start Project</a>
    </div>
</div>

<footer>
    <div class="footerrightdiv">
        <span id="shadow1"></span>
        <span id="shadow2"></span>
        <span id="shadow3"></span>
        <span id="shadow4"></span>
    </div>

    <div class="footerleftdiv">
        <!--
        <span id="shadow5"></span> -->
        <!--
        <span id="shadow6"></span> -->
    </div>
    <div style="max-width:1400px; float:none; margin:auto;" >
    <div class="container-fluid">


            <div class="col-lg-12">
                <img src="/web/assets/images/logofooter.png" id="footerlogo"  />
            </div>

        <div class="col-md-12" style="padding: 0px;">
            <div class="col-md-8" id="leftpart">
                <ul>
                    <li><a href="{{url('/about')}}">ABOUT US</a></li>
                    <li><a href="{{url('/jobs')}}">CAREERS</a></li>
                    <li><a href="{{url('/portfolio')}}">PORTFOLIO</a></li>
                    <li><a href="{{url('/services')}}">SERVICES</a></li>
                    <!--
                    <li><a href="#">GALLERY</a></li>

                    <li><a href="#">MEDIA CENTER</a></li> -->
                    <li><a href="#section4">TEAM</a></li>
                    <!--
                    <li><a href="#">HELP</a></li> -->

                    <li><a href="#">TERMS & CONDITIONS</a></li>
                    <li><a href="#">Privacy policy</a></li>
                    <!--
                    <li><a href="#">FAQ</a></li> -->
                </ul>
            </div>
            <div class="col-md-4" id="rightpart">
                <p id="socialtxt">Follow us on our social media accounts</p>
                <ul class="social">
                    <li><a href="https://www.facebook.com/NADSoftIT/" target="_blank"><img src="/web/assets/images/facebook.png" /></a></li>
                    <li><a href="#"><img src="/web/assets/images/twitter.png" /></a></li>
                    <!--
                    <li><a href="#"><img src="/web/assets/images/google-plus.png" /></a></li> -->
                    <li><a href="#"><img src="/web/assets/images/linkedin.png" /></a></li>
                </ul>
            </div>
        </div>


            <div class="col-lg-12">
                <p class="copyright">
                    <span>All rights reserved</span>
                    <img src="/web/assets/images/copyright.png" />
                    <span>&copy; 2019</span>
                </p>
            </div>


    </div>
    </div>
</footer>








<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>




<script>

    $(document).ready(function(){
        $('a[href="#btnmodaal"]').on('click',function(){
            var title = $(this).data("title");
            var desc = $(this).data("descr");

            var html =  '<div class="modal " id="vacanymodaldetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
            html += '<div class="modal-dialog modal-dialog-centered" role="document">';
            html += '<div class="modal-content">';
            html += '<div class="modal-header">';
            html += '<div class="col-md-12" id="intro"> <h2 class="maintitle " >'+title+'</h2><p style="text-align: center;">Job Application</p></div>';
            html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
            html += '<span aria-hidden="true" style="color: #E94150; font-size: 35px; opacity: 1;">&times;</span>';
            html += '</button>';
            html += '</div>';
            html += '<div class="modal-body" >';
            html += '<div class="col-md-12"> <h2 class="maintitle " style="text-align: left;     font-size: 20px;" >Job Description</h2>';
            html += '<div id="jobdec">'+desc+'</div></div>';

            html += '<div class="col-md-12"> <h2 class="maintitle " style="text-align: left;     font-size: 20px;" >Application Form</h2>';
            html += '</div>';
            html += '<div id="applicationform"> ';

            html += '<form action="{{ route("jobformsend") }}" id="vacancyform" method="post" name="vacancyform" enctype="multipart/form-data">';
            html += '{{ csrf_field() }}';
            html += '<div class="col-md-6"><input type="text" name="empname" value="" placeholder="Your Name" class="inputform" /></div>';
            html += '<div class="col-md-6"><input type="text" name="empemail" value="" placeholder="Your Email" class="inputform" /></div>';
            html += '<div class="col-md-6"><input type="text" name="empphone" value="" placeholder="Your Phone Number" class="inputform" /></div>';
            html += '<div class="col-md-6"><input type="file" name="cv" value="" placeholder="Uploade your resume" class="inputform cv" /></div>';
            html += '<input type="hidden" name="formtype" value="vacancy"  />';
            html += '<input type="submit"   value="Send Information" class="readmore"/><span>';


            html += '</form>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';  // modalWindow
            html += '</div>';  // modalWindow
            html += '</div>';  // modalWindow


            $("#vacanymodaldetailsdiv").html(html);
            $("#vacanymodaldetails").modal();
            $("#vacanymodaldetails").modal('show');

        });


    });





    $(function () {

        $('#errorsmodal').modal('show');
        $('[data-toggle="tooltip"]').tooltip()
    });



    $('#contactbtnsubmit').click(function () {
            $('#contform').submit();
    });




    $('.closestartproject').click(function () {
        $('#errorsmodal').modal('hide');
        $('#exampleModal').modal('show');
    });

    $('.closecontacterrors').click(function () {
        $('#errorsmodal').modal('hide');
        window.location.href = "/#section7";
    });

    $(document).on('click', '.visitus', function(e){
        e.preventDefault();
        var url = 'https://www.google.com/maps/place/Prs.+Zeinab+St.,+Amman/@31.9803977,35.8473782,19z/data=!3m1!4b1!4m5!3m4!1s0x151ca197902cbec9:0x443120effc660073!8m2!3d31.9804024!4d35.8479296';
        window.open(url, '_blank');
    });

    AOS.init({
        easing: 'ease-in-out-sine'
    });

    setInterval(300);


    $('.arcContain').hide();

    $(window).scroll(function () {
        var a = $('.arcContain');
        var c = $('.archide');
        var p = c.offset().top;
        var q = $(window).scrollTop();
        var r = $('.arcContain').parent().offset().top;
        console.log(q);

        if (p - q < 320) {

            $('.archide').addClass("archideLeft");
            $('.arcContain').show();
        } else {
            if ( q < 400) {
                //if (p < 1679) {

                $('.archide').removeClass("archideLeft");
                $('.arcContain').hide();
            }
        }


        if(p >= 1679){
            $('.archide').addClass("archideLeft");
            $('.arcContain').show();
        }

    });


    /*counter code */
    (function ($) {
        $.fn.countTo = function (options) {
            options = options || {};

            return $(this).each(function () {
                // set options for current element
                var settings = $.extend({}, $.fn.countTo.defaults, {
                    from:            $(this).data('from'),
                    to:              $(this).data('to'),
                    speed:           $(this).data('speed'),
                    refreshInterval: $(this).data('refresh-interval'),
                    decimals:        $(this).data('decimals')
                }, options);

                // how many times to update the value, and how much to increment the value on each update
                var loops = Math.ceil(settings.speed / settings.refreshInterval),
                    increment = (settings.to - settings.from) / loops;

                // references & variables that will change with each update
                var self = this,
                    $self = $(this),
                    loopCount = 0,
                    value = settings.from,
                    data = $self.data('countTo') || {};

                $self.data('countTo', data);

                // if an existing interval can be found, clear it first
                if (data.interval) {
                    clearInterval(data.interval);
                }
                data.interval = setInterval(updateTimer, settings.refreshInterval);

                // initialize the element with the starting value
                render(value);

                function updateTimer() {
                    value += increment;
                    loopCount++;

                    render(value);

                    if (typeof(settings.onUpdate) == 'function') {
                        settings.onUpdate.call(self, value);
                    }

                    if (loopCount >= loops) {
                        // remove the interval
                        $self.removeData('countTo');
                        clearInterval(data.interval);
                        value = settings.to;

                        if (typeof(settings.onComplete) == 'function') {
                            settings.onComplete.call(self, value);
                        }
                    }
                }

                function render(value) {
                    var formattedValue = settings.formatter.call(self, value, settings);
                    $self.html(formattedValue);
                }
            });
        };

        $.fn.countTo.defaults = {
            from: 0,               // the number the element should start at
            to: 0,                 // the number the element should end at
            speed: 1000,           // how long it should take to count between the target numbers
            refreshInterval: 100,  // how often the element should be updated
            decimals: 0,           // the number of decimal places to show
            formatter: formatter,  // handler for formatting the value before rendering
            onUpdate: null,        // callback method for every time the element is updated
            onComplete: null       // callback method for when the element finishes updating
        };

        function formatter(value, settings) {
            return value.toFixed(settings.decimals);
        }
    }(jQuery));

    jQuery(function ($) {
        // custom formatting example
        $('.count-number').data('countToOptions', {
            formatter: function (value, options) {
                return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
            }
        });

        // start all the timers
        $('.timer').each(count);

        function count(options) {
            var $this = $(this);
            options = $.extend({}, options || {}, $this.data('countToOptions') || {});
            $this.countTo(options);
        }
    });
    /*../counter code */

    $('a#web').click(function () {
        $('.filiterul li').removeClass('active');
        $(this).parent().addClass('active');
       $('.cats').hide();
       $('.cats.web').show();
        return false;
    });

    $('a#mobile').click(function () {
        $('.filiterul li').removeClass('active');
        $(this).parent().addClass('active');
        $('.cats').hide();
        $('.cats.mobile').show();
        return false;
    });

    $('a#design').click(function () {
        $('.filiterul li').removeClass('active');
        $(this).parent().addClass('active');
        $('.cats').hide();
        $('.cats.design').show();
        return false;
    });


    $('a#all').click(function () {
        $('.filiterul li').removeClass('active');
        $(this).parent().addClass('active');
        $('.cats').show();

        return false;
    });


    $('a#amman').click(function () {
        $('.filiterul li').removeClass('active');
        $(this).parent().addClass('active');
        $('.cats').hide();
        $('.cats.amman').show();
        return false;
    });


    $('a#nazareth').click(function () {
        $('.filiterul li').removeClass('active');
        $(this).parent().addClass('active');
        $('.cats').hide();
        $('.cats.nazareth').show();
        return false;
    });

</script>

</body>
</html>
