@extends('nadsoftweb.layouts.Web_app')



@section('content')
    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#about').addClass('active');
        });
    </script>
    <!-- about style -->
    <link rel="stylesheet" href="{{ url('/web/assets/about.css') }}" />
    <!-- section 1 -->
    <div class="container-fluid" id="section1">
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-4" id="pagetitle">
                    <h2>ABOUT US</h2>
                </div>
                <div class="col-md-1"></div>

                <div class="col-md-7 aos-item" id="intro" data-aos="fade-up">
                    <h2 class="maintitle " >WHO<span>WE ARE ?</span>  </h2>
                    {!! $about->desc !!}
                </div>



            </div>
        </div>
    </div>
    <!-- ../section 1 -->



    <!-- section 2 -->
    @if($about)
    <div class="container-fluid" id="section2">
        <div class="">
            <div class="col-lg-l2">
                <div class="col-md-4 aos-item" data-aos="fade-down" id="innerbox">
                    <span id="icon"><img src="/web/assets/images/about/startup-2.png" /></span>
                    <span id="title">Startups</span>
                    {!! $about->service1_desc !!}
                </div>

                <div class="col-md-4 aos-item" data-aos="zoom-in" id="innerbox">
                    <span id="icon"><img src="/web/assets\images\about\entrepreneur-2.png" /></span>
                    <span id="title">Individual Entrepreneurs</span>
                    {!! $about->service2_desc !!}
                </div>

                <div class="col-md-4 aos-item" data-aos="fade-up" id="innerbox">
                    <span id="icon"><img src="/web/assets\images\about\building.png" /></span>
                    <span id="title">Organisations / Businesses</span>

                    {!! $about->service3_desc !!}
                </div>

            </div>
        </div>
    </div>
    @endif
    <!-- ../section 2 -->


    <!-- section 3 -->
    <div class="container-fluid" id="section3">

        <h2 class="maintitle aos-item" data-aos="fade-up"> OUR Partners <span>& Clients</span> </h2>
        <!-- snow -->

        <span class="skystar"  aria-hidden="true"></span>

        <span class="skystar"  aria-hidden="true"></span>





        <span class="skystar red"  aria-hidden="true"></span>


        <div class="row">
            <div class="col-lg-l2 aos-item" data-aos="zoom-in-up">
            {!! $about->about_clients !!}
           </div>

            <div class="containercustom">
                <div class="col-md-l2">

                    <div class="d-none d-sm-block d-sm-none  d-md-block">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel"> <!--  -->
                        <!-- Indicators -->
                        @if(count($clients) > 10)
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <!--
                            <li data-target="#myCarousel" data-slide-to="2"></li> -->
                        </ol>
                        @endif

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php $index = 0;  $slide = 1;?>
                            @foreach($clients as $client)
                            @if(($slide==1) && ($index==0))
                                        <div class="item active">
                                @endif
                                            @if(($slide==2) && ($index==0))
                                        </div>
                                                <div class="item">
                                                    @endif
                                            <div>
                                                <img src="{{url('storage/'.$client->thumbbg)}}" alt="Nadsoft">
                                                <span><img src="{{url('storage/'.$client->thumb)}}" /></span>
                                            </div>
                                                <?php $index++; ?>
                                            @if($index==10)
                                                <?php $slide ++;
                                                        $index = 0;
                                                ?>
                                    @endif
                            @endforeach
                        </div>

                    </div>
                </div><!-- descktop slider -->

            </div>

                    <!-- mobile slider -->
                    <div class="d-block d-sm-none d-sm-none  d-none d-sm-block d-md-none">
                        <div id="myCarouselmobile" class="carousel slide" data-ride="carousel">
                        <?php $index = 0;  ?>
                            <ol class="carousel-indicators">
                        @foreach($clients as $client)
                            @if($index==0)
                                        <li data-target="#myCarouselmobile" data-slide-to="{{$index}}" class="active"></li>

                                @else
                                        <li data-target="#myCarouselmobile" data-slide-to="{{$index}}"></li>
                                    @endif
                                <?php $index++; ?>
                            @endforeach

                        </ol>
                            <?php $index = 0;  ?>

                        <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php $class='active'; ?>
                                @foreach($clients as $client)
                                        <div class="item {{$class}}">
                                            <div style="width: 100% !important; height: 70% !important;">
                                                <img src="{{url('storage/'.$client->thumbbg)}}" alt="Nadsoft">
                                                <span><img src="{{url('storage/'.$client->thumb)}}" /></span>
                                            </div>
                                        </div>
                                        <?php $class=''; ?>
                                    @endforeach



                    </div>
                    </div>
                    </div>
                    <!-- ../mobile slider -->
            </div>
        </div>


    </div>


    </div>


    <!-- ../section 3 -->


    <!-- section 4 -->
    <div class="container-fluid" id="section4">
        <div class="row" >
            <h2 class="maintitle aos-item" data-aos="fade-up"> OUR  <span>TEAM</span> </h2>
            <span class="skystar"></span>
            <div class="desc col-md-12 aos-item" data-aos="zoom-out-up">
                {!! $about->about_team !!}



            </div>
        </div>
    </div>



    <!-- ../section 4 -->


@endsection
