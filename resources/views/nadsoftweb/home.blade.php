@extends('nadsoftweb.layouts.Web_app')



@section('content')

    <script>
        $(document).ready(function() {
            $('ul.navbar-nav li').removeClass('active');
            $('li#home').addClass('active');
        });
    </script>

    <!-- home style -->

    <link rel="stylesheet" href="{{ url('/web/assets/home.css') }}" />
    <!-- section 1 -->
    <div class="container-fluid" id="section1">
        <div class="row">
            <div class="col-lg-l2">
                <div class="col-md-7 col-xs-12" id="textpart">
                    <div class="col-md-8 col-xs-12" style="padding-left: 0px;">


                        <div class="d-block d-sm-none col-xs-12">
                            <img src="/web/assets/images/img.png" class = "img-responsive" />
                            <a class="nav-link" href="#section7" id="btnbluemobile" data-toggle="modal" data-target="#exampleModal">Start Project</a>

                        </div>

                        <p class="star"></p>
                        <span class="skystar"></span>
                        <h2 class="maintitle"><span>Why</span> Nadsoft?</h2>


                        @if($about)
                        {!! $about->introduction !!}

                        @endif
                    </div>
                </div>


                <div class="col-md-5 col-xs-12" id="aboutimgintro">
                    <div class="innercontainer">
                        <span class="skystar"></span>

                        <!-- main image as animation -->
                        <span id="leaf"></span>
                        <span id="leaf2"></span>
                        <img src="/web/assets/images/girl.gif" id="img1"/>
                        <span id="img2"></span>
                        <img src="/web/assets/images/img23.png" class="mainimg" />
                        <!-- ../main image as animation -->
                    </div>
                </div>
            </div>
        </div>






    </div>
    <!-- ../section 1 -->



    <!-- section 2 -->
    <div   class="container-fluid" id="section2">

            <div class="col-md-l2">
                <div class="row">
                        @foreach($parteners as $logo)
                            <div class=" col-md-2 col-xs-6">
                                <div class="photo">
                                    <img src="{{url('storage/'.$logo->thumb)}}" alt="{{$logo->desc}}"   title="{{$logo->desc}}" class="filitergray"/>
                                    <div class="glow-wrap">
                                        <i class="glow"></i>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                </div>
            </div>

    </div>
    <!-- ../section 2 -->


    <!-- section 3 -->
    @if($histories)
    <div class="container-fluid " id="section3">

        <h2 class="maintitle aos-item" data-aos="fade-up"><span>Our</span> History</h2>
        <!-- snow -->
        <div class="d-sm-none d-md-block d-none d-sm-block" style="position: absolute">
        <span class="skystar"  aria-hidden="true"></span>
        <span class="skystar"  aria-hidden="true"></span>
        <span class="skystar red"  aria-hidden="true"></span>
        </div> <!-- responsive hidden -->


        <!-- for mobile just -->
        <div class="d-none  d-sm-block d-md-none d-block d-sm-none"> <!--  -->
        <div id="myCarousel" class="carousel slide " data-ride="carousel" style="    z-index: 9;"> <!--  -->
            <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                    <!--
                    <li data-target="#myCarousel" data-slide-to="2"></li> -->
                </ol>


        <!-- Wrapper for slides -->
            <?php $class = ' active'; ?>
            <div class="carousel-inner">
                @foreach($histories as $history)
                <div class="item {{$class}}" style="text-align: center;">
                    <div class="bubblemobile">
                        <span class="bubbleicon"><img src="{{url('storage/'.$history->thumb)}}" style="    max-width: 69px;"/></span>
                        <span class="bubblecounter"><h2 class="timer count-title count-number" data-to="{{$history->years}}" data-speed="1500" ></h2></span>
                        <span class="bubbledesc">{{$history->title}}</span>
                    </div>
                </div>
                    <?php $class = ''; ?>
                    @endforeach
            </div>


        </div>
        </div>

        <!-- ../for mobile just -->



        <div class="d-sm-none d-md-block d-none d-sm-block">
        <div class="bubbles-container1 ">
            <div class="row">

                <div class="aos-item bubble" data-aos="zoom-out-down" id="bubble1">
                    <span class="bubbleicon"><img src="{{url('storage/'.$histories[0]->thumb)}}" style="    max-width: 69px;"/></span>
                    <span class="bubblecounter"><h2 class="timer count-title count-number" data-to="{{$histories[0]->years}}" data-speed="1500" ></h2></span>
                    <span class="bubbledesc">{{$histories[0]->title}}</span>
                </div>


                <div class="aos-item bubble" data-aos="zoom-up" id="bubble2">
                    <span class="bubbleicon"><img src="{{url('storage/'.$histories[1]->thumb)}}" style="    max-width: 69px;" /></span>
                    <span class="bubblecounter"><h2 class="timer count-title count-number" data-to="{{$histories[1]->years}}" data-speed="1500"></h2></span>
                    <span class="bubbledesc">{{$histories[1]->title}}</span>
                </div>


                <div class="aos-item bubble" data-aos="zoom-out-up" id="bubble3">
                    <span class="bubbleicon"><img src="{{url('storage/'.$histories[4]->thumb)}}" style="    max-width: 69px;" /></span>
                    <span class="bubblecounter"><h2 class="timer count-title count-number" data-to="{{$histories[4]->years}}" data-speed="1500"></h2></span>
                    <span class="bubbledesc">{{$histories[4]->title}}</span>
                </div>

            </div>
        </div>
        </div>



        <!-- dashed line -->
        <div class="dashedline">

            <div class="arccontainer">
                <div class="arcContain">
                    <div class="archide"><!-- archide archideLeft -->
                        <div class="arc"></div>
                    </div>

                </div>


            </div>

            <div class="arccontainer1">
                <div class="arcContain">
                    <div class="archide"> <!--  archideLeft -->
                        <div class="arc"></div>
                    </div>
                </div>
            </div>

            <div class="icon1 aos-item" data-aos="zoom-out">
                <img src="/web/assets/images/web-development.png" />
            </div>

            <div class="arccontainer2">
                <div class="arcContain">
                    <div class="archide"><!-- archide archideLeft -->
                        <div class="arc"></div>
                    </div>
                </div>
            </div>


            <div class="icon2 aos-item" data-aos="fade-up">
                <img src="/web/assets/images/start-up-2.png" />
            </div>


            <div class="arccontainer3">
                <div class="arcContain">
                    <div class="archide"><!-- archide archideLeft -->
                        <div class="arc"></div>
                    </div>
                </div>
            </div>




            <div class="icon3 aos-item" data-aos="zoom-out">
                <img src="/web/assets/images/smartphone.png" />
            </div>


            <div class="arccontainer4">
                <div class="arcContain">
                    <div class="archide"><!-- archide archideLeft -->
                        <div class="arc"></div>
                    </div>
                </div>
            </div>


            <div class="arccontainer5">
                <div class="arcContain">
                    <div class="archide"><!-- archide archideLeft -->
                        <div class="arc"></div>
                    </div>
                </div>
            </div>



        </div>




        <!-- ../dashed line -->



        <div class="d-sm-none d-md-block d-none d-sm-block">
        <div class="bubbles-container2">
            <div class="row">

                <div class="aos-item bubble" data-aos="zoom-out-down" id="bubble4">
                    <span class="bubbleicon"><img src="{{url('storage/'.$histories[2]->thumb)}}" style="    max-width: 69px;" /></span>
                    <span class="bubblecounter"><h2 class="timer count-title count-number" data-to="{{$histories[2]->years}}" data-speed="1500"></h2></span>
                    <span class="bubbledesc">{{$histories[2]->title}}</span>
                </div>


                <div class="aos-item bubble" data-aos="zoom-up" id="bubble5">
                    <span class="bubbleicon"><img src="{{url('storage/'.$histories[3]->thumb)}}" style="    max-width: 69px;" /></span>
                    <span class="bubblecounter"><h2 class="timer count-title count-number" data-to="{{$histories[3]->years}}" data-speed="1500"></h2></span>
                    <span class="bubbledesc">{{$histories[3]->title}}</span>
                </div>


            </div>
        </div>
        </div>




        <div class="clear"></div>
        <!-- more btn -->
        <div class="container" style="max-height: 74px;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="btn-container aos-item" data-aos="zoom-out-up">
                        <a href="{{url('/about')}}" class="readmore"><span> More About Us</a>
                    </div>
                </div>
            </div>
        </div>


    </div> <!-- added -->
@endif


    <!-- ../section 3 -->


    <!-- section 4 -->
    @if($services)
    <div class="container-fluid " id="section4">

            <h2 class="maintitle aos-item" data-aos="zoom-out-up">What <span>We do?</span></h2>
            <span class="skystar"></span>
            <div class="desc col-md-12 aos-item" data-aos="zoom-out-up">

                {!! $services->intduction !!}

            </div>

            <div class="col-lg-12" style="text-align:center">
            <div class="row" >
                <div class="col-md-3 box1-sect4 aos-item" data-aos="fade-up">
                    <span id="icon"><img src="/web/assets/images/coding.png" /></span>
                    <span id="title">{{$services->sec1_title }}</span>
                    <hr/>
                    <div id="additionalimages" class="aos-item" data-aos="fade">
                        <a href="#" > <img src="/web/assets/images/node-js-736399_960_720.png"  width="80"/></a>
                        <img src="/web/assets/images/img1.png" width="31" />
                        <img src="/web/assets/images/img2.png" width="36"/>
                        <img src="/web/assets/images/img3.png" width="44" />
                    </div>
                </div>

                <div class="col-md-3 box2-sect4 aos-item" data-aos="fade-down">
                    <span id="icon"><img src="/web/assets/images/coding-3.png" /></span>
                    <span id="title">{{$services->sec2_title }}</span>
                    <hr/>
                    <div id="additionalimages" class="aos-item" data-aos="fade">
                        <img src="/web/assets/images/img4.png" width="32"/>
                        <img src="/web/assets/images/img5.png" width="32"/>
                        <img src="/web/assets/images/img6.png" width="32"/>
                        <img src="/web/assets/images/img7.png" width="45"/>
                    </div>
                </div>

                <div class="col-md-3 box3-sect4 aos-item" data-aos="fade-up">
                    <span id="icon"><img src="/web/assets/images/development.png" /></span>
                    <span id="title">{{$services->sec3_title }}</span>
                    <hr/>
                    <div id="additionalimages" class="aos-item" data-aos="fade">
                        <img src="/web/assets/images/img8.png"  width="60"/>
                        <img src="/web/assets/images/img9.png" width="55"/>
                        <img src="/web/assets/images/img10.png" width="80" />
                    </div>
                </div>

                <div class="col-md-3 box4-sect4 aos-item" data-aos="fade-down">
                    <span id="icon"><img src="/web/assets/images/Page-1.png" /></span>
                    <span id="title">{{$services->sec4_title }}</span>
                    <hr/>
                    <div id="additionalimages" class="aos-item" data-aos="fade">
                        <img src="/web/assets/images/img11.png" width="38"/>
                        <img src="/web/assets/images/img12.png" width="34"/>
                        <img src="/web/assets/images/img13.png" width="36"/>
                        <img src="/web/assets/images/img14.png" width="49"/>
                    </div>
                </div>
            </div>
            </div>



        </div>


        <!-- more btn -->
        <div class="container" id="sec4btn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="btn-container aos-item" data-aos="fade-up">
                        <a href="{{url('/services')}}" class="readmore"> <span>More Services</a>
                    </div>
                </div>
            </div>
        </div>





@endif

    <!-- ../section 4 -->


    <!-- section 5 -->
    @if(count($apps) > 0)
    <div class="container-fluid " id="section5">
        <div class="row" >
            <h2 class="maintitle aos-item" data-aos="zoom-out-up"><span> Our</span> portfolio</h2>
            <p class="aos-item" data-aos="fade-up" id="shortdesc">We make the difference</p>
        </div>


        <div class="row">
            <div class="col-lg-12">

                <div id="portfolio1" class="aos-item" data-aos="fade-up">

                    <span id="icon"><img src="{{url('storage/'.$apps[0]->mainthumb)}}" /></span>
                    <div id="portfolio-desc">
                        <h1>{{$apps[0]['title']}}</h1>
                        <p>{{$apps[0]['shortdesc'] }}</p>
                        <a href="{{url('app/'.$apps[0]['id'])}}" id="readmore2">Read More</a>
                    </div>
                </div>

                <div id="portfolio2" class="aos-item" data-aos="fade-down">

                    <span id="icon"><img src="{{url('storage/'.$apps[1]->mainthumb)}}"  /></span>
                    <div id="portfolio-desc">
                        <h1>{{$apps[1]['title']}}</h1>
                        <p>{{$apps[1]['shortdesc'] }}</p>
                        <a href="{{url('app/'.$apps[1]['id'])}}" id="readmore2">Read More </a>
                    </div>
                </div>


                <div id="portfolio3" class="aos-item" data-aos="fade-up">

                    <span id="icon"><img src="{{url('storage/'.$apps[2]->mainthumb)}}"/></span>
                    <div id="portfolio-desc">
                        <h1>{{$apps[2]['title']}}</h1>
                        <p>{{$apps[2]['shortdesc'] }}</p>
                        <a href="{{url('app/'.$apps[2]['id'])}}" id="readmore2">Read More</a>
                    </div>
                </div>


                <div id="portfolio4" class="aos-item" data-aos="fade-down">

                    <span id="icon"><img src="{{url('storage/'.$apps[3]->mainthumb)}}" /></span>
                    <div id="portfolio-desc">
                        <h1>{{$apps[3]['title']}}</h1>
                        <p>{{$apps[3]['shortdesc'] }}</p>
                        <a href="{{url('app/'.$apps[3]['id'])}}" id="readmore2">Read More</a>
                    </div>
                </div>



                <div id="portfolio5" class="aos-item" data-aos="fade-up">

                    <span id="icon"><img src="{{url('storage/'.$apps[4]->mainthumb)}}" /></span>
                    <div id="portfolio-desc">
                        <h1>{{$apps[4]['title']}}</h1>
                        <p>{{$apps[4]['shortdesc'] }}</p>
                        <a href="{{url('app/'.$apps[4]['id'])}}" id="readmore2">Read More</a>
                    </div>
                </div>



                <div id="portfolio6" class="aos-item" data-aos="fade-down">
                    <span id="icon"><img src="{{url('storage/'.$apps[5]->mainthumb)}}"  /></span>
                    <div id="portfolio-desc">
                        <h1>{{$apps[5]['title']}}</h1>
                        <p>{{$apps[5]['shortdesc'] }}</p>
                        <a href="{{url('app/'.$apps[5]['id'])}}" id="readmore2">Read More</a>
                    </div>
                </div>

            </div>
        </div>

    </div>
    @endif
    <!-- ../section 5 -->



    @if(count($workprogress)  > 0)
    <!-- section 6 -->
    <div class="container-fluid" id="section6">
        <div class="row" >
            <h2 class="maintitle aos-item" data-aos="zoom-out-up">OUR <span> WORK PROCESS</span></h2>
        </div>
        <div class="row" >
            <div class="col-lg-12">

                <div id="cloud1" class="aos-item" data-aos="fade-up" data-aos-duration="3000">
                    <h1>{{$workprogress[0]->title}}</h1>
                    {!! $workprogress[0]->desc !!}
                </div>


                <div id="cloud2" class="aos-item" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="4000">
                    <h1>{{$workprogress[1]->title}}</h1>
                    {!! $workprogress[1]->desc !!}
                </div>



                <div id="cloud3" class="aos-item" data-aos="fade-down-left"  data-aos-delay="100"  data-aos-easing="ease-in-sine" data-aos-duration="1500">
                    <h1>{{$workprogress[2]->title}}</h1>
                    {!! $workprogress[2]->desc !!}
                </div>

                <div id="cloud4" data-aos="fade-down-right"  data-aos-delay="700"  data-aos-easing="ease-in-sine" data-aos-duration="1800">
                    <h1>{{$workprogress[3]->title}}</h1>
                    {!! $workprogress[3]->desc !!}
                </div>



                <div id="cloud5" class="aos-item" data-aos="fade-up-left"  data-aos-delay="450"  data-aos-easing="ease-in-sine" data-aos-duration="6800">
                    <h1>{{$workprogress[4]->title}}</h1>
                    {!! $workprogress[4]->desc !!}
                </div>



                <div id="cloud6" class="aos-item" data-aos="fade-up-right"  data-aos-delay="600"  data-aos-easing="ease-in-back" data-aos-duration="1800">
                    <h1>{{$workprogress[5]->title}}</h1>
                    {!! $workprogress[5]->desc !!}

                </div>

                <div id="cloud7" class="aos-item" data-aos="fade-up-right"  data-aos-delay="300"  data-aos-easing="ease-in-back" data-aos-duration="1800">
                    <h1>{{$workprogress[6]->title}}</h1>
                    {!!  $workprogress[6]->desc !!}
                </div>


            </div>
        </div>
    </div>
    @endif
    <!-- ../section 6 -->





@endsection
