<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Paertener extends Model
{
    protected $table = "paerteners";
    protected $fillable = [
        'title',
        'thumb',
        'desc',
        'thumbbg',
    ];
}
