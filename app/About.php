<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class About extends Model
{
    use Translatable;
    protected $translatable = [
        'section1',
        'section2',
        'fulldescription',
        'sect1_title',
        'sect3_title',
    ];
    protected $table = "abouts";
    protected $fillable = [
        'section1',
        'section2',
        'fulldescription',
        'sect1_title',
        'sect3_title'
    ];





}
