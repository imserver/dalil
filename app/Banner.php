<?php

namespace App;

use App\BannersPlace;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;


class Banner extends Model
{
    protected $table = "banners";
    protected $fillable = [
        'image',

    ];

    public function places()
    {
        return $this->belongsTo('App\BannersPlace');
    }

    public function getAdsByPlace($slug,$limit=null){
       // $limit = ($limit)?$limit:1;

       $date = Carbon::now();

       $banner = Array();
       $place = BannersPlace::where('slug',$slug)->first();
       if(!empty($place)){
            $banners = $place->banners()
                            ->where([
                                        ['banners.is_active', '1'],
                                        ['banners.fdate','<=',$date],
                                        ['banners.tdate','>=',$date]
                                    ])
                            ->inRandomOrder()
                            ->take($limit)
                            ->get();
            foreach($banners as $bnr){
                if($bnr['script'])
                    $banner[] = $bnr['script'];
                else {
                    if(isset($bnr['link'])){
                        $link = $bnr['link'];
                    } else if(isset($bnr['page'])){
                        $link = route('banner.show',$bnr['id']);
                    } else $link = "";

                    if($link)
                        $banner[] = '<a href="'.$link.'"><img src="'. asset('storage/'.$bnr['image']) .'" class="img-responsive"></a>';
                    else
                        $banner[] = '<img src="'. asset('storage/'.$bnr['image']) .'" class="img-responsive">';

                }
            }
        }
        return $banner;
    }
}
