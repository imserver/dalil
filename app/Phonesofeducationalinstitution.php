<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Phonesofeducationalinstitution extends Model
{
    use Translatable;
    protected $translatable = ['fullname'];
    protected $table = "phonesofeducationalinstitution";
    protected $fillable = [
        'fullname',
        'phone',
        'location'
    ];
}
