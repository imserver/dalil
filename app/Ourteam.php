<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Ourteam extends Model
{
    protected $table = "ourteams";
    protected $fillable = [
        'name',
        'jobtitle',
        'thumb',
    ];
}
