<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class VideoCategory extends Model
{
    protected $table = 'video_categories';

    public function videos()
    {
        return $this->belongsToMany('App\Video','categories_videos','video_category_id','video_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_cat');
    }

    public function subcategory()
    {
        return $this->hasMany(self::class, 'parent_cat');
    }
}
