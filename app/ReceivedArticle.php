<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ReceivedArticle extends Model
{
    protected $table = 'received_articles';
    protected $fillable = ['title','name', 'details'];

}
