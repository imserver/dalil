<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Service extends Model
{
    protected $table = "services";
    protected $fillable = [
        'sec1_thumb',
        'sec1_desc',
        'sec2_thumb',
        'sec2_desc',
        'sec3_thumb',
        'sec3_desc',
        'sec4_thumb',
        'sec4_desc',
        'sec1_title',
        'sec2_title',
        'sec3_title',
        'sec4_title',
        'intduction',
    ];
}
