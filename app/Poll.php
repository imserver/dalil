<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Database\Eloquent\Model;


class Poll extends Model
{
    public function answers()
    {
        return $this->hasmany('App\PollAnswer','poll_id');
    }

    public function getActivePoll(){
        $date = Carbon::now();

        $poll = $this->where([
                            ['closed', '0'],
                            ['fdate','<=',$date],
                            ['tdate','>=',$date]
                        ])->first();

        return $poll;
    }

    public function hasVoted(){
        if(Cookie::has('poll_'.$this->id)) return true;
        else return false;
    }

    public function totalVotes()
    {
        return $this->answers->sum('votes');
    }

    public function votesPercent($optionVotesCount = 0)
    {
        $totalVotes = $this->totalVotes();

        // $optionVotesCount = $this->votes;
        if ($optionVotesCount == 0 && $totalVotes == 0)
        {
            return 0;
        }
        return round(($optionVotesCount / $totalVotes) * 100);
    }



}
