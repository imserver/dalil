<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BannersSlider extends Model
{
    protected $table = "banners_sliders";
    protected $fillable = [
        'title_ar',
        'image',
        'link',
        'is_active',
        'title_he'
    ];
}
