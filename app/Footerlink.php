<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Footerlink extends Model
{

    use Translatable;
    protected $translatable = [
        'section1_title',
        'section1_text1',
        'section1_text2',
        'section1_text3',
        'section1_text4',
        'section1_text5',
        'section2_title',
        'section2_text1',
        'section2_text2',
        'section2_text3',
        'section2_text4',
        'section2_text5',
        'section3_title',
        'section3_text1',
        'section3_text2',
        'section3_text3',
        'section3_text4',
        'section3_text5',
        'section4_title',
        'section4_text1',
        'section4_text2',
        'section4_text3',
        'section4_text4',
        'section4_text5'
    ];
    protected $table = 'footerlinks';
    protected $fillable = [
        'section1_title',
        'section1_link1',
        'section1_text1',
        'section1_link2',
        'section1_text2',
        'section1_link3',
        'section1_text3',
        'section1_link4',
        'section1_text4',
        'section1_link5',
        'section1_text5',
        'section2_title',
        'section2_link1',
        'section2_text1',
        'section2_link2',
        'section2_text2',
        'section2_link3',
        'section2_text3',
        'section2_link4',
        'section2_text4',
        'section2_link5',
        'section2_text5',
        'section3_title',
        'section3_link1',
        'section3_text1',
        'section3_link2',
        'section3_text2',
        'section3_link3',
        'section3_text3',
        'section3_link4',
        'section3_text4',
        'section3_link5',
        'section3_text5',
        'section4_title',
        'section4_link1',
        'section4_text1',
        'section4_link2',
        'section4_text2',
        'section4_link3',
        'section4_text3',
        'section4_link4',
        'section4_text4',
        'section4_link5',
        'section4_text5'
    ];
}
