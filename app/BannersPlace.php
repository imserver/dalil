<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BannersPlace extends Model
{
    public function banners()
    {
        return $this->hasmany('App\Banner','place');
    }
}
