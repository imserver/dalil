<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class CategoriesPost extends Model
{
    use Translatable;
    protected $translatable = [];
    protected $table = "categories_posts";
    protected $fillable = ['post_id', 'category_id'];
}
