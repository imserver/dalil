<?php

use App\Page;
use App\Post;
use App\Video;
use App\Category;
use App\VideoCategory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\URL;


function embedMetas($page,$slug=null,$id=null) {

    $site_name = setting('site.title');
    $image = setting('site.image');
    $keywords = str_replace(" ",",",setting('site.keywords'));

    switch ($page) {

        case "main":
            $title = setting('site.title');
            $description = setting('site.description');

        break;

        case "PostCategory":
            $category = Category::where('slug',$slug)->firstOrFail();
            $title = $category->name.' - '.setting('site.title');
            $description = ($category->sdata)?$category->sdata:setting('site.description');
            $image = ($category->picture)?getImage($category->picture):$image;
        break;

        case "Post":
            $post = Post::whereSlug($slug)->firstOrFail();
            $title = ($post->seo_title)?$post->seo_title.' - '.setting('site.title'):$post->title.' - '.setting('site.title');
            $description = ($post->meta_description)?$post->meta_description:setting('site.description');
            $keywords = ($post->meta_keywords)?$post->meta_keywords:$keywords;
            $image =getImage($post->image,$post->crop_image);
        break;

        case "Page":
            $page = Page::whereSlug($slug)->firstOrFail();
            $title = ($page->seo_title)?$page->seo_title.' - '.setting('site.title'):$page->title.' - '.setting('site.title');
            $description = ($page->meta_description)?$page->meta_description:setting('site.description');
            $keywords = ($page->meta_keywords)?$page->meta_keywords:$keywords;
            $image =getImage($page->image,$page->crop_image);
        break;

        case "VideoCategory":
            $category = VideoCategory::where('slug',$slug)->firstOrFail();
            $title = $category->title.' - '.setting('site.title');
            $description = setting('site.description');
            $image = ($category->picture)?getVidImage($category->picture):$image;
        break;

        case "Video":
            $video = Video::whereSlug($slug)->firstOrFail();
            $title = $video->title.' - '.setting('site.title');
            $description = ($video->details)?$video->details:setting('site.description');
            $image =getVidImage($video->image,$video->crop_image);
        break;


    }



    $title = fixString($title);
    $site_name = fixString($site_name);
    $description = fixString($description);

    $meta = [
        'site_name' => $site_name,
        'title' => str_limit($title,60),
        'description' => $description,
        'keywords' => $keywords,
        'url' => URL::current(),
        'image' => $image
    ];

    return $meta;
}

function fixString($text) {
    $text = str_replace('"',"",$text);
    $text = str_replace("'","",$text);
    $text = htmlspecialchars($text);
    $text = str_replace("\n","",$text);
    $text = str_replace("\r","",$text);
    $text = str_replace("\n\r","",$text);
    $text = str_replace("\l","",$text);
    return ($text);
}

function getImage($path=null,$crop=null,$width=null,$height=null,$ratio=false)
{
    $fileInfo = pathinfo(storage_path().$path);

        if ($crop) {
        $cropName = $fileInfo['basename'];
        if(file_exists('storage/croped/'.$cropName))
            $image = 'storage/croped/'.$cropName;
        else $image = uploadCropImage($crop,$cropName);
    } elseif($path && file_exists('storage/'.$path)){
        $image = 'storage/'.$path;
    } else{
        $image = 'img/not-found.jpg';
        $fileInfo = pathinfo(storage_path().$image);
    }

    $ext = $fileInfo['extension'];
    if ($ratio) $thumbName = "ratio-" . $width."-".$height.$fileInfo['filename'].".".$ext;
    else $thumbName = $width."-".$height.$fileInfo['filename'].".".$ext;

    if(!isset($width) && !isset($height) && file_exists('storage/'.$path)) $thumb = 'storage/'.$path;
    else if(!isset($width) && !isset($height) && !file_exists('storage/'.$path)) $thumb = 'img/not-found.jpg';
    else $thumb = 'storage/thumbs/'.$thumbName;

    if((!file_exists($thumb))){

        $img = resizeImage($image,$width,$height,$ratio);
    } else $img = asset($thumb);

    return $img;
}

function uploadCropImage($blbImg,$name)
{

    $image = $blbImg;
    list($type, $image) = explode(';', $image);
    list(, $image)      = explode(',', $image);

    $image = base64_decode($image);
    $image_name= $name;
    $path = public_path('storage/croped/'.$image_name);

    file_put_contents($path, $image);
    return 'storage/croped/'.$image_name;
}

function resizeImage($path,$width=null,$height=null,$ratio=false)
{

    $image = $path;
    $fileInfo = pathinfo(storage_path().$path);
    $ext = $fileInfo['extension'];

    if (strpos($path, 'img.youtube') !== false) {
        $getvdid = explode('/',$path);
        $filename = $getvdid[4].$fileInfo['filename'];
    } else  $filename = $fileInfo['filename'];

    if ($ratio) $thumb = "ratio-" . $width."-".$height.$filename.".".$ext;
    else $thumb = $width."-".$height.$filename.".".$ext;

    $destinationPath = public_path('storage/thumbs');

    if (!file_exists($destinationPath)) {

        mkdir($destinationPath, 0755, true);
    }

    $img = Image::make($image);

    $img->fit($width, $height)->save($destinationPath.'/'.$thumb);

    // if($ratio){
    //     $img->resize($width, $height, function ($constraint) {
    //         $constraint->aspectRatio();
    //     })->save($destinationPath.'/'.$thumb);
    // } else{
    //     $img->resize($width, $height, 'center', true)->save($destinationPath.'/'.$thumb);
    // }

    return asset('/storage/thumbs/'.$thumb);

    /*
    // crop the best fitting 5:3 (600x360) ratio and resize to 600x360 pixel
    $img->fit(600, 360);

    // crop the best fitting 1:1 ratio (200x200) and resize to 200x200 pixel
    $img->fit(200);

    // add callback functionality to retain maximal original image size
    $img->fit(800, 600, function ($constraint) {
        $constraint->upsize();
    });
    */

    /*
    // resize image to fixed size
    $img->resize(300, 200);

    // resize only the width of the image
    $img->resize(300, null);

    // resize only the height of the image
    $img->resize(null, 200);

    // resize the image to a width of 300 and constrain aspect ratio (auto height)
    $img->resize(300, null, function ($constraint) {
        $constraint->aspectRatio();
    });

    // resize the image to a height of 200 and constrain aspect ratio (auto width)
    $img->resize(null, 200, function ($constraint) {
        $constraint->aspectRatio();
    });

    // prevent possible upsizing
    $img->resize(null, 400, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
    });
    */

    /*
    // resize image canvas
    $img->resizeCanvas(300, 200);

    // resize only the width of the canvas
    $img->resizeCanvas(300, null);

    // resize only the height of the canvas
    $img->resizeCanvas(null, 200);

    // resize the canvas by cutting out bottom right position
    $img->resizeCanvas(300, 200, 'bottom-right');

    // resize the canvas relative by setting the third parameter to true
    $img->resizeCanvas(10, -10, 'center', true);

    // set a background-color for the emerging area
    $img->resizeCanvas(1280, 720, 'center', false, 'ff00ff');
    */


}

function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : str_random(25);

        $file = $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);

        return $file;
    }

function getVidImage($path=null,$crop=null,$width=null,$height=null,$ratio=false)
{

    if (strpos($path, 'graph.facebook') !== false) {
        return ($path);
    } else {
        $fileInfo = pathinfo(storage_path().$path);

        if ($crop) {
            $cropName = $fileInfo['basename'];
            if(file_exists('storage/croped/'.$cropName))
                $image = 'storage/croped/'.$cropName;
            else $image = uploadCropImage($crop,$cropName);
        } elseif(filter_var($path, FILTER_VALIDATE_URL)){
            $image = $path;
        } elseif($path && file_exists('storage/'.$path)){
            $image = 'storage/'.$path;
        } else{
            $image = 'img/not-found.jpg';
            $fileInfo = pathinfo(storage_path().$image);
        }

        $ext = $fileInfo['extension'];
        if (strpos($path, 'img.youtube') !== false) {
            $getvdid = explode('/',$path);
            $filename = $getvdid[4].$fileInfo['filename'];
        } else  $filename = $fileInfo['filename'];

        if ($ratio) $thumbName = "ratio-" . $width."-".$height.$filename.".".$ext;
        else $thumbName = $width."-".$height.$filename.".".$ext;


        if(!isset($width) && !isset($height)) $thumb = 'storage/'.$path;

        else $thumb = 'storage/thumbs/'.$thumbName;


        if((!file_exists($thumb))){

            $img = resizeImage($image,$width,$height,$ratio);
        } else $img = asset($thumb);

        return $img;

    }


}
