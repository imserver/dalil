<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Phonesofmunicipalitie extends Model
{
    use Translatable;
    protected $translatable = ['fullname'];
    protected $table = "phonesofmunicipalitie";
    protected $fillable = [
        'fullname',
        'phone',
        'location'
    ];
}
