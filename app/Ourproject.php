<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Ourproject extends Model
{
    protected $table = "ourprojects";
    protected $fillable = [
        'desc',
    ];
}
