<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Workprogress extends Model
{
    protected $table = "workprogresses";
    protected $fillable = [
        'title',
        'desc'
    ];
}
