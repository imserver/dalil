<?php

namespace App;

use TCG\Voyager\Traits\Resizable;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Category extends Model
{

    use Translatable;
   // use Resizable;

    protected $translatable = [
        'name',
        'slug'
    ];
    protected $table = 'categories';
    protected $fillable = [
        'parent_id',
        'order',
        'name',
        'slug',
        'picture',
        'show_main',
        'main_design',
        'sdata',
        'is_active',
        'items',
        'items_plugin',
        'color',
        'shortdesc'
    ];

    // public function posts()
    // {
    //     return $this->belongsToMany('App\Post');
    // }

    public function posts(){
        return $this->belongsToMany('App\Post','categories_posts','category_id','post_id');
    }

    public function parent(){
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function subcategory(){
        return $this->hasMany(self::class, 'parent_id');
    }

}
