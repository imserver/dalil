<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Phonesofgovernmentoffice extends Model
{
    use Translatable;
    protected $translatable = ['fullname'];
    protected $table = "phonesofgovernmentoffice";
    protected $fillable = [
        'fullname',
        'phone',
        'location'
    ];
}
