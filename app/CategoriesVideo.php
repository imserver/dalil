<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CategoriesVideo extends Model
{
    protected $table = "categories_videos";
    protected $fillable = ['video_id', 'video_category_id'];
}
