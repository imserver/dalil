<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Phonesofpoliceandfirefighter extends Model
{
    use Translatable;
    protected $translatable = ['fullname'];
    protected $table = "phonesofpoliceandfirefighter";
    protected $fillable = [
        'fullname',
        'phone',
        'location'
    ];
}
