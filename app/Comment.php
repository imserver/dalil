<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ['name', 'body', 'is_active', 'post_id'];

    /*
    public function posts()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }*/

}
