<?php

namespace App\Providers;

use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\ServiceProvider;
use App\FormFields\CropHandler;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addFormField(CropHandler::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      //  Voyager::addAction(\App\Actions\CommentsAction::class);
        Voyager::addAction(\App\Actions\PublishAction::class);
      //  Voyager::addAction(\App\Actions\PollsAction::class);
    }
}
