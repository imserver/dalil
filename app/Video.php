<?php

namespace App;

use MediaEmbed\MediaEmbed;
use Illuminate\Database\Eloquent\Model;


class Video extends Model
{
    protected $table = 'videos';

    public function categories()
    {
        return $this->belongsToMany('App\VideoCategory','categories_videos','video_id','video_category_id');
    }

    public function getVideoUrl($url,$file,$attr=array(),$param=array()){

        if($url){

        $MediaEmbed = new MediaEmbed();

        $id = $host = $embedCode = null;
        $MediaObject = $MediaEmbed->parseUrl($url);
        if ($MediaObject) {
            $MediaObject->setParam([
                //'autoplay' => 1,
                //'loop' => 1
            ]);
            if(sizeof($param)>0) $MediaObject->setParam($param);

            $MediaObject->setAttribute([
                'type' => null,
                //'type' => 'text/html',
                'class' => 'video-content',
                'width' => '100%',
                'height' => '100%',
                'frameborder' => '0',
                'allow' => 'autoplay; fullscreen',
                'style' => 'position: absolute;top: 0;display: none;'
            ]);

            if(sizeof($attr)>0) $MediaObject->setAttribute($attr);

            $id = $MediaObject->id();
            $host = $MediaObject->slug();
            $embedCode = $MediaObject->getEmbedCode();
        }
//dd($MediaObject->getEmbedCode());
    } else if($file){

            $embedCode = '<video width="100%" height="100%" controls style="position: absolute;top: 0;display: none;" class="video-content-file" autoplay>
                <source src="'.asset("storage/".json_decode($file)[0]->download_link).'" type="video/mp4">
            </video>';

    }

        return($embedCode);
    }


     //get video thumbnail for facebook and youtube
    function get_vid_thumbnail($link){
        $thumbnail='';

        $MediaEmbed = new MediaEmbed();

        $id = $host = $embedCode = null;
        $MediaObject = $MediaEmbed->parseUrl($link);

        if ($MediaObject) {
            $id = $MediaObject->id();
            $host = $MediaObject->slug();
        }

    //check if video link is facebook
        if ($host== 'facebook') {
            $thumbnail=$this->fb_thumb($id);
            //$thumbnail='fb';
        }
    //check if video link is youtube
        if ($host=='youtube') {
            $thumbnail=$this->youtube_thumb($id);
            //$thumbnail='youtube';
        }
        return $thumbnail;
    }

    //supporting functions
    //get youtube thumbnail
    function youtube_thumb($id){
        $img='https://img.youtube.com/vi/'.$id.'/0.jpg';
        return $img;
    }

    //get the thumbnail
    function fb_thumb($id) {
        $img = 'https://graph.facebook.com/'.$id.'/picture';
        return $img;
    }



}
