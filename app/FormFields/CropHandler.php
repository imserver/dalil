<?php

namespace App\FormFields;

use TCG\Voyager\FormFields\AbstractHandler;

class CropHandler extends AbstractHandler
{
    protected $codename = 'crop';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.crop', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
