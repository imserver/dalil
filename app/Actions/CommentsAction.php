<?php

namespace App\Actions;

use App\Comment;
use TCG\Voyager\Actions\AbstractAction;

class CommentsAction extends AbstractAction
{
    public function getTitle()
    {

        // $comments = Comment::where([
        //     ['post_id', $this->data->{$this->data->getKeyName()}],
        //     ['is_active', '0'],
        // ])->get();

        $comments = Comment::where('post_id', $this->data->{$this->data->getKeyName()})->get();
        //return 'Comments';
        return count($comments);
    }

    public function getIcon()
    {
        return 'voyager-chat';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-left',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.comments.index', array('key'=>'post_id','filter'=>'equals','s'=>$this->data->{$this->data->getKeyName()}));
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'posts';
    }
    
}