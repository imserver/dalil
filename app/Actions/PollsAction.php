<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class PollsAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Add Answer';
    }

    public function getIcon()
    {
        return 'voyager-plus';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        //return route('voyager.poll-answers.index', array('key'=>'poll_id','filter'=>'equals','s'=>$this->data->{$this->data->getKeyName()}));
        return route('voyager.poll-answers.create');
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'polls';
    }
}
