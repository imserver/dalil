<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class App extends Model
{
    protected $table = "apps";
    protected $fillable = [
        'sec1_thumb',
        'sec1_desc',
        'sec2_thumb',
        'sec2_desc',
        'fulltext',
        'title',
        'mainthumb',
        'shortdesc',
    ];
}
