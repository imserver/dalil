<?php

namespace App\Http\Controllers;

use App\Page;
use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\SiteController;

class PageController extends SiteController
{
    public function show($slug){

        $meta = embedMetas('Page',$slug);

        $page = Page::where([
                                ['slug',$slug],
                                ['status','ACTIVE']
                            ])->firstOrFail();
        $getBigBnrs = new Banner;
        $bigBanner = $getBigBnrs->getAdsByPlace('bigbnr');
        $cubeBanner = $getBigBnrs->getAdsByPlace('cubebnr');

        return view('theme.pages.index')->with([
            'page' => $page,
            'meta' => $meta,
            'bigBanner' => $bigBanner,
            'bigBnrCount' => 0,
            'cubeBanner' => $cubeBanner,
            'cubeBnrCount' => 0,
            'gifts' => $this->getGifts(10),
            'horos' => $this->getHoros()
        ]);
    }
}
