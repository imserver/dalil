<?php

namespace App\Http\Controllers;

use App\Gift;
use App\Poll;
use App\Post;
use DOMDocument;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SiteController extends Controller
{

    function getMeta($type = 'main') {
        $meta = embedMetas($type);
        return $meta;
    }

    function getPoll(){
        $poll = $PollAnswers = [];
        $getpoll = new Poll;
        $poll = $getpoll->getActivePoll();
        if($poll)
        $PollAnswers = $poll->answers()->get();

        return ['poll' => $poll,
                'PollAnswers' => $PollAnswers
                ];
    }

    function GetCatsByPlugin($plugin){

        $catsPlugin = [];

        $catsPlugin = Category::where([
            ['show_main','1'],
            ['is_active','1'],
            ['main_design',$plugin]
        ])
        ->orderBy('order', 'Asc')
        ->get();

        return $catsPlugin;
    }

    function getPostsByPlugin($plugin){

        $PostsPlugin = [];

        $categories = $this->GetCatsByPlugin($plugin);
        foreach ($categories as $cat) {
            $category = Category::find($cat->id);
            $PostsPlugin[$category->id] = $category->posts()
                                        ->where([
                                            ['posts.is_active', '1'],
                                            ['posts.status','published']
                                        ])
                                        ->orderBy('posts.order', 'Asc')
                                        ->take($cat->items_plugin)->get();
        }
        return $PostsPlugin;
    }

    function getArchievePlugin(){
        $ArchievePlugin = $this->GetCatsByPlugin('Archive');
        return $ArchievePlugin;
    }

    function getArchieveCats(){
        $categories = $this->getArchievePlugin();
        $ArchieveCats = [];

        foreach ($categories as $cat) {
            $category = Category::find($cat->id);
            $ArchieveCats[$category->id] = $category->subcategory()
                                                    ->where([
                                                        ['show_main','1'],
                                                        ['is_active','1'],
                                                    ])
                                                    ->orderBy('order', 'Asc')
                                                    ->get();
        }

        return $ArchieveCats;

    }

    function getMainPosts($limit=1){
        $posts = Post::where([
            ['status', 'PUBLISHED'],
            ['is_active','1'],
            ['featured', '1'],
        ])
        ->orderBy('order', 'Asc')
        ->take($limit)->get();

        return $posts;
    }

    function getGifts($limit=1) {
        $gifts = Gift::where([
            ['is_active','1']
        ])
        ->orderBy('order', 'Asc')
        ->take($limit)->get();

        return $gifts;
    }

    function getHoros(){
        $get_horos = file_get_contents('http://plugins.prod.nadsoft.co/horos.php');
        $array = json_decode($get_horos, true);

        return $array;
    }

}
