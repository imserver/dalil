<?php

namespace App\Http\Controllers;

use App\App;
use App\Career;
use App\Contact;
use Illuminate\Http\Request;
use Mail;
use App\Subscriptionrenewal;

class ContactController extends Controller
{
    public function index(){

        /*
        $meta = embedMetas('main');

        $getBnrs = new Banner;
        $bigBanner = $getBnrs->getAdsByPlace('bigbnr');

        return view('theme.pages.contact')->with([
            'meta' => $meta,
            'bigBanner' => $bigBanner,
            'bigBnrCount' => 0
        ]);
*/
        return redirect('/#section7')->withInput();

    }


    public function rerequest(Request $request){

        $rules = [
            'email' => 'required|email',
            'msg' => 'required',
        ];

        $customMessages = [
            'email.required' => '"Email" field is required.',
            'email.email' => '"Email" field not valid!!',
            'msg.required' => '"Message" field is required.',
        ];

        $this->validate($request, $rules, $customMessages);

        $subscriptionrenewal = new Subscriptionrenewal();
        $subscriptionrenewal->email = $request->input('email');
        $subscriptionrenewal->msg = $request->input('msg');
        $subscriptionrenewal->save();

        // Added by ibtisam send email
        Mail::send('email',
            array(
                'email' => $request->input('email'),
                'bodyMessage' => $request->input('msg')
            ), function($message)use($subscriptionrenewal)
            {
                $message->from($subscriptionrenewal->email);
                $message->to('ibtisam@nadsoft.net', 'Dalil')->subject('Dalil Subscription Renewal');
            });
        return back()->with('success', 'Thank you!');

    }

    public function saveContact(Request $request){
      /*  $validatedData = $request->validate([
            'name' => 'required',
            'phone' => 'required|numeric|max:9|min:9',
            'placeholder' => 'required|max:4',
            'email' => 'required'
        ]);
*/


        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'phone' => 'required|min:14|max:14',
            'email' => 'required|email',
            'msg' => 'required',
        ];

/*
        $customMessages = [
            'name.required' => '"Full Name" field is required.',
            'email.required' => '"Email" field is required.',
            'email.email' => '"Email" field not valid!!',
            'phone.required' => '"Phone" field is required.',
            'phone.numeric' => 'The phone number entered is not valid.',
            'msg.required' => '"Message" field is required.',
            'phone.min' => 'The phone number must be 14 digits.'
        ];*/



        $customMessages = [
            'name.required' => 'حقل الاسم الكامل مطلوب <br/> שדה שם מלא נדרש',
            'email.required' => 'حقل البريد الإلكتروني مطلوب <br/> שדה דוא"ל נדרש',
            'email.email' => 'حقل البريد الإلكتروني غير صالح! <br/> שדה דוא"ל לא תקף !!',
            'phone.required' => 'حقل الهاتف مطلوب <br/> שדה טלפון נדרש',
            'phone.numeric' => 'رقم الهاتف الذي تم إدخاله غير صالح <br/> מספר הטלפון שהוזן אינו תקף',
            'msg.required' => 'حقل الرسالة مطلوب <br/> שדה הודעה נדרש',
            'phone.min' => 'يجب أن يكون رقم الهاتف 14 رقما <br/> מספר הטלפון חייב להיות 14 ספרות'
        ];


        $this->validate($request, $rules, $customMessages);

        $contact = new Contact();
        $contact->name = $request->input('name');
        $contact->phone = "+".$request->input('phone');
        $contact->email = $request->input('email');
        $contact->msg = $request->input('msg');

        $contact->save();

/*
        // Added by ibtisam send email
        Mail::send('email',
            array(
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'phone' => '+'.$request->input('phone'),
                'bodyMessage' => $request->input('msg')
            ), function($message)use($contact)
            {
                $message->from($contact->email);
                $message->to('ibtisam@nadsoft.net', 'Dalil')->subject('Dalil Register Form');
            });
        */
        session(['success' => "تم إرسال رسالتك بنجاح <br/>ההודעה שלך נשלחה בהצלחה"]);
       // return back()->withInput($request->all());


      return redirect()->back()->with(['success' => "تم إرسال رسالتك بنجاح <br/>ההודעה שלך נשלחה בהצלחה"]);
    }

    public function jobformsendfunc(Request $request){

        $rules = [
            'empname' => 'required',
            'empphone' => 'required|min:9',
            'empemail' => 'required|email',
            'cv' => 'required|file|max:1024',
        ];

        $customMessages = [
            'empname.required' => '"Full Name" field is required.',
            'empemail.required' => '"Email" field is required.',
           'empemail.email' => '"Email" field not validation.',
            'empphone.required' => '"Phone" field is required.',
            'empphone.min' => 'The phone number must be 9 digits.',
            'cv.required' => '"CV" field is required.',
            'cv.file' => 'Uploade  CV as file with max size 1MB',
        ];
        $this->validate($request, $rules, $customMessages);

        $data = array(
            'name'=> $request->empname,
            'email'=> $request->empemail,
            'phone'=> $request->empphone
        );

       //
        $cv = $request->file('cv');

        Mail::send('email',
            array(
                'name' => $request->get('empname'),
                'email' => $request->get('empemail'),
                'bodyMessage' => "Phone: ".$request->get('empphone')
        ), function($message)use($request, $cv)
            {
                $message->from($request->get('empemail'));
                $message->to('ibtisam@nadsoft.net', 'Nadsoft')->subject('Nadsoft Job Form');
                $message->attach($cv->getRealPath(), [
                    'as' => $cv->getClientOriginalName(),
                    'mime' => $cv->getMimeType()
                ]);

            });

        $list = Career::all();


        return redirect()->back()->with('success', 'Vacancy form sent successfully!, Thank you');
    }


    public function startproject(Request $request){
        $rules = [
            'projectname' => 'required',
            'projectaim' => 'required',
            'projectidea' => 'required',
            'fullname' => 'required',
            'projectemail' => 'required|email',
            'projectphone' => 'required',
            'notes' => 'required'
        ];


        $customMessages = [
            'projectname.required' => '"Project Name" field is required.',
            'projectaim.required' => '"Project Aim" field is required.',
            'projectidea.required' => '"Project Idea" field is required.',
            'fullname.required' => '"Full Name" field is required.',
            'projectemail.required' => '"Email" field is required.',
            'projectemail.email' => '"Email" field not valid!!',
            'projectphone.required' => '"Phone" field is required.',
            'notes.required' => '"Notes" field is required.'

        ];

        $this->validate($request, $rules, $customMessages);


        // Added by ibtisam send email
        Mail::send('email',
            array(
                'name' => $request->input('fullname'),
                'email' => $request->input('projectemail'),
                'bodyMessage' => $request->input('projectname').'/n'.$request->input('projectphone').'/n'.$request->input('projectaim').'/n'.$request->input('projectidea').'/n'.$request->input('notes')
            ), function($message)use($request)
            {
                $message->from($request->input('projectemail'));
                $message->to('ibtisam@nadsoft.net', 'Nadsoft')->subject('Nadsoft Contect Form');
            });
        return back()->with('success', 'Thank you, Your Project Information has been sent.');


        //   return redirect()->back()->with(['status' => 'MEssage Sent successfully.']);
    }

}
