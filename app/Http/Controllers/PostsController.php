<?php

namespace App\Http\Controllers;

use App\Footerlink;
use App\Post;
use App\Banner;
use App\Comment;
use App\Category;
use App\ReceivedArticle;
use Illuminate\Http\Request;
use App\Http\Controllers\SiteController;

class PostsController extends SiteController
{

    public function showPost($slug){

        $meta = embedMetas('Post',$slug);

        $post = Post::where([
                                ['slug',$slug],
                                ['is_active', '1'],
                                ['status','published']
                            ])
                            ->orderBy('order', 'Asc')
                            ->first();

        $category = $post->categories()
                        ->where('categories.is_active','1')->first();

        $comments = Comment::where([
                                    ['is_active','1'],
                                    ['post_id',$post->id]
                                    ])
                                ->get();

        $post->visits++;
        $post->save();

        $getBnrs = new Banner;
        $bigBanner = $getBnrs->getAdsByPlace('bigbnr');
        $cubeBanner = $getBnrs->getAdsByPlace('cubebnr');
        $tallBanner = $getBnrs->getAdsByPlace('tallbnr');

        $ArchievePlugin = $this->getArchievePlugin();
        $ArchieveCats = $this->getArchieveCats();
        // $post = $post->translate('en');
        // dd($post->title);
        if($post->external_link) return redirect($post->external_link);
        else return view('theme.posts.post')->with([
            'post' => $post,
            'category' => $category,
            'meta' => $meta,
            'comments' => $comments,
            'bigBanner' => $bigBanner,
            'bigBnrCount' => 0,
            'cubeBanner' => $cubeBanner,
            'cubeBnrCount' => 0,
            'tallBanner' => $tallBanner,
            'ArchievePlugin' => $ArchievePlugin,
            'ArchieveCats' =>$ArchieveCats,
            'gifts' => $this->getGifts(10),
            'horos' => $this->getHoros()

        ])->with($this->getPoll());
    }

    public function showCategory($slug){

        //$pagination = 20;

        $subPosts = [];

        $meta = embedMetas('PostCategory',$slug);

        $category = Category::where([
                                        ['slug',$slug],
                                        ['is_active', '1']
                                    ])
                                    ->orderBy('order', 'Asc')
                                    ->firstOrFail();

        $categoryName = $category->name;
        $categoryDesign = $category->main_design;

        $posts = $category->posts()
                          ->where([
                                ['posts.is_active', '1'],
                                ['posts.status','published'],
                            ])
                          ->orderBy('posts.order', 'Asc')
                          ->paginate($category->items);

        $subCats = $category->subcategory()
                            ->where('is_active','1')
                            ->orderBy('order', 'Asc')
                            ->get();

        foreach ($subCats as $subcat) {
            $category = Category::findOrFail($subcat->id);
            $subPosts[$category->id] = $category->posts()
                                                    ->where([
                                                        ['posts.is_active', '1'],
                                                        ['posts.status','published']
                                                    ])
                                                    ->orderBy('posts.order', 'Asc')
                                                    ->take(5)->get();
            }

        $getBnrs = new Banner;
        $bigBanner = $getBnrs->getAdsByPlace('bigbnr');
        $cubeBanner = $getBnrs->getAdsByPlace('cubebnr');

        if($categoryDesign=="Archive") $view = 'theme.posts.parentcat';
        else $view = 'theme.posts.cat';



        return view($view)->with([
                                    'category' => $category,
                                    'categoryName' => $categoryName,
                                    'posts' => $posts,
                                    'subCats' => $subCats,
                                    'subPosts' => $subPosts,
                                    'meta' => $meta,
                                    'bigBanner' => $bigBanner,
                                    'bigBnrCount' => 0,
                                    'cubeBanner' => $cubeBanner,
                                    'cubeBnrCount' => 0,
                                    'gifts' => $this->getGifts(10),
                                    'horos' => $this->getHoros()
                                ]);
    }


    public function addArt(){

        $getBigBnrs = new Banner;

        $meta = embedMetas('main');
        $bigBanner = $getBigBnrs->getAdsByPlace('bigbnr');
        $cubeBanner = $getBigBnrs->getAdsByPlace('cubebnr');

        return view('theme.posts.addArt')->with([
            'meta' => $meta,
            'bigBanner' => $bigBanner,
            'bigBnrCount' => 0,
            'cubeBanner' => $cubeBanner,
            'cubeBnrCount' => 0,
            'gifts' => $this->getGifts(10),
            'horos' => $this->getHoros()
        ]);
    }

    public function saveArt(Request $request){

        $meta = embedMetas('main');

        $art = new ReceivedArticle();

        $validatedData = $request->validate([
            'name' => 'required',
            'phone' => 'required|numeric',
            'title' => 'required',
            'image'     =>  'required|image|mimes:jpeg,png,jpg,gif'
        ]);

        if ($request->has('image')) {
            // Get image file
            $image = $request->file('image');
            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            // Define folder path
            $folder = '/uploads/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $art->image = $filePath;
        }

        $art->name = $request->input('name');
        $art->phone = $request->input('phone');
        $art->title = $request->input('title');
        $art->details = $request->input('details');

        $art->save();

        return redirect()->back()->with(['status' => 'Article Send successfully.']);
    }

    public function search(Request $request)
    {
        $meta = embedMetas('main');

        $getBnrs = new Banner;
        $bigBanner = $getBnrs->getAdsByPlace('bigbnr');
        $cubeBanner = $getBnrs->getAdsByPlace('cubebnr');

        $request->validate([
            'query' => 'required|min:3',
        ]);

        $query = $request->input('query');

        $posts = Post::where('title', 'like', "%$query%")
                           ->orWhere('excerpt', 'like', "%$query%")
                           ->orWhere('body', 'like', "%$query%")
                           ->paginate(10);

        $gifts = $this->getGifts(10);

        return view('theme.posts.search')->with([
                                            'posts' => $posts,
                                            'meta' => $meta,
                                            'bigBanner' => $bigBanner,
                                            'bigBnrCount' => 0,
                                            'cubeBanner' => $cubeBanner,
                                            'cubeBnrCount' => 0,
                                            'gifts' => $this->getGifts(10),
                                            'horos' => $this->getHoros()
                                            ]);
    }


    public function searchdalil(Request $request){
        $meta = embedMetas('main');

        $rules = [
            'query' => 'required|min:3',
        ];

        $customMessages = [
            'query.required' => 'من فضلك ، اكتب ما تريد البحث عنه!<br/>בבקשה, שלח את מה שאתה רוצה לחפש!',
            'query.min' => 'يجب أن يكون نص البحث 3 أحرف على الأقل.<br/>הטקסט המחפש שלך חייב לכלול לפחות 3 תווים.'
        ];

        $this->validate($request, $rules, $customMessages);

        $query = $request->input('query');

        $posts = Post::where([['title', 'like', "%$query%"],['is_active','=','1'],['status','=','PUBLISHED']])
            ->orWhere('excerpt', 'like', "%$query%")
            ->orWhere('body', 'like', "%$query%")
            ->paginate(10);

        $footer_links = Footerlink::all();
        return view('dalil.search', compact('posts' , 'footer_links' ));

    }

}
