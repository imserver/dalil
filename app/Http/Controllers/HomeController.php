<?php

namespace App\Http\Controllers;

use App\CategoriesPost;
use App\Footerlink;
use App\Page;
use App\Phonesofeducationalinstitution;
use App\Phonesofgovernmentoffice;
use App\Phonesofmunicipalitie;
use App\Phonesofpoliceandfirefighter;
use App\Phonesoftouristoffice;
use App\Poll;
use App\Post;
use App\Video;
use App\Banner;
use App\Category;
use App\BannersSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\SiteController;
use App\Workprogress;
use App\History;
use App\About;
use App\Service;
use App\Paertener;
use App\Ourteam;
use App\App;
use App\Career;
use App\Ourproject;

use DB;
use App\Quotation;



class HomeController extends SiteController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $meta, $bigBanner, $cubeBanner;

    public function index()
    {
        $about = DB::table('abouts')->orderBy('id' , 'desc')->first();
        //$categories = Category::withTranslations(['ar', 'he'])->get()->where([["is_active","=", "1"] , ["parent_id" , "'=' ","NULL"]]);
        $categories = Category::where([['is_active','=', '1'] ])->whereNull('parent_id')->get();



        $slider = BannersSlider::all();

        $directories_col2 = Phonesoftouristoffice::withTranslations(['ar', 'he'])->get();
        $directories_col4 = Phonesofpoliceandfirefighter::withTranslations(['ar', 'he'])->get();
        $directories_col1 = Phonesofmunicipalitie::withTranslations(['ar', 'he'])->get();
        $directories_col5 = Phonesofgovernmentoffice::withTranslations(['ar', 'he'])->get();
        $directories_col3 = Phonesofeducationalinstitution::withTranslations(['ar', 'he'])->get();
        $banners = Banner::where([['is_active','=','1']])->limit(2)->get();

        $pagephones = Page::all()->where('slug', 'arqam-hwatf-mhmh')->first();
        $footer_links = Footerlink::all();
        return view('dalil.home', compact('about' ,'pagephones' ,'categories' ,'slider', 'footer_links' , 'directories_col5' , 'directories_col4' , 'directories_col3' , 'directories_col2' , 'directories_col1' , 'banners'));
    }

    public function about(){
       // $about = DB::table('abouts')->orderBy('id', 'desc');
        $about = About::all();
        $about->load('translations');
        $footer_links = Footerlink::all();
        return view('dalil.aboutus', compact('about' , 'footer_links' ));
    }

    public function page($slug){
        $page = Page::all()->where('slug', $slug)->first();
        $bottom = "0";
        if($slug=='اتصل-بنا')
            $bottom = "1";

        $footer_links = Footerlink::all();
        if($bottom==1)
        return view('dalil.contact', compact('page'  , 'footer_links' ));
        else
            return view('dalil.page', compact('page'  , 'footer_links' ));
    }

    public function category($slug){
            //$pagination = 20;
            $subPosts = [];
            $meta = embedMetas('PostCategory',$slug);
            $category = Category::withTranslations(['ar', 'he'])->where([
                ['slug','=',$slug],
                ['is_active', '1']
            ])
                ->orderBy('order', 'Asc')
                ->firstOrFail();

            $categoryName = $category->name;
            $categoryDesign = $category->main_design;

            $today = date("yy-m-d");

      $posts = array();
      /*       $posts = $category->posts()
                    ->where([
                        ['posts.is_active', '1'],
                        ['posts.status','published'],
                        ['posts.author_id' , '=','3'], ['posts.end_at','>',$today],
                    ])
                    ->orderBy('posts.order', 'Asc')
                    ->paginate(9); //$category->items

*/

        //     ['posts.author_id' , '!=','3'],
    /*    $promoted_posts = $category->posts()
            ->where([
                ['posts.is_active', '1'],
                ['posts.status','published'],
                ['posts.end_at','>',$today],
            ])
            ->orderBy('posts.order', 'Asc')
            ->paginate($category->items);
*/

            $subCats = $category->subcategory()
                ->where('is_active','1')
                ->orderBy('id', 'Asc')
                ->get();


        $promoted_posts= array();

		if(count($subCats) >= 2)
        $total = 6;
		else
			$total = round(2/count($subCats));
		
            foreach ($subCats as $subcat) {
                $category1 = Category::findOrFail($subcat->id);
                $subPosts[$category1->id] = $category1->posts()
                    ->where([
                        ['posts.is_active', '1'],
                        ['posts.status', 'published'],
                        //  ['posts.author_id' , '=','null'],
                        ['posts.end_at', '>', $today],
                    ])
                    ->orderBy('posts.id', 'Asc')->get();

                $temp = $category1->posts()
                    ->where([
                        ['posts.is_active', '1'],
                        ['posts.status', 'published'],
                        //  ['posts.author_id' , '=','null'],
                        ['posts.end_at', '>', $today],
                    ])
                    ->orderBy('posts.id', 'Asc')->withTranslations(['he'])->get()->toArray();
                if ((count($temp) > 0) ) {
                    if(count($temp) < $total)
                       $randKeys = array_rand($temp, count($temp));
				   else
					   $randKeys = array_rand($temp, $total);
				
					
					if(is_array($randKeys)){ 
					if(count($randKeys) > 0){ 
                    foreach ($randKeys as $key) {
                        $item = $temp[$key];
                        if (!empty($item))
                            $promoted_posts[] = $item;
                    }
					}
					}else{
						 $item = $temp[$randKeys];
                        if (!empty($item))
                            $promoted_posts[] = $item;
					}
                }

            }




        $footer_links = Footerlink::all();
        return view('dalil.category' , compact('footer_links' , 'promoted_posts' , 'category' , 'categoryName' , 'subCats' , 'posts' , 'subPosts'));


        }

    public function details($slug){
            $meta = embedMetas('Post',$slug);
            $today = date("yy-m-d");
            $post = Post::where([
                ['slug',$slug],
                ['is_active', '1'],
                ['status','published'],
                ['posts.end_at','>',$today],
            ])->withTranslations(['ar', 'he'])->orderBy('order', 'Asc')->first();

            $additionals_images = array();

            if($post!=null){
                if(!empty($post->images)){
                    $post->images = ltrim($post->images,'["');
                    $post->images = str_replace('"','',$post->images);
                    $post->images = rtrim($post->images,']');
                    $additionals_images = explode(',', $post->images);
                }
            }
        $footer_links = Footerlink::all();
        return view('dalil.details' , compact('footer_links' , 'post' , 'additionals_images'));
        }

    public function loginscreen(){

        $footer_links = Footerlink::all();
        return view('dalil.login' , compact('footer_links'));
    }



    public function freeaddscreen(){
        $footer_links = Footerlink::all();
        $categories = Category::where([['is_active','=', "1"]])->WhereNull('parent_id')->get();


        return view('dalil.freeadd' , compact('footer_links' , 'categories'));
    }

	protected function getRelatedSlugs($slug, $id = 0)
    {
        return Post::select('slug')->where('slug', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->get();
    }

    public function freeadd(Request $request){
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'phone' => 'required|min:14:max14',
            'email' => 'required|email',
            'category' => 'required|min:0',
            'location' => 'required',
            'thumb' => 'required',
            'companydesc' => 'required',
        ];


        $customMessages = [
            'name.required' => '"Full Name" field is required.',
            'thumb.required' => '"Business min  image" field is required.',
            'companydesc.required' => '"Company Description" field is required.',
            'category.required' => '"Sub Category" field is required.',
            'email.required' => '"Email" field is required.',
            'email.email' => '"Email" field not valid!!',
            'phone.required' => '"Phone" field is required.',
            'phone.numeric' => 'The phone number entered is not valid.',
            'phone.min' => 'The phone number must be 14 digits.'
        ];

        $this->validate($request, $rules, $customMessages);

        $author_id = '3';
        $status = 'PENDING';
        $is_active = 0;
        $rating = 0;
        $end_at = date('yy-m-d', strtotime('+1 year'));

        $footer_links = Footerlink::all();
        $post = new Post();
        $post->author_id = $author_id;
        $post->title = $request->name;
        $post->slug = str_slug($request->name ,'-');
		  // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs( $post->slug, 0);

        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug',  $post->slug)){
          $post->slug = $post->slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug =  $post->slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
               $post->slug = $newSlug; break;
            }
        }

        $post->seo_title = $request->name;

        $post->body = $request->companydesc;
        $post->phone = $request->phone;
        $post->is_active = $is_active;
        $post->status = $status;
        $post->location = $request->location;
        $post->email = $request->email;
        $post->rating = $rating;
        $post->end_at = $end_at;
        $name = '';
        if($request->hasFile('thumb')) {
            $photo = $request->thumb;
            $filename = $photo->store('storage/posts/');
            $name = $photo->getClientOriginalName();
            $photo->move('storage/posts/', $name);
        }

        $post->image = 'posts/' . $name;


        $additionanimages = "";
        if($request->hasFile('photos')) {
            $additionanimages = "[";
            $photos = $request->photos;
            $index = 0;
            foreach ($photos as $photo){
                $phototemp = $photo;
                $filename = $phototemp->store('storage/posts/');
                $name = $phototemp->getClientOriginalName();
                $phototemp->move('storage/posts/', $name);
                $name = 'posts/' . $name;
                $additionanimages = $additionanimages.'"'.$name.'"';
                $index++;
                if($index< count($photos))
                    $additionanimages = $additionanimages.",";
                        elseif ($index==count($photos))
                            $additionanimages = $additionanimages."]";
            }

        }

        $post->images = $additionanimages;
        $post->save();
        $post->categories()->attach($request->category);
        $post = $post->translate('he');
        $post->title = $request->namehe;
        $post->body = $request->companydesche;
        $post->location = $request->locationhe;

        $post->save();

          session(['success' => "تم إرسال المعلومات بنجاح <br/>המידע נשלח בהצלחה."]);
        return back()->withInput($request->all());

        // with success message
      //  return back()->with('success', "تم إرسال المعلومات بنجاح <br/>המידע נשלח בהצלחה.");
    }




}
