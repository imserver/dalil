<?php

namespace App\Http\Controllers;

use App\Video;
use App\Banner;
use App\VideoCategory;
use Illuminate\Http\Request;

class VideosController extends Controller
{
    public function index(){

        $videos = $subCats = $subVideos = [];

        $meta = embedMetas('main');

        $categories = VideoCategory::where([
            ['parent_cat',NULL],
            ['is_active', '1']
        ])->get();

        foreach($categories as $cat){
            $category = VideoCategory::findOrFail($cat->id);

            $videos[$category->id] = $category->videos()
                          ->where([
                                ['videos.is_active', '1'],
                            ])
                          ->orderBy('videos.order', 'Asc')
                          ->take(4)->get();
            if(sizeof($category->subcategory())>0){
            $subCats[$category->id] = $category->subcategory()
                ->where('is_active','1')
                ->get();

            if(sizeof($subCats[$category->id])>0){

            foreach ($subCats[$category->id] as $subcat) {

                $category = VideoCategory::findOrFail($subcat->id);
                $subVideos[$category->id] = $category->videos()
                                                    ->where([
                                                        ['videos.is_active', '1'],
                                                    ])
                                                    ->orderBy('videos.order', 'Asc')
                                                    ->take(4)->get();
                }
            }
        }
        }



        $getBigBnrs = new Banner;
        $bigBanner = $getBigBnrs->getAdsByPlace('bigbnr');
        $cubeBanner = $getBigBnrs->getAdsByPlace('cubebnr');


        return view('theme.videos.index')->with([
            'categories' => $categories,
            'videos' => $videos,
            'subCats' => $subCats,
            'subVideos' => $subVideos,
            'meta' => $meta,
            'bigBanner' => $bigBanner,
            'bigBnrCount' => 0,
            'cubeBanner' => $cubeBanner,
            'cubeBnrCount' => 0
        ]);

    }

    public function showCategory($slug){

        $pagination = 20;

        $subPosts = [];

        $meta = embedMetas('VideoCategory',$slug);

        $category = VideoCategory::where([
                                        ['slug',$slug],
                                        ['is_active', '1']
                                    ])
                                    ->firstOrFail();

        $categoryName = $category->title;

        $videos = $category->videos()
                          ->where([
                                ['videos.is_active', '1'],
                            ])
                          ->orderBy('videos.order', 'Asc')
                          ->paginate($pagination);

        $getBigBnrs = new Banner;
        $bigBanner = $getBigBnrs->getAdsByPlace('bigbnr');
        $cubeBanner = $getBigBnrs->getAdsByPlace('cubebnr');

        return view('theme.videos.cat')->with([
                                    'category' => $category,
                                    'categoryName' => $categoryName,
                                    'videos' => $videos,
                                    'meta' => $meta,
                                    'bigBanner' => $bigBanner,
                                    'bigBnrCount' => 0,
                                    'cubeBanner' => $cubeBanner,
                                    'cubeBnrCount' => 0
                                ]);
    }

    public function showVideo($slug){

        $meta = embedMetas('Video',$slug);

        $video = Video::where([
                                ['slug',$slug],
                                ['is_active', '1'],
                            ])
                            ->orderBy('order', 'Asc')
                            ->firstOrFail();

        $category = $video->categories()
                        ->where('is_active','1')->firstOrFail();

        $videoUrl = $video->getVideoUrl($video->link,$video->file);

        $video['image'] = (isset($video['image']))?$video['image']:$video->get_vid_thumbnail($video['link']);

        $getBigBnrs = new Banner;
        $bigBanner = $getBigBnrs->getAdsByPlace('bigbnr');
        $cubeBanner = $getBigBnrs->getAdsByPlace('cubebnr');


        return view('theme.videos.show')->with([
            'video' => $video,
            'category' => $category,
            'videoUrl' => $videoUrl,
            'meta' => $meta,
            'bigBanner' => $bigBanner,
            'bigBnrCount' => 0,
            'cubeBanner' => $cubeBanner,
            'cubeBnrCount' => 0
        ]);
    }
}
