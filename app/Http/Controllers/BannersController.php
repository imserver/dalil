<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;

class BannersController extends Controller
{
    public function show($id){

        $meta = embedMetas('main');

        $banner = Banner::where([
                                ['id',$id],
                                ['is_active','1']
                            ])->firstOrFail();

        return view('theme.banners.index')->with([
            'banner' => $banner,
            'meta' => $meta
        ]);
    }
}
