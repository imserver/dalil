<?php

namespace App\Http\Controllers;

use App\Category;
use App\Footerlink;
use App\Post;
use App\Subscriptionrenewal;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use TCG\Voyager\Traits\Translatable;
use Auth;
use Session;

class UserController extends Controller
{
    /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web')->except('logout');
    }


    protected function guard(){
        return Auth::guard('web');
    }



    public function weblogin(Request $request)
    {


        $footer_links = Footerlink::all();
        $categories = Category::where([['is_active','=', '1'] ])->whereNull('parent_id')->get();
        $is_auth = 0;
        if ($request->session()->has('user')) {
            $is_auth = 1;
        }else{

            $this->validate($request, [
                'email'   => 'required|email',
                'password' => 'required'
            ]);
            $credentials = $request->only('email', 'password');
            if(Auth::guard('web')->attempt($credentials)){
                $is_auth = 1;
            }
        }


        if($is_auth == 1){

            $user = Auth::guard('web')->user();
            $data['name'] = $user->name;
            $data['email'] = $user->email;
			$data['phone'] = $user->phone;
            session(['user' => $user->email]);
            $today = date("yy-m-d");
            //bussiness
            $post = Post::where([
                ['author_id',$user->id],
                //['is_active', '1'],
               // ['status','published']
            ])->withTranslations(['ar', 'he'])->orderBy('order', 'Asc')->first();

            //, ['posts.end_at','>',$today],
            $additionals_images = array();
            if($post!=null) {
                $data['phone'] = $post->phone;
                $data['id'] = $user->id;
                $data['location'] = $post->location;
                $data['bussiness_email'] = $post->email;
                $data['status'] = $post->status;
                $data['currentcats'] = $post->categories()->first();

                if($post->images!=""){
                    $post->images = ltrim($post->images,'["');
                    $post->images = str_replace('"','',$post->images);
                    $post->images = rtrim($post->images,']');
                    $additionals_images = explode(',', $post->images);
                }

                if($post->is_active==0)
                $data['active'] = "Not Active";
                else
                    $data['active'] = " Active";

                if($post->end_at > $today)
                   $data['account_validation'] = 1;
                else
                    $data['account_validation'] = 0;


            }else{
               
                $data['id'] = $user->id;
                $data['location'] = "";
                $data['bussiness_email'] = "";
                $data['status'] = 'Non';
                $data['active'] = 'Non';
                $data['account_validation'] = 0;

            }

            return view('dalil.profile', compact('footer_links' , 'data' , 'post' , 'categories' , 'additionals_images') );
        }else{
            return view('dalil.login' , compact('footer_links' ));
        }
    }


    public function logout(Request $request)
    {
        $request->session()->invalidate();
        Auth::guard('web')->logout();
        $request->session()->regenerate();
        return redirect('/');
    }

    public function edit( Request $request){
        $this->validate($request, [
           // 'bussiness_email'   => 'required|email',
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'phone' => 'required|min:14|max:14',
          //  'location' => 'required'
        ]);

        $user_id = $request->id;

        $user = User::find($user_id);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->save();
		$data['phone'] = $request->phone;
        $today = date("yy-m-d");
        //bussiness
        $post = Post::where([
            ['author_id',$user_id],
            ['is_active', '1'],
            ['status','published'] , ['posts.end_at','>',$today],
        ])->orderBy('order', 'Asc')->first();

		if($post != null){
        $post = Post::find($post->id);
        $post->phone = $request->phone;
		
		if($request->bussiness_email != "")
			$post->email = $request->bussiness_email;
		
		if($request->location != "")
			$post->location = $request->location;
		
        $post->save();
		}

        return redirect()->back()->with('message', 'User Information Updated!!');
    }

	protected function getRelatedSlugs($slug, $id = 0)
    {
        return Post::select('slug')->where('slug', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->get();
    }

    public function promotedadd(Request $request){
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'phone' => 'required|min:14|max:14',
            'email' => 'required|email',
            'category' => 'required',
            'location' => 'required',
            'thumb' => 'required',
            'companydesc' => 'required',
        ];


        $customMessages = [
            'name.required' => '"Full Name" field is required.',
            'email.required' => '"Email" field is required.',
            'email.email' => '"Email" field not valid!!',
            'phone.required' => '"Phone" field is required.',
            'phone.numeric' => 'The phone number entered is not valid.',
            'phone.min' => 'The phone number must be 14 digits.'
        ];

        $this->validate($request, $rules, $customMessages);

        $author_id = $request->id;
        $status = 'PENDING';
        $is_active = 0;
        $rating = 0;
        $end_at = date('yy-m-d', strtotime('+1 year'));

        $footer_links = Footerlink::all();
        $post = new Post();
        $post->author_id = $author_id;
        $post->title = $request->name;
        $post->slug = str_slug($request->name ,'-');
		  // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs( $post->slug, 0);

        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug',  $post->slug)){
            $post->slug = $post->slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug =  $post->slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                 $post->slug = $newSlug; break;
            }
        }

        $post->seo_title = $request->name;

        $post->body = $request->companydesc;
        $post->phone = $request->phone;
        $post->is_active = $is_active;
        $post->status = $status;
        $post->location = $request->location;
        $post->email = $request->email;
        $post->rating = $rating;
        $post->end_at = $end_at;
        $name = '';
        if($request->hasFile('thumb')) {
            $photo = $request->thumb;
            $filename = $photo->store('storage/posts/');
            $name = $photo->getClientOriginalName();
            $photo->move('storage/posts/', $name);
        }

        $post->image = 'posts/' . $name;


/*

        $post = $post->translate('he');
        $post->title = $request->namehe;
        $post->body = $request->companydesche;
        $post->location = $request->locationhe;*/

        $additionanimages = "";
        if($request->hasFile('photos')) {
            $additionanimages = "[";
            $photos = $request->photos;
            $index = 0;
            foreach ($photos as $photo){
                $phototemp = $photo;
                $filename = $phototemp->store('storage/posts/');
                $name = $phototemp->getClientOriginalName();
                $phototemp->move('storage/posts/', $name);
                $name = 'posts/' . $name;
                $additionanimages = $additionanimages.'"'.$name.'"';
                $index++;
                if($index< count($photos))
                    $additionanimages = $additionanimages.",";
                elseif ($index==count($photos))
                    $additionanimages = $additionanimages."]";
            }

        }

        $post->images = $additionanimages;

        $post->save();
        $post->categories()->attach($request->category);
 


      $post = $post->translate('he');




        if($post->translate('he') != null){
           
            $post->title = $request->namehe;
            $post->body = $request->companydesche;
            $post->location = $request->locationhe;
        }else{
            $translate = new Translation();
            $translate->table_name = 'posts';
            $translate->foreign_key = $post->id;
            $translate->locale = 'he';
            $translate->column_name = 'title';
            $translate->value = $request->namehe;
            $translate->save();

            $translate = new Translation();
            $translate->table_name = 'posts';
            $translate->foreign_key = $post->id;
            $translate->locale = 'he';
            $translate->column_name = 'body';
            $translate->value = $request->companydesche;
            $translate->save();

            $translate = new Translation();
            $translate->table_name = 'posts';
            $translate->foreign_key = $post->id;
            $translate->locale = 'he';
            $translate->column_name = 'location';
            $translate->value = $request->locationhe;
            $translate->save();
      }


        // with success message
        return back()->with('success', "تم إرسال المعلومات بنجاح <br/>המידע נשלח בהצלחה.");
    }

    public function editbusiness(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'phone' => 'required|min:14|max:14',
            'email' => 'required|email',
            // 'category' => 'required',
            'location' => 'required',
            'companydesc' => 'required',
        ];


        $customMessages = [
            'name.required' => '"Full Name" field is required.',
            'email.required' => '"Email" field is required.',
            'email.email' => '"Email" field not valid!!',
            'phone.required' => '"Phone" field is required.',
            'phone.numeric' => 'The phone number entered is not valid.',
            'phone.min' => 'The phone number must be 14 digits.'
        ];

        $this->validate($request, $rules, $customMessages);

        $author_id = $request->id;
        $post_id = $request->post_id;
        $status = 'PENDING';
        $is_active = 0;
        $rating = 0;
        $end_at = date('yy-m-d', strtotime('+1 year'));

        $footer_links = Footerlink::all();
        $post = Post::find($post_id);
        $post->author_id = $author_id;
        $post->title = $request->name;
        $post->slug = str_slug($request->name, '-');
        $post->seo_title = $request->name;

        $post->body = $request->companydesc;
        $post->phone = $request->phone;
        $post->is_active = $is_active;
        $post->status = $status;
        $post->location = $request->location;
        $post->email = $request->email;
        $post->rating = $rating;
        $post->end_at = $end_at;
        $name = '';
        if ($request->hasFile('thumb')) {
            $photo = $request->thumb;
            $filename = $photo->store('storage/posts/');
            $name = $photo->getClientOriginalName();
            $photo->move('storage/posts/', $name);
            $post->image = 'posts/' . $name;
        }


        $additionanimages = "";
        if ($request->hasFile('photos')) {
            $additionanimages = "[";
            $photos = $request->photos;
            $index = 0;
            foreach ($photos as $photo) {
                $phototemp = $photo;
                $filename = $phototemp->store('storage/posts/');
                $name = $phototemp->getClientOriginalName();
                $phototemp->move('storage/posts/', $name);
                $name = 'posts/' . $name;
                $additionanimages = $additionanimages . '"' . $name . '"';
                $index++;
                if ($index < count($photos))
                    $additionanimages = $additionanimages . ",";
                elseif ($index == count($photos))
                    $additionanimages = $additionanimages . "]";
            }
        }

        $post->images = $additionanimages;
        $post->save();


        if ($request->category != ''){
            $post->categories()->attach($request->category);
        }


/*
        $post = $post->translate('he');
        $post->title = $request->namehe;
        $post->body = $request->companydesche;
        $post->location = $request->locationhe;*/

        // with success message
        return back()->with('success', "تم إرسال المعلومات بنجاح <br/>המידע נשלח בהצלחה.");
    }



    public function renewrequest(Request $request){
        $contact = new Subscriptionrenewal();
        $contact->email = $request->input('email');
        $contact->msg = $request->input('msg');
        $contact->user_id = $request->user_id;


        $contact->save();
        return back()->with('success', "تم إرسال رسالتك بنجاح <br/>ההודעה שלך נשלחה בהצלחה");
    }

}
