<?php

namespace App\Http\Controllers;

use App\Category;
use App\Gift;
use App\Poll;
use App\Post;
use Validator;
use App\Comment;
use App\PollAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;

class AjaxController extends Controller
{


    public function ajaxCommentPost(Request $request){

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'post_id' => 'required'
        ]);

        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {

            $comment = new Comment([
                'name'      =>  $request->get('name'),
                'body'      =>  $request->get('body'),
                'post_id'   =>  $request->get('post_id'),
                'is_active' => '0'
            ]);
            $comment->save();
            $success_output = '<div class="alert alert-success">تم التعليق بنجاح.بعد الفحص سوف يتم نشر التعليق.</div>';
        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    public function getsubcategories(Request $request){

        $validation = Validator::make($request->all(), [
            'maincategory' => 'required'
        ]);


        $sub_categories = array();
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }else{
            $success_output = "";
            $maincategoryid = $request->maincategory;
            $sub_categories = Category::where([['is_active','=', "1"] , ['parent_id','=', $maincategoryid]])->withTranslations(['he'])->get();
        }

        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output,
            'result'   =>  $sub_categories
        );
        echo json_encode($output);

    }
	
	
	
	public function updatesliderscategory(Request $request){
		
		$validation = Validator::make($request->all(), [
            'category_id' => 'required'
        ]);
		
		$error_array = array();
        $success_output = '';
		$promoted_posts= array();
		
		if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }else{
            $success_output = "";
            $id = $request->category_id;
			
			$category = Category::withTranslations(['he'])->where([['id','=',$id], ['is_active', '1']])->orderBy('order', 'Asc')->firstOrFail();
			$today = date("yy-m-d");
			$subCats = $category->subcategory()->where('is_active','1')->orderBy('id', 'Asc')->get();
			
			if(count($subCats) >= 2)
				$total = 6;
			else
			$total = round(2/count($subCats));
		
            foreach ($subCats as $subcat) {
                $category1 = Category::findOrFail($subcat->id);
                $subPosts[$category1->id] = $category1->posts()
                    ->where([
                        ['posts.is_active', '1'],
                        ['posts.status', 'published'],
                        ['posts.end_at', '>', $today],
                    ])
                    ->orderBy('posts.id', 'Asc')->get();

                $temp = $category1->posts()->where([['posts.is_active', '1'],['posts.status', 'published'], ['posts.end_at', '>', $today],
                    ])->orderBy('posts.id', 'Asc')->withTranslations([ 'he'])->get()->toArray();
					
                if ((count($temp) > 0) ) {
                    if(count($temp) < $total)
                       $randKeys = array_rand($temp, count($temp));
				   else
					   $randKeys = array_rand($temp, $total);
				
					
					if(is_array($randKeys)){ 
					if(count($randKeys) > 0){ 
                    foreach ($randKeys as $key) {
                        $item = $temp[$key];
                        if (!empty($item))
                            $promoted_posts[] = $item;
                    }
					}
					}else{
						 $item = $temp[$randKeys];
                        if (!empty($item))
                            $promoted_posts[] = $item;
					}
                }

            }
        }

        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output,
            'result'    =>  $promoted_posts
        );
        echo json_encode($output);
		
		

	}
	
	
    public function putrate(Request $request){

        $validation = Validator::make($request->all(), [
            'rate' => 'required',
            'post_id' => 'required'
        ]);

        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            $post = Post::find($request->post_id);
            $post->rating = $request->rate;
            $post->save();
            $success_output = '<div class="alert alert-success">شكرا لك لتقمينا :)  <br/> תודה שעשיתם זאת :)</div>';
        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output,
            'rating'   =>  $request->rate
        );
        echo json_encode($output);
    }

    public function ajaxVotePollPost(Request $request)

    {

        $validation = Validator::make($request->all(), [
            'poll_id' => 'required',
            'option' => 'required'
        ]);

        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            PollAnswer::where('id', $request->get('option'))->update(['votes' => DB::raw('votes+1')]);
            $cookie = Cookie::queue('poll_' . $request->get('poll_id'), $request->get('option'));

            $getpoll = new Poll;
            $poll = $getpoll->getActivePoll();
            $PollAnswers = $poll->answers()->get();
            foreach($PollAnswers as $option) {
                $success_output .= $option->answer . $poll->votesPercent($option->votes) .'%' . '<div class="progress">
                <div class="progress-bar progress-bar-info six-sec-ease-in-out" aria-valuetransitiongoal="'. $poll->votesPercent($option->votes) .'">'. $poll->votesPercent($option->votes).'%</div></div>

                <script type="text/javascript">
                    $(window).ready(function(e){
                        $.each($("div.progress-bar"),function(){
                            $(this).css("width", $(this).attr("aria-valuetransitiongoal")+"%");
                        });
                    });
                </script>
                ';
            }

        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    public function ajaxGiftPost(Request $request)

    {

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
            'data' => 'required'
        ]);

        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {

            $gift = new Gift([
                'name'      =>  $request->get('name'),
                'title'      =>  $request->get('title'),
                'data'   =>  $request->get('data'),
                'is_active' => '0'
            ]);
            $gift->save();
            $success_output = '<div class="alert alert-success">تم إرسال الإهداء بنجاح.بعد الفحص سوف يتم نشر التعليق.</div>';
        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }
}
