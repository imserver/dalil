<?php

namespace App;

use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;



class Post extends Model
{
    use Translatable;
    use Resizable;
    protected $translatable = ['title' ,'location' , 'body'];

    protected $table = 'posts';
    protected $fillable = [
        'author_id',
        'title',
        'seo_title',
        'excerpt',
        'body',
        'image',
        'slug',
        'meta_description',
        'meta_keywords',
        'status',
        'featured',
        'images',
        'crop_image',
        'author',
        'show_pic',
        'is_active',
        'external_link',
        'target_link',
        'phone',
        'order',
        'author_pic',
        'location',
        'rating',
        'email',
        'end_at'
        ];


    // public function categories()
    // {
    //     return $this->belongsToMany('App\Category');
    // }
    public function categories()
    {
        return $this->belongsToMany('App\Category','categories_posts','post_id','category_id');
    }
	

    public function User(){
        return $this->hasOne('App\User');

    }
    /*
    public function comments()
    {
        return $this->hasMany('App\Comment','post_id');
    }*/
}
