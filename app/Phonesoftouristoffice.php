<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Phonesoftouristoffice extends Model
{
    use Translatable;
    protected $translatable = ['full_tilte'];
    protected $table = "phonesoftouristoffice";
    protected $fillable = [
        'full_tilte',
        'phone',
        'location'
    ];
}
