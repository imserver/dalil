<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {

    Route::get('posts/publish','Voyager\PostsController@publish')->name('posts.publish');
   // Route::get('comments.posts','Voyager\CommentsController@getCommentsByArticle')->name('comments.posts');

    Route::post('change_switch', 'Voyager\VoyagerController@changeswitch');

    Voyager::routes();
});


/* Ibtisam -  web route */
Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/page/{slug}','HomeController@page')->name('page.show');
Route::get('/login', 'HomeController@loginscreen')->name('loginscreen');
Route::post('/weblogin', 'UserController@weblogin')->name('weblogin');
Route::get('/weblogin', 'UserController@weblogin')->name('weblogin');

Route::get('/profile', 'UserController@profile')->name('profile');
Route::post('/edit', 'UserController@edit')->name('edit');

Route::post('/editbusiness', 'UserController@editbusiness')->name('editbusiness');
Route::post('/newrerequest', 'UserController@renewrequest')->name('newrerequest');
Route::get('/logout', 'UserController@logout')->name('user.logout');
Route::get('/businessscreen', 'UserController@businessscren')->name('user.businessscreen');
Route::post('/business', 'UserController@promotedadd')->name('business');


Route::get('/freeaddscreen', 'HomeController@freeaddscreen')->name('freeaddscreen');
Route::post('/freeadd', 'HomeController@freeadd')->name('freeadd');


Route::get('/category/{slug}', 'HomeController@category')->name('category');
Route::get('/details/{slug}', 'HomeController@details')->name('details');
Route::post('/putrate', 'AjaxController@putrate')->name('putrate');
Route::post('/getsubcategories', 'AjaxController@getsubcategories')->name('getsubcategories');
Route::post('/updatesliderscategory', 'AjaxController@updatesliderscategory')->name('updatesliderscategory');

/* Ibtisam -  web route */

Route::post('/ajaxComment', 'AjaxController@ajaxCommentPost')->name('ajaxComment');

Route::post('/ajaxVotePoll', 'AjaxController@ajaxVotePollPost')->name('ajaxVotePoll');
Route::post('/ajaxGift', 'AjaxController@ajaxGiftPost')->name('ajaxGift');

Route::get('/post/{slug}','PostsController@showPost')->name('post.showPost');
//Route::get('/search', 'PostsController@search')->name('search');
Route::get('/search', 'PostsController@searchdalil')->name('search');
//Route::get('/category/{slug}','PostsController@showCategory')->name('post.showCategory');
// Route::get('/category/{id}','PostsController@showCategory')->name('post.showCategory');
Route::get('/addArt','PostsController@addArt')->name('post.addArt');
Route::post('/addArt','PostsController@saveArt')->name('post.saveArt');

//Route::get('/page/{slug}','PageController@show')->name('page.show');

//Route::get('/contact','ContactController@index')->name('contactscreen');
Route::post('/contact','ContactController@saveContact')->name('contact');


Route::post('/rerequest','ContactController@rerequest')->name('rerequest');

Route::get('/banners/{id}','BannersController@show')->name('banner.show');

Route::get('/video','VideosController@index')->name('video.index');
Route::get('/video/{slug}','VideosController@showVideo')->name('video.showVideo');
Route::get('/video/category/{slug}','VideosController@showCategory')->name('video.showCategory');



Route::get('crop-image-before-upload-using-croppie', 'CropImageController@index');
Route::post('crop-image-before-upload-using-croppie', ['as'=>'croppie.upload-image','uses'=>'CropImageController@uploadCropImage']);
