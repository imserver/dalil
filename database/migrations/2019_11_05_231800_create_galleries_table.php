<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('galleries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('title', 65535)->nullable();
			$table->text('image', 65535)->nullable();
			$table->text('images', 65535)->nullable();
			$table->text('sdata', 16777215)->nullable();
			$table->integer('category_id')->nullable();
			$table->boolean('is_active')->nullable();
			$table->integer('order')->nullable();
			$table->string('slug')->unique('albums_slug_unique');
			$table->boolean('watermark')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('galleries');
	}

}
