<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('services', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('sec1_thumb', 65535);
			$table->text('sec1_desc');
			$table->timestamps();
			$table->text('sec2_thumb', 65535);
			$table->text('sec2_desc');
			$table->text('sec3_thumb', 65535);
			$table->text('sec3_desc');
			$table->text('sec4_thumb', 65535);
			$table->text('sec4_desc');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('services');
	}

}
