<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for storing roles
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('author_id');
			$table->text('title', 65535);
			$table->text('seo_title', 65535)->nullable();
			$table->text('excerpt', 65535)->nullable();
			$table->text('body');
			$table->text('image', 65535)->nullable();
			$table->string('slug')->unique();
			$table->text('meta_description', 65535)->nullable();
			$table->text('meta_keywords', 65535)->nullable();
			$table->string('status');
			$table->boolean('featured')->default(0);
			$table->timestamps();
			$table->text('images', 65535)->nullable();
			$table->binary('crop_image')->nullable();
			$table->text('author', 65535)->nullable();
			$table->boolean('show_pic')->nullable();
			$table->boolean('is_active')->nullable();
			$table->boolean('watermark')->nullable();
			$table->text('external_link', 65535)->nullable();
			$table->string('target_link')->nullable();
			$table->text('file', 65535)->nullable();
			$table->integer('order')->nullable()->default(1);
			$table->integer('visits')->nullable()->default(0);
			$table->text('author_pic', 65535)->nullable();

            //$table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
