<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaertenersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Paerteners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 500);
			$table->text('thumb', 65535);
			$table->text('desc');
			$table->timestamps();
			$table->softDeletes();
			$table->text('thumbbg', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Paerteners');
	}

}
