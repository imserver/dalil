<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceivedArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('received_articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('title', 65535)->nullable();
			$table->text('name', 65535)->nullable();
			$table->text('phone', 65535)->nullable();
			$table->text('details')->nullable();
			$table->text('image', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('received_articles');
	}

}
