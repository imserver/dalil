<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAppsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('apps', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('sec1_thumb', 65535);
			$table->text('sec1_desc');
			$table->text('sec2_thumb', 65535);
			$table->text('sec2_desc');
			$table->text('fulltext');
			$table->timestamps();
			$table->softDeletes();
			$table->string('title', 1000);
			$table->text('mainthumb', 65535);
			$table->text('shortdesc', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('apps');
	}

}
