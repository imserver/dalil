<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('title', 65535)->nullable();
			$table->text('file', 65535)->nullable();
			$table->text('link', 65535)->nullable();
			$table->text('nadstream', 65535)->nullable();
			$table->text('image', 65535)->nullable();
			$table->text('crop_image')->nullable();
			$table->integer('is_active')->nullable();
			$table->timestamps();
			$table->integer('order')->nullable();
			$table->text('details')->nullable();
			$table->text('slug', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('videos');
	}

}
