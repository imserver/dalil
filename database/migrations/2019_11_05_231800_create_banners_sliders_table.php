<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBannersSlidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banners_sliders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('title', 65535)->nullable();
			$table->text('logo', 65535)->nullable();
			$table->text('crop_logo', 65535)->nullable();
			$table->text('image', 65535)->nullable();
			$table->text('crop_image', 65535)->nullable();
			$table->text('link', 65535)->nullable();
			$table->integer('is_active')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banners_sliders');
	}

}
