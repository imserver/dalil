<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use TCG\Voyager\Models\Page;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for storing roles
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('author_id');
			$table->string('title');
			$table->text('excerpt', 65535)->nullable();
			$table->text('body', 65535)->nullable();
			$table->string('image')->nullable();
			$table->string('slug')->unique();
			$table->text('meta_description', 65535)->nullable();
			$table->text('meta_keywords', 65535)->nullable();
			$table->text('status', 65535);
			$table->timestamps();
			$table->binary('crop_image')->nullable();
			$table->text('picture', 65535)->nullable();
			$table->binary('crop_picture')->nullable();
			$table->text('images', 65535)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
