<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOurteamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ourteams', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('jobtitle');
			$table->timestamps();
			$table->softDeletes();
			$table->text('thumb', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ourteams');
	}

}
