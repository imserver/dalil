<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBannersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('title', 65535)->nullable();
			$table->integer('place')->nullable();
			$table->date('fdate')->nullable();
			$table->date('tdate')->nullable();
			$table->text('image', 65535)->nullable();
			$table->text('page', 65535)->nullable();
			$table->text('link', 65535)->nullable();
			$table->text('script', 16777215)->nullable();
			$table->integer('is_active')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banners');
	}

}
