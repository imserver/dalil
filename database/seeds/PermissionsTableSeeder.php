<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_categories',
                'table_name' => 'categories',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_categories',
                'table_name' => 'categories',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_categories',
                'table_name' => 'categories',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_categories',
                'table_name' => 'categories',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_categories',
                'table_name' => 'categories',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_posts',
                'table_name' => 'posts',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_posts',
                'table_name' => 'posts',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_posts',
                'table_name' => 'posts',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_posts',
                'table_name' => 'posts',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_posts',
                'table_name' => 'posts',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'browse_pages',
                'table_name' => 'pages',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'read_pages',
                'table_name' => 'pages',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'edit_pages',
                'table_name' => 'pages',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'add_pages',
                'table_name' => 'pages',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'delete_pages',
                'table_name' => 'pages',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-08-25 14:28:00',
            ),
            41 => 
            array (
                'id' => 47,
                'key' => 'browse_category_post',
                'table_name' => 'category_post',
                'created_at' => '2019-08-25 14:39:27',
                'updated_at' => '2019-08-25 14:39:27',
            ),
            42 => 
            array (
                'id' => 48,
                'key' => 'read_category_post',
                'table_name' => 'category_post',
                'created_at' => '2019-08-25 14:39:27',
                'updated_at' => '2019-08-25 14:39:27',
            ),
            43 => 
            array (
                'id' => 49,
                'key' => 'edit_category_post',
                'table_name' => 'category_post',
                'created_at' => '2019-08-25 14:39:27',
                'updated_at' => '2019-08-25 14:39:27',
            ),
            44 => 
            array (
                'id' => 50,
                'key' => 'add_category_post',
                'table_name' => 'category_post',
                'created_at' => '2019-08-25 14:39:27',
                'updated_at' => '2019-08-25 14:39:27',
            ),
            45 => 
            array (
                'id' => 51,
                'key' => 'delete_category_post',
                'table_name' => 'category_post',
                'created_at' => '2019-08-25 14:39:27',
                'updated_at' => '2019-08-25 14:39:27',
            ),
            46 => 
            array (
                'id' => 52,
                'key' => 'browse_categories_posts',
                'table_name' => 'categories_posts',
                'created_at' => '2019-08-25 15:26:25',
                'updated_at' => '2019-08-25 15:26:25',
            ),
            47 => 
            array (
                'id' => 53,
                'key' => 'read_categories_posts',
                'table_name' => 'categories_posts',
                'created_at' => '2019-08-25 15:26:25',
                'updated_at' => '2019-08-25 15:26:25',
            ),
            48 => 
            array (
                'id' => 54,
                'key' => 'edit_categories_posts',
                'table_name' => 'categories_posts',
                'created_at' => '2019-08-25 15:26:25',
                'updated_at' => '2019-08-25 15:26:25',
            ),
            49 => 
            array (
                'id' => 55,
                'key' => 'add_categories_posts',
                'table_name' => 'categories_posts',
                'created_at' => '2019-08-25 15:26:25',
                'updated_at' => '2019-08-25 15:26:25',
            ),
            50 => 
            array (
                'id' => 56,
                'key' => 'delete_categories_posts',
                'table_name' => 'categories_posts',
                'created_at' => '2019-08-25 15:26:25',
                'updated_at' => '2019-08-25 15:26:25',
            ),
            51 => 
            array (
                'id' => 57,
                'key' => 'browse_gallery_categories',
                'table_name' => 'gallery_categories',
                'created_at' => '2019-09-10 09:13:09',
                'updated_at' => '2019-09-10 09:13:09',
            ),
            52 => 
            array (
                'id' => 58,
                'key' => 'read_gallery_categories',
                'table_name' => 'gallery_categories',
                'created_at' => '2019-09-10 09:13:09',
                'updated_at' => '2019-09-10 09:13:09',
            ),
            53 => 
            array (
                'id' => 59,
                'key' => 'edit_gallery_categories',
                'table_name' => 'gallery_categories',
                'created_at' => '2019-09-10 09:13:09',
                'updated_at' => '2019-09-10 09:13:09',
            ),
            54 => 
            array (
                'id' => 60,
                'key' => 'add_gallery_categories',
                'table_name' => 'gallery_categories',
                'created_at' => '2019-09-10 09:13:09',
                'updated_at' => '2019-09-10 09:13:09',
            ),
            55 => 
            array (
                'id' => 61,
                'key' => 'delete_gallery_categories',
                'table_name' => 'gallery_categories',
                'created_at' => '2019-09-10 09:13:09',
                'updated_at' => '2019-09-10 09:13:09',
            ),
            56 => 
            array (
                'id' => 62,
                'key' => 'browse_galleries',
                'table_name' => 'galleries',
                'created_at' => '2019-09-10 09:49:24',
                'updated_at' => '2019-09-10 09:49:24',
            ),
            57 => 
            array (
                'id' => 63,
                'key' => 'read_galleries',
                'table_name' => 'galleries',
                'created_at' => '2019-09-10 09:49:24',
                'updated_at' => '2019-09-10 09:49:24',
            ),
            58 => 
            array (
                'id' => 64,
                'key' => 'edit_galleries',
                'table_name' => 'galleries',
                'created_at' => '2019-09-10 09:49:24',
                'updated_at' => '2019-09-10 09:49:24',
            ),
            59 => 
            array (
                'id' => 65,
                'key' => 'add_galleries',
                'table_name' => 'galleries',
                'created_at' => '2019-09-10 09:49:24',
                'updated_at' => '2019-09-10 09:49:24',
            ),
            60 => 
            array (
                'id' => 66,
                'key' => 'delete_galleries',
                'table_name' => 'galleries',
                'created_at' => '2019-09-10 09:49:24',
                'updated_at' => '2019-09-10 09:49:24',
            ),
            61 => 
            array (
                'id' => 67,
                'key' => 'browse_comments',
                'table_name' => 'comments',
                'created_at' => '2019-09-30 19:20:44',
                'updated_at' => '2019-09-30 19:20:44',
            ),
            62 => 
            array (
                'id' => 68,
                'key' => 'read_comments',
                'table_name' => 'comments',
                'created_at' => '2019-09-30 19:20:44',
                'updated_at' => '2019-09-30 19:20:44',
            ),
            63 => 
            array (
                'id' => 69,
                'key' => 'edit_comments',
                'table_name' => 'comments',
                'created_at' => '2019-09-30 19:20:44',
                'updated_at' => '2019-09-30 19:20:44',
            ),
            64 => 
            array (
                'id' => 70,
                'key' => 'add_comments',
                'table_name' => 'comments',
                'created_at' => '2019-09-30 19:20:44',
                'updated_at' => '2019-09-30 19:20:44',
            ),
            65 => 
            array (
                'id' => 71,
                'key' => 'delete_comments',
                'table_name' => 'comments',
                'created_at' => '2019-09-30 19:20:44',
                'updated_at' => '2019-09-30 19:20:44',
            ),
            66 => 
            array (
                'id' => 72,
                'key' => 'browse_video_categories',
                'table_name' => 'video_categories',
                'created_at' => '2019-09-30 20:25:49',
                'updated_at' => '2019-09-30 20:25:49',
            ),
            67 => 
            array (
                'id' => 73,
                'key' => 'read_video_categories',
                'table_name' => 'video_categories',
                'created_at' => '2019-09-30 20:25:49',
                'updated_at' => '2019-09-30 20:25:49',
            ),
            68 => 
            array (
                'id' => 74,
                'key' => 'edit_video_categories',
                'table_name' => 'video_categories',
                'created_at' => '2019-09-30 20:25:49',
                'updated_at' => '2019-09-30 20:25:49',
            ),
            69 => 
            array (
                'id' => 75,
                'key' => 'add_video_categories',
                'table_name' => 'video_categories',
                'created_at' => '2019-09-30 20:25:49',
                'updated_at' => '2019-09-30 20:25:49',
            ),
            70 => 
            array (
                'id' => 76,
                'key' => 'delete_video_categories',
                'table_name' => 'video_categories',
                'created_at' => '2019-09-30 20:25:49',
                'updated_at' => '2019-09-30 20:25:49',
            ),
            71 => 
            array (
                'id' => 82,
                'key' => 'browse_received_article',
                'table_name' => 'received_article',
                'created_at' => '2019-10-15 11:26:35',
                'updated_at' => '2019-10-15 11:26:35',
            ),
            72 => 
            array (
                'id' => 83,
                'key' => 'read_received_article',
                'table_name' => 'received_article',
                'created_at' => '2019-10-15 11:26:35',
                'updated_at' => '2019-10-15 11:26:35',
            ),
            73 => 
            array (
                'id' => 84,
                'key' => 'edit_received_article',
                'table_name' => 'received_article',
                'created_at' => '2019-10-15 11:26:35',
                'updated_at' => '2019-10-15 11:26:35',
            ),
            74 => 
            array (
                'id' => 85,
                'key' => 'add_received_article',
                'table_name' => 'received_article',
                'created_at' => '2019-10-15 11:26:35',
                'updated_at' => '2019-10-15 11:26:35',
            ),
            75 => 
            array (
                'id' => 86,
                'key' => 'delete_received_article',
                'table_name' => 'received_article',
                'created_at' => '2019-10-15 11:26:35',
                'updated_at' => '2019-10-15 11:26:35',
            ),
            76 => 
            array (
                'id' => 87,
                'key' => 'browse_received_articles',
                'table_name' => 'received_articles',
                'created_at' => '2019-10-15 11:31:40',
                'updated_at' => '2019-10-15 11:31:40',
            ),
            77 => 
            array (
                'id' => 88,
                'key' => 'read_received_articles',
                'table_name' => 'received_articles',
                'created_at' => '2019-10-15 11:31:40',
                'updated_at' => '2019-10-15 11:31:40',
            ),
            78 => 
            array (
                'id' => 89,
                'key' => 'edit_received_articles',
                'table_name' => 'received_articles',
                'created_at' => '2019-10-15 11:31:40',
                'updated_at' => '2019-10-15 11:31:40',
            ),
            79 => 
            array (
                'id' => 90,
                'key' => 'add_received_articles',
                'table_name' => 'received_articles',
                'created_at' => '2019-10-15 11:31:40',
                'updated_at' => '2019-10-15 11:31:40',
            ),
            80 => 
            array (
                'id' => 91,
                'key' => 'delete_received_articles',
                'table_name' => 'received_articles',
                'created_at' => '2019-10-15 11:31:40',
                'updated_at' => '2019-10-15 11:31:40',
            ),
            81 => 
            array (
                'id' => 92,
                'key' => 'browse_banner_slider',
                'table_name' => 'banner_slider',
                'created_at' => '2019-10-17 10:40:57',
                'updated_at' => '2019-10-17 10:40:57',
            ),
            82 => 
            array (
                'id' => 93,
                'key' => 'read_banner_slider',
                'table_name' => 'banner_slider',
                'created_at' => '2019-10-17 10:40:57',
                'updated_at' => '2019-10-17 10:40:57',
            ),
            83 => 
            array (
                'id' => 94,
                'key' => 'edit_banner_slider',
                'table_name' => 'banner_slider',
                'created_at' => '2019-10-17 10:40:57',
                'updated_at' => '2019-10-17 10:40:57',
            ),
            84 => 
            array (
                'id' => 95,
                'key' => 'add_banner_slider',
                'table_name' => 'banner_slider',
                'created_at' => '2019-10-17 10:40:57',
                'updated_at' => '2019-10-17 10:40:57',
            ),
            85 => 
            array (
                'id' => 96,
                'key' => 'delete_banner_slider',
                'table_name' => 'banner_slider',
                'created_at' => '2019-10-17 10:40:57',
                'updated_at' => '2019-10-17 10:40:57',
            ),
            86 => 
            array (
                'id' => 107,
                'key' => 'browse_banners_slider',
                'table_name' => 'banners_slider',
                'created_at' => '2019-10-17 11:24:15',
                'updated_at' => '2019-10-17 11:24:15',
            ),
            87 => 
            array (
                'id' => 108,
                'key' => 'read_banners_slider',
                'table_name' => 'banners_slider',
                'created_at' => '2019-10-17 11:24:15',
                'updated_at' => '2019-10-17 11:24:15',
            ),
            88 => 
            array (
                'id' => 109,
                'key' => 'edit_banners_slider',
                'table_name' => 'banners_slider',
                'created_at' => '2019-10-17 11:24:15',
                'updated_at' => '2019-10-17 11:24:15',
            ),
            89 => 
            array (
                'id' => 110,
                'key' => 'add_banners_slider',
                'table_name' => 'banners_slider',
                'created_at' => '2019-10-17 11:24:15',
                'updated_at' => '2019-10-17 11:24:15',
            ),
            90 => 
            array (
                'id' => 111,
                'key' => 'delete_banners_slider',
                'table_name' => 'banners_slider',
                'created_at' => '2019-10-17 11:24:15',
                'updated_at' => '2019-10-17 11:24:15',
            ),
            91 => 
            array (
                'id' => 112,
                'key' => 'browse_banners_sliders',
                'table_name' => 'banners_sliders',
                'created_at' => '2019-10-17 11:26:27',
                'updated_at' => '2019-10-17 11:26:27',
            ),
            92 => 
            array (
                'id' => 113,
                'key' => 'read_banners_sliders',
                'table_name' => 'banners_sliders',
                'created_at' => '2019-10-17 11:26:27',
                'updated_at' => '2019-10-17 11:26:27',
            ),
            93 => 
            array (
                'id' => 114,
                'key' => 'edit_banners_sliders',
                'table_name' => 'banners_sliders',
                'created_at' => '2019-10-17 11:26:27',
                'updated_at' => '2019-10-17 11:26:27',
            ),
            94 => 
            array (
                'id' => 115,
                'key' => 'add_banners_sliders',
                'table_name' => 'banners_sliders',
                'created_at' => '2019-10-17 11:26:27',
                'updated_at' => '2019-10-17 11:26:27',
            ),
            95 => 
            array (
                'id' => 116,
                'key' => 'delete_banners_sliders',
                'table_name' => 'banners_sliders',
                'created_at' => '2019-10-17 11:26:27',
                'updated_at' => '2019-10-17 11:26:27',
            ),
            96 => 
            array (
                'id' => 117,
                'key' => 'browse_banners_places',
                'table_name' => 'banners_places',
                'created_at' => '2019-10-20 08:46:34',
                'updated_at' => '2019-10-20 08:46:34',
            ),
            97 => 
            array (
                'id' => 118,
                'key' => 'read_banners_places',
                'table_name' => 'banners_places',
                'created_at' => '2019-10-20 08:46:34',
                'updated_at' => '2019-10-20 08:46:34',
            ),
            98 => 
            array (
                'id' => 119,
                'key' => 'edit_banners_places',
                'table_name' => 'banners_places',
                'created_at' => '2019-10-20 08:46:34',
                'updated_at' => '2019-10-20 08:46:34',
            ),
            99 => 
            array (
                'id' => 120,
                'key' => 'add_banners_places',
                'table_name' => 'banners_places',
                'created_at' => '2019-10-20 08:46:34',
                'updated_at' => '2019-10-20 08:46:34',
            ),
            100 => 
            array (
                'id' => 121,
                'key' => 'delete_banners_places',
                'table_name' => 'banners_places',
                'created_at' => '2019-10-20 08:46:34',
                'updated_at' => '2019-10-20 08:46:34',
            ),
            101 => 
            array (
                'id' => 122,
                'key' => 'browse_banners',
                'table_name' => 'banners',
                'created_at' => '2019-10-20 08:49:59',
                'updated_at' => '2019-10-20 08:49:59',
            ),
            102 => 
            array (
                'id' => 123,
                'key' => 'read_banners',
                'table_name' => 'banners',
                'created_at' => '2019-10-20 08:49:59',
                'updated_at' => '2019-10-20 08:49:59',
            ),
            103 => 
            array (
                'id' => 124,
                'key' => 'edit_banners',
                'table_name' => 'banners',
                'created_at' => '2019-10-20 08:49:59',
                'updated_at' => '2019-10-20 08:49:59',
            ),
            104 => 
            array (
                'id' => 125,
                'key' => 'add_banners',
                'table_name' => 'banners',
                'created_at' => '2019-10-20 08:49:59',
                'updated_at' => '2019-10-20 08:49:59',
            ),
            105 => 
            array (
                'id' => 126,
                'key' => 'delete_banners',
                'table_name' => 'banners',
                'created_at' => '2019-10-20 08:49:59',
                'updated_at' => '2019-10-20 08:49:59',
            ),
            106 => 
            array (
                'id' => 127,
                'key' => 'browse_videos',
                'table_name' => 'videos',
                'created_at' => '2019-10-22 07:30:54',
                'updated_at' => '2019-10-22 07:30:54',
            ),
            107 => 
            array (
                'id' => 128,
                'key' => 'read_videos',
                'table_name' => 'videos',
                'created_at' => '2019-10-22 07:30:54',
                'updated_at' => '2019-10-22 07:30:54',
            ),
            108 => 
            array (
                'id' => 129,
                'key' => 'edit_videos',
                'table_name' => 'videos',
                'created_at' => '2019-10-22 07:30:54',
                'updated_at' => '2019-10-22 07:30:54',
            ),
            109 => 
            array (
                'id' => 130,
                'key' => 'add_videos',
                'table_name' => 'videos',
                'created_at' => '2019-10-22 07:30:54',
                'updated_at' => '2019-10-22 07:30:54',
            ),
            110 => 
            array (
                'id' => 131,
                'key' => 'delete_videos',
                'table_name' => 'videos',
                'created_at' => '2019-10-22 07:30:54',
                'updated_at' => '2019-10-22 07:30:54',
            ),
            111 => 
            array (
                'id' => 132,
                'key' => 'browse_categories_videos',
                'table_name' => 'categories_videos',
                'created_at' => '2019-10-22 07:31:54',
                'updated_at' => '2019-10-22 07:31:54',
            ),
            112 => 
            array (
                'id' => 133,
                'key' => 'read_categories_videos',
                'table_name' => 'categories_videos',
                'created_at' => '2019-10-22 07:31:54',
                'updated_at' => '2019-10-22 07:31:54',
            ),
            113 => 
            array (
                'id' => 134,
                'key' => 'edit_categories_videos',
                'table_name' => 'categories_videos',
                'created_at' => '2019-10-22 07:31:54',
                'updated_at' => '2019-10-22 07:31:54',
            ),
            114 => 
            array (
                'id' => 135,
                'key' => 'add_categories_videos',
                'table_name' => 'categories_videos',
                'created_at' => '2019-10-22 07:31:54',
                'updated_at' => '2019-10-22 07:31:54',
            ),
            115 => 
            array (
                'id' => 136,
                'key' => 'delete_categories_videos',
                'table_name' => 'categories_videos',
                'created_at' => '2019-10-22 07:31:54',
                'updated_at' => '2019-10-22 07:31:54',
            ),
            116 => 
            array (
                'id' => 137,
                'key' => 'browse_gifts',
                'table_name' => 'gifts',
                'created_at' => '2019-11-03 11:12:26',
                'updated_at' => '2019-11-03 11:12:26',
            ),
            117 => 
            array (
                'id' => 138,
                'key' => 'read_gifts',
                'table_name' => 'gifts',
                'created_at' => '2019-11-03 11:12:26',
                'updated_at' => '2019-11-03 11:12:26',
            ),
            118 => 
            array (
                'id' => 139,
                'key' => 'edit_gifts',
                'table_name' => 'gifts',
                'created_at' => '2019-11-03 11:12:26',
                'updated_at' => '2019-11-03 11:12:26',
            ),
            119 => 
            array (
                'id' => 140,
                'key' => 'add_gifts',
                'table_name' => 'gifts',
                'created_at' => '2019-11-03 11:12:26',
                'updated_at' => '2019-11-03 11:12:26',
            ),
            120 => 
            array (
                'id' => 141,
                'key' => 'delete_gifts',
                'table_name' => 'gifts',
                'created_at' => '2019-11-03 11:12:26',
                'updated_at' => '2019-11-03 11:12:26',
            ),
            121 => 
            array (
                'id' => 142,
                'key' => 'browse_polls',
                'table_name' => 'polls',
                'created_at' => '2019-11-03 11:26:45',
                'updated_at' => '2019-11-03 11:26:45',
            ),
            122 => 
            array (
                'id' => 143,
                'key' => 'read_polls',
                'table_name' => 'polls',
                'created_at' => '2019-11-03 11:26:45',
                'updated_at' => '2019-11-03 11:26:45',
            ),
            123 => 
            array (
                'id' => 144,
                'key' => 'edit_polls',
                'table_name' => 'polls',
                'created_at' => '2019-11-03 11:26:45',
                'updated_at' => '2019-11-03 11:26:45',
            ),
            124 => 
            array (
                'id' => 145,
                'key' => 'add_polls',
                'table_name' => 'polls',
                'created_at' => '2019-11-03 11:26:45',
                'updated_at' => '2019-11-03 11:26:45',
            ),
            125 => 
            array (
                'id' => 146,
                'key' => 'delete_polls',
                'table_name' => 'polls',
                'created_at' => '2019-11-03 11:26:45',
                'updated_at' => '2019-11-03 11:26:45',
            ),
            126 => 
            array (
                'id' => 147,
                'key' => 'browse_poll_answers',
                'table_name' => 'poll_answers',
                'created_at' => '2019-11-03 11:28:58',
                'updated_at' => '2019-11-03 11:28:58',
            ),
            127 => 
            array (
                'id' => 148,
                'key' => 'read_poll_answers',
                'table_name' => 'poll_answers',
                'created_at' => '2019-11-03 11:28:58',
                'updated_at' => '2019-11-03 11:28:58',
            ),
            128 => 
            array (
                'id' => 149,
                'key' => 'edit_poll_answers',
                'table_name' => 'poll_answers',
                'created_at' => '2019-11-03 11:28:58',
                'updated_at' => '2019-11-03 11:28:58',
            ),
            129 => 
            array (
                'id' => 150,
                'key' => 'add_poll_answers',
                'table_name' => 'poll_answers',
                'created_at' => '2019-11-03 11:28:58',
                'updated_at' => '2019-11-03 11:28:58',
            ),
            130 => 
            array (
                'id' => 151,
                'key' => 'delete_poll_answers',
                'table_name' => 'poll_answers',
                'created_at' => '2019-11-03 11:28:58',
                'updated_at' => '2019-11-03 11:28:58',
            ),
            131 => 
            array (
                'id' => 152,
                'key' => 'browse_contact',
                'table_name' => 'contact',
                'created_at' => '2019-11-03 11:39:28',
                'updated_at' => '2019-11-03 11:39:28',
            ),
            132 => 
            array (
                'id' => 153,
                'key' => 'read_contact',
                'table_name' => 'contact',
                'created_at' => '2019-11-03 11:39:28',
                'updated_at' => '2019-11-03 11:39:28',
            ),
            133 => 
            array (
                'id' => 154,
                'key' => 'edit_contact',
                'table_name' => 'contact',
                'created_at' => '2019-11-03 11:39:28',
                'updated_at' => '2019-11-03 11:39:28',
            ),
            134 => 
            array (
                'id' => 155,
                'key' => 'add_contact',
                'table_name' => 'contact',
                'created_at' => '2019-11-03 11:39:28',
                'updated_at' => '2019-11-03 11:39:28',
            ),
            135 => 
            array (
                'id' => 156,
                'key' => 'delete_contact',
                'table_name' => 'contact',
                'created_at' => '2019-11-03 11:39:28',
                'updated_at' => '2019-11-03 11:39:28',
            ),
        ));
        
        
    }
}