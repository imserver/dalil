<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('videos')->delete();
        
        \DB::table('videos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'د. دالية فضيلي رئيسة كلية القاسمي للهندسة والعلوم تتحدث عن التعليم والاقلية العربية في قناة الجزيرة',
                'file' => '[]',
                'link' => 'https://www.youtube.com/watch?v=0aVGHKNAs3o',
                'nadstream' => NULL,
                'image' => NULL,
                'crop_image' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-22 21:01:00',
                'updated_at' => '2019-11-03 12:24:15',
                'order' => 1,
                'details' => NULL,
                'slug' => 'd-dalyh-fdhyly-reysh-klyh-alqasmy-llhndsh-walalwm-tthdth-an-altalym-walaqlyh-alarbyh-fy-qnah-aljzyrh',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'أغرب طائفة دينية عرفها التاريخ الإسلامي!',
                'file' => '[]',
                'link' => 'https://www.facebook.com/www.bladna.co.il/videos/vb.349160865272789/411884736431893/?type=2&theater',
                'nadstream' => NULL,
                'image' => NULL,
                'crop_image' => NULL,
                'is_active' => 1,
                'created_at' => '2019-11-03 12:19:00',
                'updated_at' => '2019-11-03 12:50:15',
                'order' => 1,
                'details' => NULL,
                'slug' => 'aghrb-taefh-dynyh-arfha-altarykh-alislamy',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'test file upload video',
                'file' => '[{"download_link":"videos\\\\November2019\\\\DZZnOpjTwQXcm48fhlE1.mp4","original_name":"SampleVideo_1280x720_1mb.mp4"}]',
                'link' => NULL,
                'nadstream' => NULL,
                'image' => 'videos\\November2019\\ubIVq8H4ihj3a6wSNcVw.png',
                'crop_image' => NULL,
                'is_active' => 1,
                'created_at' => '2019-11-03 12:32:50',
                'updated_at' => '2019-11-03 12:32:50',
                'order' => NULL,
                'details' => NULL,
                'slug' => 'test-file-upload-video',
            ),
        ));
        
        
    }
}