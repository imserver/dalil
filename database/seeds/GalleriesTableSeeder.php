<?php

use Illuminate\Database\Seeder;

class GalleriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('galleries')->delete();
        
        \DB::table('galleries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'album 1',
                'image' => 'galleries\\September2019\\jsA5tkb7brVjB8C0SBxB.jpg',
                'images' => '["galleries\\\\September2019\\\\O2RD9WgC5XcqnPX6UN7W.jpg","galleries\\\\September2019\\\\fCrdK7QDGkY7yP3zSS53.jpg","galleries\\\\September2019\\\\sxCps6zAkf4WnlZFDEiv.jpg"]',
                'sdata' => NULL,
                'category_id' => 2,
                'is_active' => 1,
                'order' => NULL,
                'slug' => 'album-1',
                'watermark' => 1,
                'created_at' => '2019-09-10 09:56:00',
                'updated_at' => '2019-10-02 11:22:13',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'jhjjjjjjjjjjjjjjjj',
                'image' => NULL,
                'images' => NULL,
                'sdata' => NULL,
                'category_id' => NULL,
                'is_active' => 1,
                'order' => NULL,
                'slug' => 'jhjjjjjjjjjjjjjjjj',
                'watermark' => 1,
                'created_at' => '2019-09-22 21:31:30',
                'updated_at' => '2019-09-22 21:31:30',
            ),
        ));
        
        
    }
}