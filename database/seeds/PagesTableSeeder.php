<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'author_id' => 1,
                'title' => 'من نحن',
                'excerpt' => 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.',
                'body' => '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>
<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>',
                'image' => 'pages/page1.jpg',
                'slug' => 'من-نحن',
                'meta_description' => 'Yar Meta Description',
                'meta_keywords' => 'Keyword1, Keyword2',
                'status' => 'ACTIVE',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-11-03 12:07:29',
                'crop_image' => NULL,
                'picture' => NULL,
                'crop_picture' => NULL,
                'images' => '[]',
            ),
            1 => 
            array (
                'id' => 2,
                'author_id' => 1,
                'title' => 'للإعلان في بلدنا',
                'excerpt' => 'gfsd',
                'body' => '<p>sfgdg</p>',
                'image' => NULL,
                'slug' => 'للإعلان-في-بلدنا',
                'meta_description' => 'fdg',
                'meta_keywords' => 'fdg',
                'status' => 'ACTIVE',
                'created_at' => '2019-09-04 07:27:12',
                'updated_at' => '2019-11-03 12:07:50',
                'crop_image' => NULL,
                'picture' => NULL,
                'crop_picture' => NULL,
                'images' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'author_id' => 1,
                'title' => 'شروط الإستخدام',
                'excerpt' => 'hgfh',
                'body' => '',
                'image' => 'pages\\September2019\\2jPqETwnPz3UpuunEJg7.jpg',
                'slug' => 'شروط-الاستخدام',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2019-09-06 13:26:55',
                'updated_at' => '2019-11-03 12:08:24',
                'crop_image' => NULL,
                'picture' => NULL,
                'crop_picture' => NULL,
                'images' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'author_id' => 1,
                'title' => 'أرسل للإدارة',
                'excerpt' => NULL,
                'body' => '',
                'image' => NULL,
                'slug' => 'أرسل-للإدارة',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2019-09-08 04:45:50',
                'updated_at' => '2019-11-03 12:08:59',
                'crop_image' => NULL,
                'picture' => NULL,
                'crop_picture' => NULL,
                'images' => NULL,
            ),
        ));
        
        
    }
}