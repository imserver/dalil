<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banners')->delete();
        
        \DB::table('banners')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'كبير',
                'place' => 2,
                'fdate' => '2019-10-19',
                'tdate' => '2019-12-31',
                'image' => 'banners\\October2019\\Zjir8Q5lZP8QAeU6btmw.jpg',
                'page' => 'banners\\October2019\\XtvSQPR40zlkc76vvN3h.jpg',
                'link' => 'http://nadsoft.net/',
                'script' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-20 09:06:41',
                'updated_at' => '2019-10-20 09:06:41',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'مكعب',
                'place' => 1,
                'fdate' => '2019-10-20',
                'tdate' => '2019-12-27',
                'image' => 'banners\\October2019\\cdoVHrbsdBhdIvETAUJK.jpg',
                'page' => NULL,
                'link' => NULL,
                'script' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-20 09:07:22',
                'updated_at' => '2019-10-20 09:07:22',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'متوسط',
                'place' => 3,
                'fdate' => '2019-10-20',
                'tdate' => '2019-12-28',
                'image' => 'banners\\October2019\\CtEuTym8IeuhxUNXfxYa.png',
                'page' => NULL,
                'link' => NULL,
                'script' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-20 09:07:00',
                'updated_at' => '2019-10-20 20:07:53',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'script',
                'place' => 1,
                'fdate' => '2019-10-20',
                'tdate' => '2019-12-27',
                'image' => 'banners\\October2019\\wjmgcZVhNwh5hnufpRAN.jpg',
                'page' => NULL,
                'link' => NULL,
                'script' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-20 09:25:00',
                'updated_at' => '2019-10-20 20:11:32',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'كبير 2',
                'place' => 2,
                'fdate' => '2019-10-14',
                'tdate' => '2019-12-27',
                'image' => 'banners\\October2019\\lydX8BAmoB3Ybg7iJQ7w.png',
                'page' => 'banners\\October2019\\uTUSVsKB3h3VRrkDcEa7.jpg',
                'link' => NULL,
                'script' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-20 11:12:00',
                'updated_at' => '2019-10-20 22:47:43',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'مكعب 2',
                'place' => 1,
                'fdate' => '2019-10-20',
                'tdate' => '2020-01-03',
                'image' => 'banners\\October2019\\3g2FeoWkn7zbdE1Gqkme.jpg',
                'page' => 'banners\\October2019\\QCBilCm61syzJ4nE2wML.jpg',
                'link' => NULL,
                'script' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-20 20:08:00',
                'updated_at' => '2019-10-20 20:10:25',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'متوسط',
                'place' => 3,
                'fdate' => '2019-10-20',
                'tdate' => '2019-12-27',
                'image' => 'banners\\October2019\\MA6qqzyTJnXOE5eVmgZq.png',
                'page' => NULL,
                'link' => NULL,
                'script' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-20 21:03:00',
                'updated_at' => '2019-10-20 22:57:19',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'طولي',
                'place' => 4,
                'fdate' => '2019-10-27',
                'tdate' => '2020-01-04',
                'image' => 'banners\\November2019\\1R5WKWF4sJZYMRHzIuuo.jpg',
                'page' => NULL,
                'link' => NULL,
                'script' => NULL,
                'is_active' => 1,
                'created_at' => '2019-11-03 13:01:07',
                'updated_at' => '2019-11-03 13:01:07',
            ),
        ));
        
        
    }
}