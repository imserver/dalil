<?php

use Illuminate\Database\Seeder;

class ContactTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contact')->delete();
        
        \DB::table('contact')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ibtihal Shihab',
                'phone' => '0599378020',
                'email' => 'ibtihal@nadsoft.net',
                'created_at' => '2019-11-03 12:15:39',
                'updated_at' => '2019-11-03 12:15:39',
                'msg' => 'test',
            ),
        ));
        
        
    }
}