<?php

use Illuminate\Database\Seeder;

class CategoriesPostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories_posts')->delete();
        
        \DB::table('categories_posts')->insert(array (
            0 => 
            array (
                'id' => 75,
                'post_id' => 9,
                'category_id' => 1,
                'created_at' => '2019-08-27 18:34:22',
                'updated_at' => '2019-08-27 18:34:22',
            ),
            1 => 
            array (
                'id' => 76,
                'post_id' => 9,
                'category_id' => 4,
                'created_at' => '2019-08-27 18:34:22',
                'updated_at' => '2019-08-27 18:34:22',
            ),
            2 => 
            array (
                'id' => 83,
                'post_id' => 6,
                'category_id' => 6,
                'created_at' => '2019-08-27 21:31:25',
                'updated_at' => '2019-08-27 21:31:25',
            ),
            3 => 
            array (
                'id' => 84,
                'post_id' => 6,
                'category_id' => 3,
                'created_at' => '2019-08-27 21:31:25',
                'updated_at' => '2019-08-27 21:31:25',
            ),
            4 => 
            array (
                'id' => 85,
                'post_id' => 6,
                'category_id' => 4,
                'created_at' => '2019-08-27 21:31:25',
                'updated_at' => '2019-08-27 21:31:25',
            ),
            5 => 
            array (
                'id' => 87,
                'post_id' => 10,
                'category_id' => 1,
                'created_at' => '2019-09-01 08:14:16',
                'updated_at' => '2019-09-01 08:14:16',
            ),
            6 => 
            array (
                'id' => 89,
                'post_id' => 13,
                'category_id' => 9,
                'created_at' => '2019-09-01 13:06:31',
                'updated_at' => '2019-09-01 13:06:31',
            ),
            7 => 
            array (
                'id' => 90,
                'post_id' => 52,
                'category_id' => 1,
                'created_at' => '2019-09-08 05:25:26',
                'updated_at' => '2019-09-08 05:25:26',
            ),
            8 => 
            array (
                'id' => 91,
                'post_id' => 52,
                'category_id' => 4,
                'created_at' => '2019-09-08 05:25:26',
                'updated_at' => '2019-09-08 05:25:26',
            ),
            9 => 
            array (
                'id' => 99,
                'post_id' => 34,
                'category_id' => 2,
                'created_at' => '2019-10-08 12:29:39',
                'updated_at' => '2019-10-08 12:29:39',
            ),
            10 => 
            array (
                'id' => 100,
                'post_id' => 34,
                'category_id' => 4,
                'created_at' => '2019-10-08 12:29:39',
                'updated_at' => '2019-10-08 12:29:39',
            ),
            11 => 
            array (
                'id' => 117,
                'post_id' => 5,
                'category_id' => 2,
                'created_at' => '2019-10-08 21:14:47',
                'updated_at' => '2019-10-08 21:14:47',
            ),
            12 => 
            array (
                'id' => 118,
                'post_id' => 5,
                'category_id' => 5,
                'created_at' => '2019-10-08 21:14:47',
                'updated_at' => '2019-10-08 21:14:47',
            ),
            13 => 
            array (
                'id' => 119,
                'post_id' => 12,
                'category_id' => 2,
                'created_at' => '2019-10-09 08:20:40',
                'updated_at' => '2019-10-09 08:20:40',
            ),
            14 => 
            array (
                'id' => 120,
                'post_id' => 12,
                'category_id' => 4,
                'created_at' => '2019-10-09 08:20:40',
                'updated_at' => '2019-10-09 08:20:40',
            ),
            15 => 
            array (
                'id' => 121,
                'post_id' => 12,
                'category_id' => 5,
                'created_at' => '2019-10-09 08:20:40',
                'updated_at' => '2019-10-09 08:20:40',
            ),
            16 => 
            array (
                'id' => 147,
                'post_id' => 4,
                'category_id' => 1,
                'created_at' => '2019-10-15 01:46:28',
                'updated_at' => '2019-10-15 01:46:28',
            ),
            17 => 
            array (
                'id' => 148,
                'post_id' => 4,
                'category_id' => 2,
                'created_at' => '2019-10-15 01:46:28',
                'updated_at' => '2019-10-15 01:46:28',
            ),
            18 => 
            array (
                'id' => 149,
                'post_id' => 4,
                'category_id' => 4,
                'created_at' => '2019-10-15 01:46:28',
                'updated_at' => '2019-10-15 01:46:28',
            ),
            19 => 
            array (
                'id' => 156,
                'post_id' => 3,
                'category_id' => 3,
                'created_at' => '2019-10-15 02:29:59',
                'updated_at' => '2019-10-15 02:29:59',
            ),
            20 => 
            array (
                'id' => 157,
                'post_id' => 3,
                'category_id' => 6,
                'created_at' => '2019-10-15 02:29:59',
                'updated_at' => '2019-10-15 02:29:59',
            ),
            21 => 
            array (
                'id' => 163,
                'post_id' => 49,
                'category_id' => 4,
                'created_at' => '2019-10-15 10:41:13',
                'updated_at' => '2019-10-15 10:41:13',
            ),
            22 => 
            array (
                'id' => 164,
                'post_id' => 49,
                'category_id' => 5,
                'created_at' => '2019-10-15 10:41:13',
                'updated_at' => '2019-10-15 10:41:13',
            ),
            23 => 
            array (
                'id' => 165,
                'post_id' => 58,
                'category_id' => 1,
                'created_at' => '2019-10-15 10:41:31',
                'updated_at' => '2019-10-15 10:41:31',
            ),
            24 => 
            array (
                'id' => 166,
                'post_id' => 58,
                'category_id' => 2,
                'created_at' => '2019-10-15 10:41:31',
                'updated_at' => '2019-10-15 10:41:31',
            ),
            25 => 
            array (
                'id' => 167,
                'post_id' => 58,
                'category_id' => 3,
                'created_at' => '2019-10-15 10:41:31',
                'updated_at' => '2019-10-15 10:41:31',
            ),
            26 => 
            array (
                'id' => 168,
                'post_id' => 58,
                'category_id' => 4,
                'created_at' => '2019-10-15 10:41:31',
                'updated_at' => '2019-10-15 10:41:31',
            ),
            27 => 
            array (
                'id' => 169,
                'post_id' => 2,
                'category_id' => 1,
                'created_at' => '2019-10-15 10:42:24',
                'updated_at' => '2019-10-15 10:42:24',
            ),
            28 => 
            array (
                'id' => 170,
                'post_id' => 2,
                'category_id' => 2,
                'created_at' => '2019-10-15 10:42:24',
                'updated_at' => '2019-10-15 10:42:24',
            ),
            29 => 
            array (
                'id' => 171,
                'post_id' => 2,
                'category_id' => 4,
                'created_at' => '2019-10-15 10:42:24',
                'updated_at' => '2019-10-15 10:42:24',
            ),
            30 => 
            array (
                'id' => 178,
                'post_id' => 7,
                'category_id' => 3,
                'created_at' => '2019-10-21 16:03:21',
                'updated_at' => '2019-10-21 16:03:21',
            ),
            31 => 
            array (
                'id' => 179,
                'post_id' => 7,
                'category_id' => 7,
                'created_at' => '2019-10-21 16:03:21',
                'updated_at' => '2019-10-21 16:03:21',
            ),
            32 => 
            array (
                'id' => 180,
                'post_id' => 60,
                'category_id' => 2,
                'created_at' => '2019-10-21 16:03:36',
                'updated_at' => '2019-10-21 16:03:36',
            ),
            33 => 
            array (
                'id' => 181,
                'post_id' => 60,
                'category_id' => 6,
                'created_at' => '2019-10-21 16:03:36',
                'updated_at' => '2019-10-21 16:03:36',
            ),
            34 => 
            array (
                'id' => 182,
                'post_id' => 60,
                'category_id' => 7,
                'created_at' => '2019-10-21 16:03:36',
                'updated_at' => '2019-10-21 16:03:36',
            ),
            35 => 
            array (
                'id' => 183,
                'post_id' => 56,
                'category_id' => 1,
                'created_at' => '2019-10-21 16:03:46',
                'updated_at' => '2019-10-21 16:03:46',
            ),
            36 => 
            array (
                'id' => 184,
                'post_id' => 56,
                'category_id' => 4,
                'created_at' => '2019-10-21 16:03:46',
                'updated_at' => '2019-10-21 16:03:46',
            ),
            37 => 
            array (
                'id' => 185,
                'post_id' => 56,
                'category_id' => 8,
                'created_at' => '2019-10-21 16:03:46',
                'updated_at' => '2019-10-21 16:03:46',
            ),
            38 => 
            array (
                'id' => 186,
                'post_id' => 55,
                'category_id' => 2,
                'created_at' => '2019-10-21 16:04:51',
                'updated_at' => '2019-10-21 16:04:51',
            ),
            39 => 
            array (
                'id' => 187,
                'post_id' => 55,
                'category_id' => 6,
                'created_at' => '2019-10-21 16:04:51',
                'updated_at' => '2019-10-21 16:04:51',
            ),
            40 => 
            array (
                'id' => 188,
                'post_id' => 55,
                'category_id' => 7,
                'created_at' => '2019-10-21 16:04:51',
                'updated_at' => '2019-10-21 16:04:51',
            ),
            41 => 
            array (
                'id' => 189,
                'post_id' => 1,
                'category_id' => 2,
                'created_at' => '2019-11-03 22:48:27',
                'updated_at' => '2019-11-03 22:48:27',
            ),
            42 => 
            array (
                'id' => 190,
                'post_id' => 1,
                'category_id' => 4,
                'created_at' => '2019-11-03 22:48:27',
                'updated_at' => '2019-11-03 22:48:27',
            ),
            43 => 
            array (
                'id' => 191,
                'post_id' => 1,
                'category_id' => 5,
                'created_at' => '2019-11-03 22:48:27',
                'updated_at' => '2019-11-03 22:48:27',
            ),
            44 => 
            array (
                'id' => 193,
                'post_id' => 61,
                'category_id' => 2,
                'created_at' => '2019-11-03 23:25:06',
                'updated_at' => '2019-11-03 23:25:06',
            ),
        ));
        
        
    }
}