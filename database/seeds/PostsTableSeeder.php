<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('posts')->delete();

        \DB::table('posts')->insert(array (
            0 =>
            array (
                'id' => 1,
                'author_id' => 1,
                'title' => 'مقال باللغة العربية',
                'seo_title' => 'Lorem Ipsum Post',
                'excerpt' => 'This is the excerpt for the Lorem Ipsum Post',
                'body' => '<p>This is the body of the lorem ipsum post</p>
<p>&nbsp;</p>
<figure class="image"><img title="test image title" src="http://localhost:8000/storage/posts/October2019/516201708231245214521.jpg" alt="test image description" width="100%" />
<figcaption>test the caption</figcaption>
</figure>
<p>&nbsp;</p>
<p>Thes images uploaded on Images Folder</p>',
                'image' => 'posts/post1.jpg',
                'slug' => 'mqal-ballghh-alarbyh',
                'meta_description' => 'This is the meta description',
                'meta_keywords' => 'keyword1, keyword2, keyword3',
                'status' => 'PUBLISHED',
                'featured' => 1,
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-11-04 09:51:35',
                'images' => '["posts/September2019/a7TrINHEV0Qwdx6ZyHYD.jpg","posts/September2019/FfmpEPc9BGm9MPHDBetO.jpg","posts/September2019/hEi7zOAtIlfkT0753jKx.jpg"]',
                'crop_image' => NULL,
                'author' => NULL,
                'show_pic' => 1,
                'is_active' => 1,
                'watermark' => 1,
                'external_link' => NULL,
                'target_link' => 'BLANK',
                'file' => '[]',
                'order' => 2,
                'visits' => 41,
                'author_pic' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'author_id' => 1,
                'title' => 'My Sample Post',
                'seo_title' => '',
                'excerpt' => 'This is the excerpt for the sample Post',
                'body' => '<p>This is the body for the sample post, which includes the body.</p>
<h2>We can use all kinds of format!</h2>
<p>And include a bunch of other stuff.</p>',
                'image' => 'posts/post2.jpg',
                'slug' => 'my-sample-post',
                'meta_description' => 'Meta Description for sample post',
                'meta_keywords' => 'keyword1, keyword2, keyword3',
                'status' => 'PUBLISHED',
                'featured' => 1,
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-10-20 22:52:53',
                'images' => '[]',
                'crop_image' => NULL,
                'author' => NULL,
                'show_pic' => 0,
                'is_active' => 1,
                'watermark' => 1,
                'external_link' => NULL,
                'target_link' => 'BLANK',
                'file' => '[]',
                'order' => 6,
                'visits' => 5,
                'author_pic' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'author_id' => 1,
                'title' => 'Latest Post',
                'seo_title' => '',
                'excerpt' => 'This is the excerpt for the latest post',
                'body' => '<p>This is the body for the latest post</p>',
                'image' => 'posts/post3.jpg',
                'slug' => 'latest-post',
                'meta_description' => 'This is the meta description',
                'meta_keywords' => 'keyword1, keyword2, keyword3',
                'status' => 'PUBLISHED',
                'featured' => 1,
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-10-10 12:59:00',
                'images' => '[]',
                'crop_image' => NULL,
                'author' => NULL,
                'show_pic' => 1,
                'is_active' => 1,
                'watermark' => 0,
                'external_link' => NULL,
                'target_link' => 'BLANK',
                'file' => '[]',
                'order' => 10,
                'visits' => 0,
                'author_pic' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'author_id' => 1,
                'title' => 'Yarr Post',
                'seo_title' => '',
                'excerpt' => 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.',
                'body' => '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>
<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>
<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>',
                'image' => 'posts/post4.jpg',
                'slug' => 'yarr-post',
                'meta_description' => 'this be a meta descript',
                'meta_keywords' => 'keyword1, keyword2, keyword3',
                'status' => 'PUBLISHED',
                'featured' => 1,
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-10-10 12:59:00',
                'images' => '[]',
                'crop_image' => NULL,
                'author' => 'CNN',
                'show_pic' => 0,
                'is_active' => 1,
                'watermark' => 0,
                'external_link' => NULL,
                'target_link' => 'BLANK',
                'file' => '[]',
                'order' => 9,
                'visits' => 0,
                'author_pic' => NULL,
            ),
            4 =>
            array (
                'id' => 61,
                'author_id' => 1,
                'title' => 'عنوان المقال عربي',
                'seo_title' => '',
                'excerpt' => '',
                'body' => '<p>gfdgfdgfdg</p>',
                'image' => NULL,
                'slug' => 'anwan-almqal-arby',
                'meta_description' => '',
                'meta_keywords' => '',
                'status' => 'PUBLISHED',
                'featured' => 0,
                'created_at' => '2019-11-03 22:53:03',
                'updated_at' => '2019-11-03 23:45:58',
                'images' => '[]',
                'crop_image' => NULL,
                'author' => NULL,
                'show_pic' => 0,
                'is_active' => 1,
                'watermark' => 0,
                'external_link' => NULL,
                'target_link' => 'BLANK',
                'file' => '[]',
                'order' => 1,
                'visits' => 35,
                'author_pic' => NULL,
            ),
        ));


    }
}
