<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(DataTypesTableSeeder::class);
        $this->call(DataRowsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(MenuItemsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);

        // $this->call(BannersTableSeeder::class);
        // $this->call(BannersPlacesTableSeeder::class);
        // $this->call(BannersSlidersTableSeeder::class);

      // $this->call(CategoriesPostsTableSeeder::class);
    //    $this->call(CategoriesVideosTableSeeder::class);
    //    $this->call(CommentsTableSeeder::class);
    //    $this->call(ContactTableSeeder::class);
    //    $this->call(GalleriesTableSeeder::class);
    //    $this->call(GalleryCategoriesTableSeeder::class);
    //    $this->call(GiftsTableSeeder::class);

        // $this->call(PasswordResetsTableSeeder::class);


        // $this->call(PollAnswersTableSeeder::class);
    //    $this->call(PollsTableSeeder::class);

    //    $this->call(ReceivedArticlesTableSeeder::class);



        // $this->call(UserRolesTableSeeder::class);
    //    $this->call(VideoCategoriesTableSeeder::class);
    //    $this->call(VideosTableSeeder::class);
    }
}
