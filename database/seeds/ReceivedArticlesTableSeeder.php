<?php

use Illuminate\Database\Seeder;

class ReceivedArticlesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('received_articles')->delete();
        
        \DB::table('received_articles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'test',
                'name' => 'اتبببببببببببببببب',
                'phone' => '545435',
                'details' => 'لبالبا
باللبيالب بلي ليبلب',
                'image' => 'public/uploads/kTa2XtMc5mXTZ4g7AEZkA8YkvvWRfpsv7BhT73vv.jpeg',
                'created_at' => '2019-10-15 12:56:05',
                'updated_at' => '2019-10-15 12:56:05',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'test',
                'name' => 'اتبببببببببببببببب',
                'phone' => NULL,
                'details' => 'لبالبا
باللبيالب بلي ليبلب',
                'image' => NULL,
                'created_at' => '2019-10-15 13:02:32',
                'updated_at' => '2019-10-15 13:02:32',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'hgfhgf',
                'name' => 'gfhdhgfh',
                'phone' => '5646546546',
                'details' => 'gh fg hfghgvb dfds',
                'image' => '/storage/uploads/gfhdhgfh_1571145710.jpg',
                'created_at' => '2019-10-15 13:21:50',
                'updated_at' => '2019-10-15 13:21:50',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'طقم بناتي قطعتين',
                'name' => 'fsdgfdg',
                'phone' => '435435',
                'details' => 'gfds',
                'image' => '/storage/uploads/fsdgfdg_1571145869.jpg',
                'created_at' => '2019-10-15 13:24:29',
                'updated_at' => '2019-10-15 13:24:29',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'طقم ولادي قطعتين',
                'name' => 'gfhdh',
                'phone' => '0599378020',
                'details' => 'fghgf gfhhgf',
                'image' => '/uploads/gfhdh_1571146063.jpg',
                'created_at' => '2019-10-15 13:27:43',
                'updated_at' => '2019-10-15 13:27:43',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'هل سيؤثر السايبر الإيراني على مسار الانتخابات الإسرائيلية؟',
                'name' => 'ابتهال شهاب',
                'phone' => '0599378020',
                'details' => 'gfh fghdfg',
                'image' => '/uploads/abthal-shhab_1571146303.jpg',
                'created_at' => '2019-10-15 13:31:43',
                'updated_at' => '2019-10-15 13:31:43',
            ),
        ));
        
        
    }
}