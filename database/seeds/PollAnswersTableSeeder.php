<?php

use Illuminate\Database\Seeder;

class PollAnswersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('poll_answers')->delete();
        
        \DB::table('poll_answers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'poll_id' => 1,
                'answer' => 'ممتاز',
                'votes' => 3,
                'created_at' => '2019-11-03 11:34:26',
                'updated_at' => '2019-11-04 09:51:52',
            ),
            1 => 
            array (
                'id' => 2,
                'poll_id' => 1,
                'answer' => 'جيد جدأ',
                'votes' => 1,
                'created_at' => '2019-11-03 11:34:40',
                'updated_at' => '2019-11-03 13:01:48',
            ),
            2 => 
            array (
                'id' => 3,
                'poll_id' => 1,
                'answer' => 'مقبول',
                'votes' => 1,
                'created_at' => '2019-11-03 11:34:47',
                'updated_at' => '2019-11-03 13:01:57',
            ),
        ));
        
        
    }
}