<?php

use Illuminate\Database\Seeder;

class BannersSlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banners_sliders')->delete();
        
        \DB::table('banners_sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'دوسات',
                'logo' => 'banners-sliders\\October2019\\cFP29jDNS54B8IYOFt6R.png',
                'crop_logo' => NULL,
                'image' => 'banners-sliders\\October2019\\QeCp3pfNB4sJqKwmjTHz.gif',
                'crop_image' => NULL,
                'link' => 'http://p1.pagewiz.net/Dawsat/',
                'is_active' => 1,
                'created_at' => '2019-10-17 12:14:19',
                'updated_at' => '2019-10-17 12:14:19',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'حنان ستايل',
                'logo' => 'banners-sliders\\October2019\\MgUyoEFDglym00FXEvRq.png',
                'crop_logo' => NULL,
                'image' => 'banners-sliders\\October2019\\v1Zf0ddpeohz1S22XW35.png',
                'crop_image' => NULL,
                'link' => 'https://www.facebook.com/HnanStyle/',
                'is_active' => 1,
                'created_at' => '2019-10-17 12:15:02',
                'updated_at' => '2019-10-17 12:15:02',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'كراميكا هعمكيم',
                'logo' => 'banners-sliders\\October2019\\VT5kBOvUtmm5KnMDRRZL.png',
                'crop_logo' => NULL,
                'image' => 'banners-sliders\\October2019\\eYUBDce9p7QHhmdNxADw.png',
                'crop_image' => NULL,
                'link' => 'https://www.facebook.com/ceramicsa/',
                'is_active' => 1,
                'created_at' => '2019-10-17 12:15:53',
                'updated_at' => '2019-10-17 12:15:53',
            ),
        ));
        
        
    }
}