<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Hz.GystZZJLZK8yXttxhqe3aWUO0ymGzogtLMFYTnBOP3CArFmeTO',
                'remember_token' => 'jxvkBHFHXiJhd4c9TsJmQd8JYjtVLc7VpoB0GVwJUHM9ajzTcOR0M37r31xt',
                'settings' => '{"locale":"en"}',
                'created_at' => '2019-08-25 14:28:00',
                'updated_at' => '2019-11-05 20:31:36',
            ),
            1 => 
            array (
                'id' => 2,
                'role_id' => 3,
                'name' => 'Ibtihal Shihab',
                'email' => 'ibtihal@nadsoft.net',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$OGsozJ3tj4b0D6z8qkZwieSuR9TxLeiWUj9SGxpd51YfSxNdyo2pe',
                'remember_token' => 'lHzSljEgMbt1S7top4iAdinu3NR3eAZ2uFKKpz5mwpo1oSFDNL1dfM502xJ5',
                'settings' => '{"locale":"en"}',
                'created_at' => '2019-08-29 09:29:09',
                'updated_at' => '2019-08-29 09:30:16',
            ),
        ));
        
        
    }
}