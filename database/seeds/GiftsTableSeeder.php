<?php

use Illuminate\Database\Seeder;

class GiftsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('gifts')->delete();
        
        \DB::table('gifts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'ghffhdgf',
                'title' => NULL,
                'data' => 'uioyiouo',
                'is_active' => 1,
                'order' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-11-03 12:05:50',
            ),
        ));
        
        
    }
}