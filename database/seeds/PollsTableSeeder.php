<?php

use Illuminate\Database\Seeder;

class PollsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('polls')->delete();
        
        \DB::table('polls')->insert(array (
            0 => 
            array (
                'id' => 1,
                'question' => 'ما رأيك بالموقع الجديد؟',
                'fdate' => '2019-10-27',
                'tdate' => '2020-01-04',
                'closed' => 0,
                'created_at' => '2019-11-03 11:34:13',
                'updated_at' => '2019-11-03 11:34:13',
            ),
        ));
        
        
    }
}