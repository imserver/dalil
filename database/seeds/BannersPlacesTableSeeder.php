<?php

use Illuminate\Database\Seeder;

class BannersPlacesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banners_places')->delete();
        
        \DB::table('banners_places')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'مكعب',
                'width' => '300',
                'height' => '250',
                'created_at' => '2019-10-20 08:58:00',
                'updated_at' => '2019-10-20 15:58:49',
                'slug' => 'cubebnr',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'كبير',
                'width' => '1130',
                'height' => '135',
                'created_at' => '2019-10-20 09:00:00',
                'updated_at' => '2019-10-20 15:26:00',
                'slug' => 'bigbnr',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'متوسط',
                'width' => '560',
                'height' => '110',
                'created_at' => '2019-10-20 09:00:00',
                'updated_at' => '2019-10-20 15:58:40',
                'slug' => 'midbnr',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'طولي',
                'width' => '150',
                'height' => '730',
                'created_at' => '2019-11-03 13:00:18',
                'updated_at' => '2019-11-03 13:00:18',
                'slug' => 'tallbnr',
            ),
        ));
        
        
    }
}