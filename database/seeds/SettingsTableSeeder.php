<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'site.title',
                'display_name' => 'Site Title',
                'value' => 'بلدنا',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Site',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'site.description',
                'display_name' => 'Site Description',
                'value' => 'بلدنا موقع اخباري متنوع',
                'details' => '',
                'type' => 'text',
                'order' => 2,
                'group' => 'Site',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'site.logo',
                'display_name' => 'Site Logo',
                'value' => 'settings\\October2019\\fjrMKNqyqvNcXPYqaINA.png',
                'details' => '',
                'type' => 'image',
                'order' => 4,
                'group' => 'Site',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'site.google_analytics_tracking_id',
                'display_name' => 'Google Analytics Tracking ID',
                'value' => NULL,
                'details' => '',
                'type' => 'text',
                'order' => 6,
                'group' => 'Site',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'admin.bg_image',
                'display_name' => 'Admin Background Image',
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 5,
                'group' => 'Admin',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'admin.title',
                'display_name' => 'Admin Title',
                'value' => 'NadCms',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Admin',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'admin.description',
                'display_name' => 'Admin Description',
                'value' => 'Welcome to NADSoft Content Management System',
                'details' => '',
                'type' => 'text',
                'order' => 2,
                'group' => 'Admin',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'admin.loader',
                'display_name' => 'Admin Loader',
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 3,
                'group' => 'Admin',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'admin.icon_image',
                'display_name' => 'Admin Icon Image',
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 4,
                'group' => 'Admin',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'admin.google_analytics_client_id',
            'display_name' => 'Google Analytics Client ID (used for admin dashboard)',
                'value' => NULL,
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Admin',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'site.address',
                'display_name' => 'Address',
                'value' => NULL,
                'details' => NULL,
                'type' => 'text',
                'order' => 16,
                'group' => 'Site',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'contact.address',
                'display_name' => 'Address',
                'value' => 'الناصرة',
                'details' => NULL,
                'type' => 'text',
                'order' => 7,
                'group' => 'Contact',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'contact.phone',
                'display_name' => 'Phone',
                'value' => '5435435',
                'details' => NULL,
                'type' => 'text',
                'order' => 8,
                'group' => 'Contact',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'contact.fax',
                'display_name' => 'Fax',
                'value' => '445 45345435',
                'details' => NULL,
                'type' => 'text',
                'order' => 9,
                'group' => 'Contact',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'contact.email',
                'display_name' => 'Email',
                'value' => 'a@a.com',
                'details' => NULL,
                'type' => 'text',
                'order' => 10,
                'group' => 'Contact',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'social.facebook',
                'display_name' => 'Facebook',
                'value' => 'http://facebook.com',
                'details' => NULL,
                'type' => 'text',
                'order' => 11,
                'group' => 'Social',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'social.twitter',
                'display_name' => 'Twitter',
                'value' => 'http://twitter.com',
                'details' => NULL,
                'type' => 'text',
                'order' => 12,
                'group' => 'Social',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'social.instagram',
                'display_name' => 'Instagram',
                'value' => 'http://instagram.com',
                'details' => NULL,
                'type' => 'text',
                'order' => 13,
                'group' => 'Social',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'social.youtube',
                'display_name' => 'Youtube',
                'value' => 'http://yputube.com',
                'details' => NULL,
                'type' => 'text',
                'order' => 14,
                'group' => 'Social',
            ),
            19 => 
            array (
                'id' => 21,
                'key' => 'contact.map',
                'display_name' => 'Map',
                'value' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3357.7271349325!2d35.28888251503442!3d32.69330079555683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151c4dc5509ee467%3A0x87d66d2ae6817042!2sNadSoft!5e0!3m2!1sen!2s!4v1572783246916!5m2!1sen!2s" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>',
                'details' => NULL,
                'type' => 'code_editor',
                'order' => 15,
                'group' => 'Contact',
            ),
            20 => 
            array (
                'id' => 22,
                'key' => 'site.favicon',
                'display_name' => 'Fav Icon',
                'value' => 'settings\\October2019\\AM1Gzx6ATuuxrsJGQVsI.png',
                'details' => NULL,
                'type' => 'image',
                'order' => 17,
                'group' => 'Site',
            ),
            21 => 
            array (
                'id' => 23,
                'key' => 'site.keywords',
                'display_name' => 'Site Keywords',
                'value' => 'بلدنا ، اخبار، موقع، baladna',
                'details' => NULL,
                'type' => 'text',
                'order' => 3,
                'group' => 'Site',
            ),
            22 => 
            array (
                'id' => 24,
                'key' => 'site.image',
                'display_name' => 'Site image',
                'value' => 'settings\\October2019\\8h2WSecWKIf45DdaGWvg.png',
                'details' => NULL,
                'type' => 'image',
                'order' => 18,
                'group' => 'Site',
            ),
            23 => 
            array (
                'id' => 25,
                'key' => 'social.whatsapp',
                'display_name' => 'WhatsApp',
                'value' => NULL,
                'details' => NULL,
                'type' => 'text',
                'order' => 19,
                'group' => 'Social',
            ),
        ));
        
        
    }
}