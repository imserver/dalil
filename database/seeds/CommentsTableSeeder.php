<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('comments')->delete();
        
        \DB::table('comments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'post_id' => 4,
                'name' => 'ibtihal',
                'body' => 'test the comments',
                'is_active' => 1,
                'created_at' => '2019-09-30 19:39:14',
                'updated_at' => '2019-09-30 19:39:14',
            ),
            1 => 
            array (
                'id' => 2,
                'post_id' => 1,
                'name' => 'ibtihal 2',
                'body' => 'it\'s ok',
                'is_active' => 1,
                'created_at' => '2019-10-02 19:15:26',
                'updated_at' => '2019-10-02 19:15:26',
            ),
            2 => 
            array (
                'id' => 3,
                'post_id' => 1,
                'name' => 'yhedat',
                'body' => 'fd fdgfdgfdg',
                'is_active' => 1,
                'created_at' => '2019-10-02 19:15:41',
                'updated_at' => '2019-10-02 19:15:41',
            ),
            3 => 
            array (
                'id' => 4,
                'post_id' => 58,
                'name' => 'ابتهال شهاب',
                'body' => 'fds dsfdsf sdgfg fdhghgfjhfhgf',
                'is_active' => 0,
                'created_at' => '2019-10-03 10:14:30',
                'updated_at' => '2019-10-03 10:14:30',
            ),
            4 => 
            array (
                'id' => 6,
                'post_id' => 1,
                'name' => 'احمد علي',
                'body' => NULL,
                'is_active' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 7,
                'post_id' => 1,
                'name' => 'jjjhjmnbmnb',
                'body' => 'jjjhjmnbmnb',
                'is_active' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 8,
                'post_id' => 1,
                'name' => 'jhhhhhhhhhhhhhh',
                'body' => 'jhhhhhhhhhhhhhh',
                'is_active' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 9,
                'post_id' => NULL,
                'name' => 'cccvcv',
                'body' => NULL,
                'is_active' => NULL,
                'created_at' => '2019-10-10 10:16:42',
                'updated_at' => '2019-10-10 10:16:42',
            ),
            8 => 
            array (
                'id' => 10,
                'post_id' => NULL,
                'name' => 'ddddddddddddddd',
                'body' => NULL,
                'is_active' => NULL,
                'created_at' => '2019-10-10 10:18:18',
                'updated_at' => '2019-10-10 10:18:18',
            ),
            9 => 
            array (
                'id' => 11,
                'post_id' => NULL,
                'name' => 'fgdfg',
                'body' => 'gggggggggggg',
                'is_active' => 0,
                'created_at' => '2019-10-10 10:20:01',
                'updated_at' => '2019-10-10 10:20:01',
            ),
            10 => 
            array (
                'id' => 12,
                'post_id' => NULL,
                'name' => 'hgfjh',
                'body' => 'hgjf',
                'is_active' => 0,
                'created_at' => '2019-10-10 10:22:41',
                'updated_at' => '2019-10-10 10:22:41',
            ),
            11 => 
            array (
                'id' => 16,
                'post_id' => 1,
                'name' => 'fdgfdg',
                'body' => 'fgfgd',
                'is_active' => 1,
                'created_at' => '2019-10-10 10:34:27',
                'updated_at' => '2019-10-10 10:34:27',
            ),
            12 => 
            array (
                'id' => 17,
                'post_id' => 1,
                'name' => 'jhgkjhg',
                'body' => NULL,
                'is_active' => 0,
                'created_at' => '2019-10-10 10:40:33',
                'updated_at' => '2019-10-10 10:40:33',
            ),
            13 => 
            array (
                'id' => 18,
                'post_id' => 5,
                'name' => 'نور',
                'body' => 'تعليق نور',
                'is_active' => 0,
                'created_at' => '2019-10-10 22:00:42',
                'updated_at' => '2019-10-10 22:00:42',
            ),
            14 => 
            array (
                'id' => 19,
                'post_id' => 58,
                'name' => 'jhhgj',
                'body' => 'ghj',
                'is_active' => 0,
                'created_at' => '2019-10-15 02:04:09',
                'updated_at' => '2019-10-15 02:04:09',
            ),
            15 => 
            array (
                'id' => 20,
                'post_id' => 1,
                'name' => 'ابتهال بيييييييي',
                'body' => 'بليلبيلبيل',
                'is_active' => 1,
                'created_at' => '2019-10-15 11:56:23',
                'updated_at' => '2019-10-15 11:56:23',
            ),
            16 => 
            array (
                'id' => 21,
                'post_id' => 1,
                'name' => 'لللللللل',
                'body' => NULL,
                'is_active' => 0,
                'created_at' => '2019-11-04 09:51:47',
                'updated_at' => '2019-11-04 09:51:47',
            ),
        ));
        
        
    }
}