<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'display_name' => 'Administrator',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'user',
                'display_name' => 'Normal User',
                'created_at' => '2019-08-25 14:27:59',
                'updated_at' => '2019-08-25 14:27:59',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Editor',
                'display_name' => 'Article Editor',
                'created_at' => '2019-08-29 09:29:55',
                'updated_at' => '2019-08-29 09:29:55',
            ),
        ));
        
        
    }
}