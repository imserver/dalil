<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('menus')->delete();

        \DB::table('menus')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'admin',
                'created_at' => '2019-08-25 14:27:58',
                'updated_at' => '2019-08-25 14:27:58',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'site-nav',
                'created_at' => '2019-08-25 14:31:15',
                'updated_at' => '2019-09-29 06:47:20',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'site-footer',
                'created_at' => '2019-08-25 14:35:15',
                'updated_at' => '2019-09-29 06:49:20',
            ),
        ));


    }
}
