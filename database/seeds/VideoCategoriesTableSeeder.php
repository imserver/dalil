<?php

use Illuminate\Database\Seeder;

class VideoCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('video_categories')->delete();
        
        \DB::table('video_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'منوعات',
                'picture' => 'video-categories\\September2019\\sFwyFi3AyJMD6kOszWQL.jpg',
                'parent_cat' => NULL,
                'is_active' => 1,
                'created_at' => '2019-09-30 20:32:00',
                'updated_at' => '2019-11-03 12:24:01',
                'slug' => 'mnwaat',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'برامج',
                'picture' => 'video-categories\\September2019\\iwbwESJV2x6aUZhvWG4V.jpg',
                'parent_cat' => NULL,
                'is_active' => 1,
                'created_at' => '2019-09-30 20:33:00',
                'updated_at' => '2019-11-03 12:23:54',
                'slug' => 'bramj',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'برامج وثائقية',
                'picture' => 'video-categories\\September2019\\Le47COrUtGtvKPKcuPix.png',
                'parent_cat' => 2,
                'is_active' => 1,
                'created_at' => '2019-09-30 20:40:00',
                'updated_at' => '2019-11-03 12:23:47',
                'slug' => 'bramj-wthaeqyh',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'برامج أطفال',
                'picture' => 'video-categories\\September2019\\1uRC9TPAJCdlyq1faN7f.jpg',
                'parent_cat' => 2,
                'is_active' => 1,
                'created_at' => '2019-09-30 20:41:00',
                'updated_at' => '2019-11-03 12:23:39',
                'slug' => 'bramj-atfal',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'فيديو بلدنا',
                'picture' => NULL,
                'parent_cat' => NULL,
                'is_active' => 1,
                'created_at' => '2019-10-22 07:51:00',
                'updated_at' => '2019-11-03 12:23:28',
                'slug' => 'fydyw-bldna',
            ),
        ));
        
        
    }
}