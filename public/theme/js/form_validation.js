	var esec = 15;
// Email Address Validation using regular expression.
		function error_counter(){
			esec--;
			if (esec==1){ $('.errors').animate({ height: "hide" }, 700); clearInterval(esecs);}
			$("#esec").html(esec);
		}

function check_email(address){
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(address) == false) {
    	return false;
    }
	return true;
}

// Phone number Validation.

function check_phone(number){
	number = number.replace(" ","");
	var reg= /^([0-9\+\-]+)$/;
	if (reg.test(number) == false ) 
		return false;	
	return true;
}

// ID Number Validation (Israeli IDs only)
function check_id(id){
	var reg= /^[0-9]{7,10}$/;
	return (reg.test(id));
}

function check_number(num)
{
	var reg= /^[0-9]+$/;
	return (reg.test(num));
}

/*
	Form Validation function
	
	just add the class "req" to the requied fields to check if they have the 		 proper values to submit.
	
	if it's an email/phone number/age/date field/id, add a proper class to it,
	for example: <input type="text" name="email" class="req email">
	
*/

function InputAR (evt) {
    evt = (evt) ? evt : event;
    var chCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));

    if((chCode == 32 || chCode == 8) || (1548 <= chCode && chCode <= 1610) )
        return true;

    return false;

}
function InputEN (evt) {
    evt = (evt) ? evt : event;
    var chCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));

    if((chCode == 32 || chCode == 8) || (65 <= chCode && chCode <= 90) || (97 <= chCode && chCode <= 122))
        return true;

    return false;

}

function InputENAR (evt) {
    evt = (evt) ? evt : event;
    var chCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));

    if((chCode == 32 || chCode == 8) || (65 <= chCode && chCode <= 90) || 
        (97 <= chCode && chCode <= 122) || (1548 <= chCode && chCode <= 1610))
        return true;

    return false;

}

function check_form(container){
	//alert(container);
	var i=0;
	var required_labels= new Array();
	var error_msgs="";
	var flag=false;
	var radios_checked= new Array();
	var checkboxes_checked= new Array();
	var password_val_error=false;
	esec = 15;
	$(container+" .req").each(function(){
		flag= false;
		
		if ($(this).is("input")){
			switch( $(this).attr("type")){
				case "text": 
					if ($(this).val()=="") flag=true; 
					if ($(this).is(".email") && !check_email($(this).val())) flag=true;
					if ($(this).is(".phone") && !check_phone($(this).val())) flag=true;
					if ($(this).is(".num") && !check_number($(this).val())) flag=true;
                                        if ($(this).is(".id") && !check_id($(this).val())) flag=true;
					break;
				case "hidden":
					if ($(this).val()=="") flag=true;
					if ($(this).is(".email") && !check_email($(this).val())) flag=true;
					if ($(this).is(".phone") && !check_phone($(this).val())) flag=true;
					if ($(this).is(".num") && !check_number($(this).val())) flag=true;
					//alert($(this).val());
					break;
				case "password":
					var cur_value= $(this).val();
					if (cur_value == "") {
						flag= true; 
						break;
					}
					if ($(this).is(".eq1")){
						$(container+" .eq1").each(function(){
								if (cur_value != $(this).val()) {
									flag=true;
									password_val_error=true;
								}
						})
					}
					break;
					
				
					//if ($(this).is(".id")) && !check_id($(this).val())) flag=true;
				case "radio":
					if (!$("input[@name="+$(this).attr("name")+"]").siblings().is(":checked") && !radios_checked[$(this).attr("name")]){ 
					flag=true;
					radios_checked[$(this).attr("name")]=true;
				
				}
				
				case "checkbox":
					if (!$("input[@name="+$(this).attr("name")+"]").siblings().is(":checked") && !checkboxes_checked[$(this).attr("name")]){ 
					flag=true;
					checkboxes_checked[$(this).attr("name")]=true;
				
				}
			}
		}
		
		if ($(this).is("select"))
			if ($(this).val()=="0" || $(this).val()==0) flag=true;
		
		if ($(this).is("textarea")) 
			if ($(this).val()=="") flag=true;
                        if ($(this).is(".id") && !check_id($(this).val())) flag=true;
		
		if (flag && !password_val_error) {
			
			$(this).css("border","1px solid #FF3333");
			$(this).css("background","#f5b0b4");
			//alert($(this).parent().prev().html());
			required_labels[i]=$(this).parent().prev().html();
			i++;
		}
		else{ $(this).css("border","1px solid #69a8bf");
		$(this).css("background","#ffffff");
		}
	})
	
	if (i){
		
		$("span.error_details").html("");	
		
		for (var j=0; j< required_labels.length; j++){
			error_msgs+= "<li>"+required_labels[j]+"</li>";
		}
		if (password_val_error) error_msgs+= "<?=$errors[1]?>";
		//alert(error_msgs);
		//$(".error_details").html(error_msgs);
		//error_msgs+="</ul>";
		/*if ($(".errors").css("display")=="none")
			$(".errors").animate({"height":"show"},700);
		esecs = setInterval("error_counter()",1000); */
		//$("#reqDialog").dialog("open");
		return false

		}
	
	$("span.error_details").html("");
	$(".errors").hide();	
	return true;
}