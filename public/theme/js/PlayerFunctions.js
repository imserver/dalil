/* 
	################################################### 
		Copyright (c) 2005: Gafni Media Technologies	
	################################################### 
*/ 

var 
	oIntervalSliderProgressPeriod, 
	oIntervalSliderDragPeriod = 100, 
	oIntervalSliderProgressTime = null, 
	oIntervalSliderProgress = null, 
	oIntervalSliderDrag, 
	sliderIsDragged = false, 
	coefProgressSlider = null, 
	isBuffering = false, 
	oIntervalBuffering = 1, 
	oIntervalSliderVolumeProgressPeriod, 
	oIntervalSliderVolumeDragPeriod = 100, 
	oIntervalSliderVolumeProgress = null, 
	oIntervalSliderVolumeDrag, 
	sliderVolumeIsDragged = false, 
	coefProgressSliderVolume = null, 
	IsNS = navigator.appName == "Netscape", 
	IsNSAXWMP = false, 
	elBPText = document.getElementById("bptext"), 
	elObjPlayer = document.getElementById("objPlayer"),
	elBtnMute = document.getElementById("imgBtnMute"),
	objCurrentMedia = elObjPlayer == null ? null : elObjPlayer.currentMedia, 
	oIntervalPermanentPlayerStateCheck = null, 
	intSVShiftD = 0, intSVShiftC = 0, intSShiftD = 0, intSShiftC = 0,
	IsCtxtMenuTrigger = false;
	
	
coefProgressSlider = null,
coefProgressSliderVolume = null;
sliderIsDragged = false;
sliderVolumeIsDragged = false;
oIntervalBuffering = null;

if (typeof(IsPresentedSlider)=='undefined')IsPresentedSlider = false;
if (typeof(IsPresentedSliderVolume)=='undefined')IsPresentedSliderVolume = false;
if (typeof(IsMuted)=='undefined')IsMuted = false;
if (typeof(IsAutoStart)=='undefined')IsAutoStart = false;
if (typeof(IsClipHourDisplay)=='undefined')IsClipHourDisplay = false;
if (typeof(itClipWidth)=='undefined')itClipWidth = null;
if (typeof(itClipHeight)=='undefined')itClipHeight = null;
if (typeof(szURLMedia)=='undefined')szURLMedia = null;
if (typeof(IsTimeReversed)=='undefined')IsTimeReversed = false;
if (typeof(IsLive)=='undefined')IsLive = false; 
if (typeof(sendPlay)=='undefined')sendPlay = null; 
if (typeof(intSShiftCNS)=='undefined')intSShiftCNS  = 0; 
if (typeof(intSShiftCIE)=='undefined')intSShiftCIE  = 0; 
if (typeof(intSShiftDNS)=='undefined')intSShiftDNS  = 0; 
if (typeof(intSShiftDIE)=='undefined')intSShiftDIE  = 0; 
if (typeof(intSVShiftCNS)=='undefined')intSVShiftCNS = 0; 
if (typeof(intSVShiftCIE)=='undefined')intSVShiftCIE = 0; 
if (typeof(intSVShiftDNS)=='undefined')intSVShiftDNS = 0; 
if (typeof(intSVShiftDIE)=='undefined')intSVShiftDIE = 2; 
if (typeof(objQueryData) == 'undefined')objQueryData = null;
if (typeof(RegionLimit) == 'undefined')RegionLimit = '';


if (IsNS) {
	var _emptyTags = {
	   "IMG":   true,
	   "BR":    true,
	   "INPUT": true,
	   "META":  true,
	   "LINK":  true,
	   "PARAM": true,
	   "HR":    true
	};
	
	HTMLElement.prototype.__defineGetter__("outerHTML", function () {
	   var attrs = this.attributes;
	   var str = "<" + this.tagName;
	   for (var i = 0; i < attrs.length; i++)
	      str += " " + attrs[i].name + "=\"" + attrs[i].value + "\"";

	   if (_emptyTags[this.tagName])
	      return str + ">";

	   return str + ">" + this.innerHTML + "</" + this.tagName + ">";
	});

	HTMLElement.prototype.__defineSetter__("outerHTML", function (sHTML) {
	   var r = this.ownerDocument.createRange();
	   r.setStartBefore(this);
	   var df = r.createContextualFragment(sHTML);
	   this.parentNode.replaceChild(df, this);
	});
	document.onmousedown=new Function ("return false");
	document.onmouseup=new Function ("return false");
}

function getOuterHTML4ObjElIE(el) {
	var str = el.outerHTML;
	var els = el.nextSibling;
	if(str.toLowerCase().indexOf("</" + el.tagName.toLowerCase() + ">") == -1) {
		while (els.tagName!=("/" + el.tagName)) {
			str += els.outerHTML;
			els = els.nextSibling;
		}
		str += ("</" + el.tagName + ">");
	}
	return str;
}

function doChangeTagName(el, szNewTagName) {
	var szHTML = IsNS ? el.outerHTML : getOuterHTML4ObjElIE(el);
	szHTML = szHTML.replace("<" + el.tagName + " ", "<" + szNewTagName + " ").replace("</" + el.tagName + ">", "</" + szNewTagName + ">");
	el.outerHTML = szHTML;
	szHTML = null; szNewTagName = null;
}

function updateCurrentPositionReflection()
{
	var s = null; 
	try	{ 
		var objControls = elObjPlayer.controls; 
		if (IsTimeReversed == true)	{ 
			var 
				objCurrentMedia = elObjPlayer.currentMedia, 
				duration = objCurrentMedia.duration,
				currentPosition = objControls.currentPosition,
				d = duration - currentPosition; 
			s = doGetTimeStrFromPlayer(d); 
			objCurrentMedia = null; duration = null; currentPosition = null;  d = null;
		} 
		else { 
			var currentPositionString = objControls.currentPositionString; 
			s = currentPositionString; 
			currentPositionString = null;
		}
		objControls = null;
	} 
	catch(e){e=null;} 
	if (s=="")s=null; 
	if (s!=null)	
	{
		if ((s.length==5)&&IsClipHourDisplay)s= "00:" + s;
		try {
			document.getElementById("spnTimeCurrentPosition")[IsNS ? "textContent" : "innerText"] = s;
		}catch(e){e = null;}
	}
	s = null;
}

function doGetTimeStrFromPlayer(n)
{
	var n_old = n;
	var intSec = Math.floor(n % 60); n = Math.floor(n / 60); var szSec = intSec < 10 ? ("0" + intSec) : ("" + intSec);
	var intMin = Math.floor(n % 60); n = Math.floor(n / 60); var szMin = intMin < 10 ? ("0" + intMin) : ("" + intMin);
	var intHur = Math.floor(n);	var szHur = intHur + "";
	var szDuration = szDuration = szMin + ":" + szSec;
	if (n_old > 3599) szDuration = szHur + ":" + szDuration;
	n_old = null; n = null; intSec = null; intMin  = null; intHur = null; szSec = null; szMin = null; szHur = null;
	return szDuration;
}

function doCheckVersion()
{
	try
	{
		var 
			intVersion = parseInt(elObjPlayer.versionInfo.charAt(0));
		if((intVersion==8)||(intVersion==7))
		{
			var 
				szURLOld = objPlayer.URL,
				i = szURLOld.lastIndexOf("/"),
				szURLNew = szURLOld.substring(i),
				szLocation = location.toString();
			i = szLocation.indexOf("/players/");
			szLocation = szLocation.substring(0, i);
			objPlayer.URL = szLocation + szURLNew;
		}
	}
	catch(e){}
} 

function findURLElementHRef()
{
	var arrParams = document.getElementsByTagName("param");
	for (var i = 0; i < arrParams.length; i++)
	{
		if ((i != "length") && (arrParams[i].name.toLowerCase() == "url"))
			return arrParams[i].value;
	}
	return null;
}

function doPlayerInit() 
{ 
	//debugger;
	elBPText = document.getElementById("bptext"); 
	if (elObjPlayer == null) elObjPlayer = document.getElementById("objPlayer"); 
	if (navigator.appName == "Microsoft Internet Explorer")
	{
		doChangeTagName(elObjPlayer, "OBJECT"); 
		elObjPlayer = document.getElementById("objPlayer"); 
	}
	oIntervalPermanentPlayerStateCheck = window.setInterval('playerStateCheck()', 3000); 
	var 
		objCurrentMedia = elObjPlayer.currentMedia, 
		objSettings = elObjPlayer.settings, 
		objControls = elObjPlayer.controls; 
	IsNSAXWMP = objControls != null ? true : false; 
	
	if (szURLMedia == null)
	{
		//alert('111');
		try
		{
			szURLMedia = findURLElementHRef();
			var 
				loc = document.location,
				szURLPage = loc.toString(),
				i = szURLPage.lastIndexOf("/");
			if ((szURLMedia.indexOf("://") == -1)&&(szURLMedia.indexOf("/") > 0))
			{
				szURLMedia = szURLPage.substring(0, i + 1) + szURLMedia;
			}
			else if (szURLMedia.indexOf("/") == 0)
			{				
				szURLMedia = loc.protocol + "//" + loc.hostname + szURLMedia;
			}
		}
		catch(e){}
	}
	// If the video is limited to a specific region
	try {
		if (RegionLimit != '' && RegionLimit != '*')
		{
			if (!IsRegionAllowed())
			{
				szURLMedia = "";
				try
				{
					var szImgSrcForIsraelOnly = "/mediazone/images/noaccess.jpg";
					try
					{
						var div = document.getElementById("spnPreview").firstChild.src = szImgSrcForIsraelOnly;						
					}
					catch(e){}
					try
					{
						document.getElementById("spnPlayerContainer").style.backgroundImage = "url(" + szImgSrcForIsraelOnly + ")";
					}
					catch(e){}
					szURLMedia = "";
				}
				catch(e){}		
			}
		}
	}
	catch (e) {}

	if (IsNSAXWMP) 
	{
		var 
			autoStart = objSettings.autoStart, 
			playState = elObjPlayer.playState;
		elObjPlayer.url = szURLMedia == "" ? "none" : szURLMedia;
		if (szURLMedia == "") 
		elObjPlayer.url = "";
		if (IsPresentedSliderVolume) initSliderVolume(); 
		if (autoStart || IsAutoStart)try{objControls.play();}catch(e){e = null;}
		onPlayStateChange(playState);
		//if (IsPresentedSlider) 
		oIntervalSliderProgressTime = window.setInterval('updateCurrentPositionReflection()', 1000);
		if (!IsNS) 
		{
			elObjPlayer.attachEvent('click', PlayerClicked); // by Yaara 
			elObjPlayer.attachEvent('MouseMove', PlayerMouseMove);
			elObjPlayer.onmouseleave = PlayerMouseLeave;
		}
		elObjPlayer[IsNS ? "parentNode" : "parentElement"].onclick = doPlayFromScreenClick;
		elObjPlayer[IsNS ? "parentNode" : "parentElement"].style.cursor = IsNS ? "pointer" : "hand";
		autoStart = null; playState = null; szURLPage = null; i = null;	
	}
	else 
	{
		if (szURLMedia!="")
		{
			elObjPlayer.style.height = "0px";
			document.getElementById("spnPreview").style.display = "none";
			document.getElementById("divNotSupported").style.display = "";
			document.getElementById("divNotSupported")[IsNS ? "parentNode" : "parentElement"].style.cursor = "";
			document.getElementById("divNotSupported")[IsNS ? "parentNode" : "parentElement"].onclick = null;
			document.getElementById("aClip").href = szURLMedia;
		}
		window.clearInterval(oIntervalPermanentPlayerStateCheck);
	}
	if (IsNS)
	{
		document.onkeypress = preparePlayerCheckPointContextMenu;
	}
	else
	{
		document.body.onkeypress = preparePlayerCheckPointContextMenu;
		elObjPlayer.attachEvent("oncontextmenu", getCheckPointPlayerContextMenu);
		//elObjPlayer.oncontextmenu = getCheckPointPlayerContextMenu;
	}
	doCheckVersion();
	objCurrentMedia = null;	objSettings = null;	objControls = null;
	autoStart = null; playState = null;	url = null; s = null;
}

function playerStateCheck()
{
	var playState = elObjPlayer.playState;
	try
	{
		if (playState == 3)
			flipVisibility2Player2SplashScreen(elObjPlayer, document.getElementById("spnPreview"));
	}
	catch(e){e=null;}
	if ((itClipWidth!=null)&&(parseInt(elObjPlayer.style.width)!= itClipWidth))
		elObjPlayer.style.width = itClipWidth + "px";
	if ((itClipHeight!=null)&&(parseInt(elObjPlayer.style.height)!= itClipHeight))
		elObjPlayer.style.height = itClipHeight + "px";
	/*
	status = elObjPlayer.width;
	if ((!IsNS)&&(elObjPlayer.width != "")&&(elObjPlayer.width != "0")&&(elObjPlayer.width != 0)&&(elObjPlayer.width != "0px"))
	{
		elObjPlayer.style.width = elObjPlayer.width +"px";
	}
	*/
	initSlider(true);
	playState = null;
}

function SliderStartStopDrag(event)
{
	if (!sliderIsDragged)
	{
		document.body.onmousemove = SliderPointDragUpdate; 
		document.body.onmouseup = SliderStartStopDrag; 
	} 
	else 
	{ 
		if(IsNS) { 
			event.preventDefault(); 
			event.stopPropagation(); 
		}
		document.body.onmousemove=null;
		document.body.onmouseup = IsNS ? new Function("return false;") : null;
		var 
			objControls = elObjPlayer.controls, 
			SliderPosition = 
				parseInt(document.getElementById("spnSliderPoint").style.left, 10) + 
				parseInt(document.getElementById("spnSliderPoint").clientWidth, 10) / 2; 
		objControls.currentPosition = coefProgressSlider * SliderPosition; 
		objControls = null; SliderPosition = null; 
	} 
	sliderIsDragged = !sliderIsDragged; 
}

function SliderVolumeStartStopDrag(event)
{
	if (!IsNSAXWMP) return;
	if (!sliderVolumeIsDragged)
	{
		document.body.onmousemove=SliderVolumePointDragUpdate;
		document.body.onmouseup=SliderVolumeStartStopDrag;
	}
	else
	{					
		document.body.onmousemove = null;
		document.body.onmouseup = IsNS ? new Function("return false;") : null;
		var SliderVolumePosition = 
				parseInt(document.getElementById("spnSliderVolumePoint").style.left, 10) + 
				parseInt(document.getElementById("spnSliderVolumePoint").clientWidth, 10) / 2,
			objSettings = elObjPlayer.settings, 
			volume = objSettings.volume; 
		volume = coefProgressSliderVolume * SliderVolumePosition; 
		window.status = "Volume " + objSettings.volume + " % " + (IsMuted ? "(muted)" : ""); 
		SliderVolumePosition = null; objSettings = null; volume = null;
	} 
	sliderVolumeIsDragged = !sliderVolumeIsDragged; 
} 
 
function doGetElementOffsetLeft(el) 
{ 
	var elParent = el[IsNS ? "parentNode" : "parentElement"], 
		itOffsetLeft = parseInt(elParent.offsetLeft, 10);
	while(elParent.tagName != "BODY") { 
		elParent = elParent[IsNS ? "parentNode" : "parentElement"]; 
		itOffsetLeft += parseInt(elParent.offsetLeft, 10);
	} 
	elParent = null; 
	return itOffsetLeft; 
} 

function doGetSliderCommonPosition(spnSliderCommonPoint, itSliderRightShift, event) 
{	
	if ((typeof(itSliderRightShift)=='undefined')||(itSliderRightShift==null))itSliderRightShift = 0; 
	var	itOffsetLeft = doGetElementOffsetLeft(spnSliderCommonPoint); 
	if (event.clientX > (itOffsetLeft + spnSliderCommonPoint[IsNS ? "parentNode" : "parentElement"].clientWidth)) 
	{
		//window.status = spnSliderCommonPoint[IsNS ? "parentNode" : "parentElement"].clientWidth;
		return spnSliderCommonPoint[IsNS ? "parentNode" : "parentElement"].clientWidth - itSliderRightShift; 
	}
	else if (event.clientX < itOffsetLeft) 
		return 0; 
	else 
		return event.clientX - itOffsetLeft; 
	itOffsetLeft = null; 
	return event.clientX; 
} 

function SliderPointDragUpdate(event)
{
	if (typeof(event)=='undefined') event = window.event;
	setSliderPosition(doGetSliderCommonPosition(document.getElementById("spnSliderPoint"), 0, event));
}

function SliderVolumePointDragUpdate(event)
{
	if (typeof(event)=='undefined') event = window.event;
	var SliderVolumePosition = doGetSliderCommonPosition(document.getElementById("spnSliderVolumePoint"), 0, event);
	setSliderVolumePosition(SliderVolumePosition, 0, event);
	var objSettings = elObjPlayer.settings, volume = objSettings.volume;
	objSettings.volume = coefProgressSliderVolume * SliderVolumePosition; // YYY YYY YYY YYY YYY YYY YYY YYY 
	//setTimeout("elObjPlayer.settings.mute = " + IsMuted, 0);
	objSettings.mute = IsMuted;
	window.status = "Volume " + volume + " % " + (IsMuted ? "(muted)" : "");
	SliderVolumePosition = null; objSettings = null; volume = null;
}

function SliderAreaClick(sender, event)
{
	if (!IsPresentedSlider) return;
	if ((sliderIsDragged==true)||(IsNS && (event!=null) && (event.target.id == "spnSliderPoint"))) return;
	setSliderPosition(sender, event);
}

function SliderVolumeAreaClick(sender, event)
{
	if (!IsPresentedSliderVolume) return;
	if ((sliderVolumeIsDragged==true)||(IsNS && (event!=null) && (event.target.id == "spnSliderVolumePoint"))) return;
	setSliderVolumePosition(sender, event);
}

function setSliderPosition(sender, event)
{
	if (!IsNSAXWMP) return;
	var SliderPosition = 
		parseInt(document.getElementById("spnSliderPoint").style.left, 10) + 
		parseInt(document.getElementById("spnSliderPoint").clientWidth, 10) / 2,
		objControls = elObjPlayer.controls; 
	if ((sender.tagName)&&(event[IsNS ? "target" : "srcElement"] != document.getElementById("spnSliderPoint"))) { 
		intOfs = event[IsNS ? "layerX" : "offsetX"];
		SliderPosition = intOfs - intSShiftC + 1; 
		intOfs = null;
		objControls.currentPosition = coefProgressSlider * SliderPosition; 
	} 
	else if (!isNaN(sender)) {
		SliderPosition = sender + (sliderIsDragged ? intSShiftDIE : 0);
	}
	document.getElementById("spnSliderPoint").style.left  = (SliderPosition - parseInt(document.getElementById("spnSliderPoint").clientWidth, 10) / 2) + "px";
	document.getElementById("spnSliderTrail").style.width = SliderPosition.toString() + "px";
	SliderPosition = null; objControls = null; currentPosition = null; sender = null; event = null;
}

function setSliderVolumePosition(sender, event) 
{ 
	if (!IsNSAXWMP) return;
	if (typeof(event)=='undefined') event = window.event;
	var SliderVolumePosition = 
		parseInt(document.getElementById("spnSliderVolumePoint").style.left, 10) + 
		parseInt(document.getElementById("spnSliderVolumePoint").clientWidth, 10) / 2,
		objSettings = elObjPlayer.settings;
	if ((sender.tagName)&&(event[IsNS ? "target" : "srcElement"] != document.getElementById("spnSliderVolumePoint"))) 
	{ 
		SliderVolumePosition = 
			(
				IsNS ?
				event.layerX - doGetElementOffsetLeft(document.getElementById("spnSliderVolumeArea")) :
				event.offsetX
			) - intSVShiftC + 1;
		initSliderVolume();
		objSettings.volume = coefProgressSliderVolume * SliderVolumePosition; 
		objSettings.mute = IsMuted;
		window.status = "Volume " + elObjPlayer.settings.volume + " % " + (IsMuted ? "(muted)" : ""); 
	} 
	else if (!isNaN(sender)) 
		SliderVolumePosition = sender + (sliderVolumeIsDragged ? intSVShiftDIE : 0); 
	document.getElementById("spnSliderVolumePoint").style.left = (SliderVolumePosition - parseInt(document.getElementById("spnSliderVolumePoint").clientWidth, 10) / 2) + "px"; 
	SliderVolumePosition = null; objSettings = null; 
} 

function SliderPointProgressUpdate()
{	
	var objControls = elObjPlayer.controls, currentPosition = objControls.currentPosition, progress = currentPosition / coefProgressSlider;
	try {
		if (isNaN(progress)) progress = 0;
		SliderAreaClick(progress, null);
	}
	catch(e){e = null;}
	objControls = null; currentPosition = null; 
}

function SliderVolumePointProgressUpdate()
{	
	//SliderVolumeAreaClick(objPlayer.settings.volume / coefProgressSliderVolume);
}

function initSlider(blRecalculateCoef) 
{ 
	if (!IsNSAXWMP) return;
	if ( 
		(typeof(IsPresentedSlider)=='undefined') || 
		!IsPresentedSlider || 
		(!blRecalculateCoef && coefProgressSlider!=null) 
		) return; 
	var newCfc = coefProgressSlider, objCurrentMedia = elObjPlayer.currentMedia, duration = objCurrentMedia == null ? null : objCurrentMedia.duration; 
	if (duration!=null)
	{
		try { 
			newCfc = duration / document.getElementById("spnSliderArea").clientWidth; 
		} 
		catch(e) { 
			e = null; 
		} 
		if (newCfc != coefProgressSlider) 
		{ 
			coefProgressSlider = newCfc; 
			if ((coefProgressSlider!=null)&&(coefProgressSlider > 0)) { 
				oIntervalSliderProgressPeriod = 5000 * coefProgressSlider; 
				window.clearInterval(oIntervalSliderProgress); 
				oIntervalSliderProgress = window.setInterval('SliderPointProgressUpdate()', oIntervalSliderProgressPeriod);	
				SliderPointProgressUpdate(); 
			} 
		} 
	}
	newCfc = null; objCurrentMedia = null; duration = null; 
} 
 
function initSliderVolume() 
{ 
	if ((coefProgressSliderVolume!=null)&&(coefProgressSliderVolume!=Infinity)&&(typeof(coefProgressSliderVolume)!='undefined')) return; 
	coefProgressSliderVolume = 100 / parseInt(document.getElementById("spnSliderVolumeArea").clientWidth, 10); 
	
	if(IsPresentedSliderVolume) {
		var objSettings = elObjPlayer.settings, volume = objSettings.volume;
		setSliderVolumePosition(volume / coefProgressSliderVolume);
		objSettings = null; volume = null;
	}
} 
 
function onBuffering(Start)
{
	//debugger
	isBuffering = Start;
	if (Start)
	{ 
		oIntervalBuffering = window.setInterval('updatebp()', 250); 
		flipVisibility2Player2SplashScreen(document.getElementById("spnPreview"), elObjPlayer);
	} 
	else {
		window.clearInterval(oIntervalBuffering);
		oIntervalBuffering = null;
		flipVisibility2Player2SplashScreen(elObjPlayer, document.getElementById("spnPreview"));
		elBPText.innerHTML = "";
		elBPText.style.display = "none";
		//debugger
	}
	Start = null;
}

function onPlayStateChange(NewState)
{
	//alert(NewState + " - " + elObjPlayer.currentMedia.sourceURL);
	switch (NewState)
	{
		case  0:
			elBPText.innerHTML = "";
			elBPText.style.display = "none";
		case  1:
			elBPText.innerHTML = "";
			elBPText.style.display = "none";
			try {
				document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSStoppedPaused");
			}catch(e){e=null;}
			//debugger
			flipVisibility2Player2SplashScreen(document.getElementById("spnPreview"), elObjPlayer);	
			break; 
		case  2:
			try
			{
				document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSStoppedPaused");
			}
			catch(e){e=null;}
			break;		
		case  3:			
			var objSettings = elObjPlayer.settings, volume = objSettings.volume;
			if(IsPresentedSliderVolume) setSliderVolumePosition(volume / coefProgressSliderVolume);
			try {
				if (!IsLive) document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSPlaying");
			}catch(e){e=null;};
  			if (isBuffering==false) 
  			{
  				elBPText.innerHTML = "";
  				elBPText.style.display = "none";
  				flipVisibility2Player2SplashScreen(elObjPlayer, document.getElementById("spnPreview"));
  				initSlider(false);
  			}
  			if (IsPresentedSlider) {
  				var objCurrentMedia = elObjPlayer.currentMedia, s = objCurrentMedia.durationString;
  				if((s.length==5) && IsClipHourDisplay) 
  					s = "00:" + s;
  				else if (s.length == 8) 
					IsClipHourDisplay = true; 
				try {
  					document.getElementById("spnTimeEnd")[IsNS ? "textContent" : "innerText"] = s;
  				}
  				catch(e){e = null;}
  				s = null; objCurrentMedia = null;
  			}
  			setTimeout("elObjPlayer.settings.mute = " + IsMuted, 0);
  			objSettings = null;
  			if (sendPlay!=null) sendPlay();
  			// alert(elObjPlayer.currentMedia.sourceURL);
  			// To be replaced by a check of the elObjPlayer.currentMedia.getItemInfo("logoURL")
  			// if there's an Erate code, then need to make a call to Erate
  			//var clipname = elObjPlayer.currentMedia.getItemInfo("Erate")
  			//alert(clipname);
  			//var clipname = elObjPlayer.currentMedia.sourceURL;
  			//if (clipname != '')
  			//	clipname = clipname.substr(clipname.lastIndexOf('/')+1);
  			//if (clipname == '21319!32251.wmv')  // need to check if there's an Erate code
  			//	requestToErate('6PKFZG');  // Erate code  			
  			// if (clipname == '4731!5559.wmv') // test clip
			//	requestToErate('7NQKCA');  // 
			break;
		case  4:	// Fast forward 
			try {
				document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSStoppedPaused");
			}catch(e){e=null;}
			break; 
		case  5:	// Rewind
			try {
				document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSStoppedPaused");
			}catch(e){e=null;}
			break; 
		case  6: break; 
		case  7:
			elBPText.innerHTML = "Please wait ...";
			elBPText.style.display = "";
			break; 
		case  8:
			try
			{
				if (IsPresentedSlider) 
					document.getElementById("spnTimeCurrentPosition")[IsNS ? "textContent" : "innerText"] = 
					document.getElementById("spnTimeEnd")[IsNS ? "textContent" : "innerText"];
			}
			catch(e){e=null;}
			try {
			document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSStoppedPaused");
			}catch(e){}
			elBPText.innerHTML = "";
			elBPText.style.display = "none";
			flipVisibility2Player2SplashScreen(document.getElementById("spnPreview"), elObjPlayer);
			elBPText.innerHTML = "";
			elBPText.style.display = "none";
			break; 
		case  9:
			elBPText.innerHTML = "Connecting ...";
			elBPText.style.display = "";
			try {
				if (!IsLive)document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSPlaying");
			}catch(e){e=null;}
			flipVisibility2Player2SplashScreen(document.getElementById("spnPreview"), elObjPlayer);
			break;
		case 10:
			window.clearInterval(oIntervalBuffering);
			oIntervalBuffering = null;
			elBPText.innerHTML = "";
			elBPText.style.display = "none";
			try {
				document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSStoppedPaused");
			}catch(e){e=null}
			flipVisibility2Player2SplashScreen(document.getElementById("spnPreview"), elObjPlayer);
			break;
		case 11:
  			document.getElementById("imgBtnPlayState").src = changeImageSource(document.getElementById("imgBtnPlayState").src, "imgBtnPSStoppedPaused"); 
 			break; 
	}
	if (typeof(IsInPlaylistShow)!='undefined')
	{
		playStateChange(NewState);
	}
	NewState = null;
}

function flipVisibility2Player2SplashScreen(control1, control2)
{
	//debugger
	if (control1.id == "spnPreview") {
		control1.style.display = "";
		control2.style.visibility = "hidden";
	}
	else {
		control2.style.display = "none";
		control1.style.visibility = "";
	}
}

function updatebp()
{
	if (oIntervalBuffering==null) 
	{ 
		elBPText.innerHTML = ""; 
		elBPText.style.display = "none";
		return;
	}
	//debugger
	var objNetwork = elObjPlayer.network, bufferingProgress = objNetwork.bufferingProgress;
	elBPText.innerHTML = "Buffering:  " + bufferingProgress + "%";
	elBPText.style.display = "";
	objNetwork = null; bufferingProgress = null;
}

function clickPlayPause()
{
	var objControls = elObjPlayer.controls, playState = elObjPlayer.playState;
	switch (playState) {
		case 3: case 9:
			if (IsLive) 
				;//objControls.Stop();
			else 
				objControls.Pause(); 
			break;
		default: 	
			try{objControls.Play();}catch(e){e = null;}
	}
	objControls = null; playState = null;
}

function clickRunStop()
{
	if (!IsNSAXWMP) return;
	var 
		playState = elObjPlayer.playState, 
		objControls = elObjPlayer.controls; 
	switch (playState) {
		case 2: case 3: case 9: 
			objControls.Stop();
			if (szURLMedia=="") {
				var 
					objCurrentMedia = elObjPlayer.currentMedia, 
					objCurrentPlaylist = elObjPlayer.currentPlaylist, 
					Item0 = objCurrentPlaylist.Item(0);
				objCurrentMedia = Item0;
				objCurrentPlaylist = null; objCurrentPlaylist = null; Item0 = null;
			}
			else {
				elObjPlayer.URL = szURLMedia;
				objControls.Stop();
			}
			//if (IsMuted)clickMute(document.getElementById("imgBtnMute"));
			break;
		default: 
			//if (IsLive) objControls.Play();
			break;
	}
	playState = null; objControls = null;
}

function doMouseOverImage(sender, event)
{
	var szImgURL = sender.tagName == "IMG" ? sender.src : sender.style.backgroundImage;
	if (typeof(event)=='undefined')
		event = window.event;
	if (sender.tagName != "IMG")
		szImgURL = szImgURL.substr(4, szImgURL.length - 5);
	var szNewSrc, i = szImgURL.lastIndexOf("."), szImgExt = szImgURL.substr(i);	
	if(szImgURL.indexOf("_DA.")>0) return;
	if(szImgURL.indexOf("_MO.")>0)
		szNewSrc = szImgURL.substr(0, szImgURL.length - 3 - szImgExt.length);
	else
		szNewSrc = szImgURL.substr(0, szImgURL.length - szImgExt.length);
	if(event.type=="mouseover") szNewSrc += "_MO";
	szImgURL = szNewSrc + szImgExt;
	if (sender.tagName == "IMG")
		sender.src = szImgURL;
	else
		sender.style.backgroundImage = "url(" + szImgURL + ")";
	szImgURL = null; szNewSrc = null; i = null; szImgExt = null;
}

function changeImageSource(szOldImageSource, szNewImageName)
{
	var i = szOldImageSource.lastIndexOf("."), szImgExt = szOldImageSource.substr(i);
	i = szOldImageSource.lastIndexOf("/");
	var szImagePath = i==-1 ? "" : szOldImageSource.substr(0, i + 1); 
	var retVal = (szImagePath + szNewImageName + szImgExt);
	i = null; szImagePath = null; szNewImageName = null; szImgExt = null;
	return retVal;
}

function clickMute(sender)
{
	if (!IsNSAXWMP) return;
	IsMuted = !IsMuted;
	//alert("clickMute" + IsMuted);
	var objSettings = elObjPlayer.settings;
	objSettings.mute = IsMuted ? true : false;
	sender.src = changeImageSource(sender.src, IsMuted ? "imgBtnMuted" : "imgBtnMute");
	try	{ 
		sender[IsNS ? "parentNode" : "parentElement"].
			childNodes[IsNS ? 3 : 2][IsNS ? "textContent" : "innerText"] = 
				IsMuted ? "���� ���" : "���� ���"; 
	} 
	catch(e){e = null;}
	objSettings = null;
}	

function clickFullScreen()
{
	var playstate = elObjPlayer.playstate;
	if(playstate==3) elObjPlayer.fullScreen = true;
}
 
function volUp() 
{ 
	var objSettings = elObjPlayer.settings; 
	if (objSettings.mute) 
		elBtnMute.src="images/imgBtnMute.png"; 
	if (objSettings.volume > 80) 
		objSettings.volume +=  5; 
	else 
		objSettings.volume += 10; 
	objSettings = null; 
} 
 
function volDn() 
{ 
	var objSettings = elObjPlayer.settings; 
	if (objSettings.mute) 
		elBtnMute.src="images/imgBtnMute.png"; 
	if (objSettings.volume < 20) 
		objSettings.volume -= 5; 
	else 
		objSettings.volume -= 10; 
	objSettings = null; 
} 

function clickGotoPrev() {
	var objControls = elObjPlayer.controls;
	objControls.previous();
	objControls = null;
}

function clickGotoNext() {
	var objControls = elObjPlayer.controls;
	objControls.next();
	objControls = null;
}

function preparePlayerCheckPointContextMenu()
{	
	document.selection.empty();
	elObjPlayer.setCapture();
	//try{window.status = event.keyCode + " " + (new Date());}catch(e){window.status = e.message + " " + (new Date()) + arguments[0];};
	//try{window.status = event.charCode + " ";}catch(e){};
	//alert('aaa');
	if (IsNS) 
	{
		event = arguments[0];
		if (event==null)
        	{
			elObjPlayer.enableContextMenu = false;
			return;
		}
	}
	else
	{
		if (arguments[0] == false) 
		{
			elObjPlayer.enableContextMenu = false;
			//window.status += "!!!!!!!!!";
			return;
		}
	}
	var kc = IsNS ? event.charCode : event.keyCode;
	if (IsNS)
	{
		if ((kc == 89) && event.ctrlKey) 
		{
			elObjPlayer.enableContextMenu = true;
			setTimeout('preparePlayerCheckPointContextMenu(null)', 1000);
		}
	}
	else
	{
		if (kc==25)
		{
			elObjPlayer.enableContextMenu = true;
			setTimeout('preparePlayerCheckPointContextMenu(false)', 1000);
		}
	}
}
    
function getCheckPointPlayerContextMenu(event)
{
	document.selection.empty();
	elObjPlayer.setCapture();
	elObjPlayer.enableContextMenu = IsCtxtMenuTrigger;
	IsCtxtMenuTrigger = false;
	//alert('aaaa');
}

function PlayerClicked(nButton, nShiftState, fX, fY)
{
	//alert("clicked");
	//var clickEnabled = clipsArr[currentClipId].enabled;
	//var url = clipsArr[currentClipId].url;

	//if (clickEnabled && url.length>0)
	//{
	//	window.open(url);
	//}
	if (nButton!=1)return;
	var szLogodURL = elObjPlayer.currentMedia.getItemInfo("logoURL");
	szLogodURL = szLogodURL.replace(/!/g,"&");
	if ((szLogodURL != null) && (szLogodURL != ""))
	{
		var mediaUrl = elObjPlayer.currentMedia.sourceURL;
		clickPlayPause();
		try{addHotlinkStat(szLogodURL, mediaUrl);}catch(e){}
		try{window.open(szLogodURL, "hotlinkwindow");}catch(e){}
	}
}

function PlayerMouseMove(nButton, nShiftState, fX, fY)
{
	var szLogodURL = elObjPlayer.currentMedia.getItemInfo("logoURL");
	if ((szLogodURL != null) && (szLogodURL != ""))
	{
		objPlayer.style.cursor = "";
		window.status = szLogodURL;
		objPlayer.style.cursor = "hand";
	}
}

function PlayerMouseLeave()
{
	window.status = '';
}

function doPlayFromScreenClick()
{
	switch (elObjPlayer.playState)
	{
		case 1: case 2: case 10: {var objControls = elObjPlayer.controls; objControls.play(); objControls = null;}
	}
}

function doGetQueryParams(szQuery) 
{ 
	szQuery = szQuery.substr(1); 
	var arrValues = szQuery.split(/[/?,/&]/), data = {}, i; 
	for(i=0; i<arrValues.length; i++) 
	{ 
		arrValues[i] = arrValues[i].split("="); 
		data[arrValues[i][0].toLowerCase()] = arrValues[i][1]; 
	} 
	return data; 
} 

function IsRegionAllowed()
{

	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
		   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
		  xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		try {
			xmlhttp = new XMLHttpRequest();
		} catch (e) {
			xmlhttp=false;
		}
	}

	xmlhttp.open("GET", "http://players.mediazone.co.il/media/redirector/test.aspx",false);
	xmlhttp.send(null);
	// return the country for the client's IP address
	var doc = xmlhttp.responseText.toUpperCase();

	if (RegionLimit.indexOf("NOT") < 0) {
		if (doc.indexOf(RegionLimit) < 0)
			return false;
		else
			return true;
	}
	else {
		if (doc.indexOf(RegionLimit.substring(4)) < 0)
			return true;
		else
			return false;
	}
}

function requestToErate(erateId)
{
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
		   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
		  xmlhttp = false;
		}
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		try {
			xmlhttp = new XMLHttpRequest();
		} catch (e) {
			xmlhttp=false;
		}
	}
	try {
		// var url="http://213.8.137.51/Erate/EventReportQuery.asp?Toolid=" +erateId+ "&EventType=1";		
		// var url = "http://213.8.137.51/erate/eventreport.asp";
		var url = "http://players.mediazone.co.il/media/erate_redirect.asp?Toolid=" +erateId+ "&EventType=1";
		// http://players.mediazone.co.il/media/erate_redirect.asp?Toolid=7NQKCA&EventType=1
		// var data = "Toolid=" + erateId + "&EventType=1";
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var doc = xmlhttp.responseText;
		// alert(doc);
	}
	catch (e) {}
}

function addHotlinkStat(hlUrl, mediaUrl)
{
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
		   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
		  xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		try {
			xmlhttp = new XMLHttpRequest();
		} catch (e) {
			xmlhttp=false;
		}
	}
	try {
		// hotlinkurl=www.cnn.com&ip=234&os=win32&browser=firefox&playlistID=12345
		var url="http://players.mediazone.co.il/media/HotLinkStat_new/HotLinkStat.aspx?hotlinkurl=" + hlUrl;
		var a = mediaUrl.lastIndexOf("/")+1;
		var b = mediaUrl.lastIndexOf("!");
		var clipId = mediaUrl.substring(a,b);
		url += "&playlistID=" + clipId;
		url += "&os=" + navigator.platform;
		url += "&browser=" + navigator.appName + ' - ' + navigator.appVersion;
		url += "&ip=" + ClientIP;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
		//var doc = xmlhttp.responseText;
	}
	catch (e) {}
}

function clickForward()
{
	var objControls = elObjPlayer.controls;
	if (objControls.isAvailable('FastForward'))
		objControls.fastForward();
	objControls = null;
}

function clickRewind()
{
	var objControls = elObjPlayer.controls;
	if (objControls.isAvailable('FastReverse'))
		objControls.fastReverse();
	objControls = null;
}